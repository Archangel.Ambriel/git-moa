<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>PHP: PHP 7 ChangeLog</title>

 <link rel="shortcut icon" href="https://www.php.net/favicon.ico">
 <link rel="search" type="application/opensearchdescription+xml" href="http://php.net/phpnetimprovedsearch.src" title="Add PHP.net search">
 <link rel="alternate" type="application/atom+xml" href="https://www.php.net/releases/feed.php" title="PHP Release feed">
 <link rel="alternate" type="application/atom+xml" href="https://www.php.net/feed.atom" title="PHP: Hypertext Preprocessor">

 <link rel="canonical" href="https://www.php.net/ChangeLog-7.php">
 <link rel="shorturl" href="https://www.php.net/ChangeLog-7">
 <link rel="alternate" href="https://www.php.net/ChangeLog-7" hreflang="x-default">



<link rel="stylesheet" type="text/css" href="/cached.php?t=1539771603&amp;f=/fonts/Fira/fira.css" media="screen">
<link rel="stylesheet" type="text/css" href="/cached.php?t=1539765004&amp;f=/fonts/Font-Awesome/css/fontello.css" media="screen">
<link rel="stylesheet" type="text/css" href="/cached.php?t=1540425603&amp;f=/styles/theme-base.css" media="screen">
<link rel="stylesheet" type="text/css" href="/cached.php?t=1540425603&amp;f=/styles/theme-medium.css" media="screen">
<link rel="stylesheet" type="text/css" href="/cached.php?t=1476869406&amp;f=/styles/changelog.css" media="screen">

 <!--[if lte IE 7]>
 <link rel="stylesheet" type="text/css" href="https://www.php.net/styles/workarounds.ie7.css" media="screen">
 <![endif]-->

 <!--[if lte IE 8]>
 <script>
  window.brokenIE = true;
 </script>
 <![endif]-->

 <!--[if lte IE 9]>
 <link rel="stylesheet" type="text/css" href="https://www.php.net/styles/workarounds.ie9.css" media="screen">
 <![endif]-->

 <!--[if IE]>
 <script src="https://www.php.net/js/ext/html5.js"></script>
 <![endif]-->

 <base href="https://www.php.net/ChangeLog-7.php">

</head>
<body class="docs ">

<nav id="head-nav" class="navbar navbar-fixed-top">
  <div class="navbar-inner clearfix">
    <a href="/" class="brand"><img src="/images/logos/php-logo.svg" width="48" height="24" alt="php"></a>
    <div id="mainmenu-toggle-overlay"></div>
    <input type="checkbox" id="mainmenu-toggle">
    <ul class="nav">
      <li class=""><a href="/downloads">Downloads</a></li>
      <li class="active"><a href="/docs.php">Documentation</a></li>
      <li class=""><a href="/get-involved" >Get Involved</a></li>
      <li class=""><a href="/support">Help</a></li>
    </ul>
    <form class="navbar-search" id="topsearch" action="/search.php">
      <input type="hidden" name="show" value="quickref">
      <input type="search" name="pattern" class="search-query" placeholder="Search" accesskey="s">
    </form>
  </div>
  <div id="flash-message"></div>
</nav>
<div class="headsup"><a href='/index.php#id2020-04-17-1'>PHP 7.2.30 Release Announcement</a></div>
<nav id="trick"><div><dl>
<dt><a href='/manual/en/getting-started.php'>Getting Started</a></dt>
	<dd><a href='/manual/en/introduction.php'>Introduction</a></dd>
	<dd><a href='/manual/en/tutorial.php'>A simple tutorial</a></dd>
<dt><a href='/manual/en/langref.php'>Language Reference</a></dt>
	<dd><a href='/manual/en/language.basic-syntax.php'>Basic syntax</a></dd>
	<dd><a href='/manual/en/language.types.php'>Types</a></dd>
	<dd><a href='/manual/en/language.variables.php'>Variables</a></dd>
	<dd><a href='/manual/en/language.constants.php'>Constants</a></dd>
	<dd><a href='/manual/en/language.expressions.php'>Expressions</a></dd>
	<dd><a href='/manual/en/language.operators.php'>Operators</a></dd>
	<dd><a href='/manual/en/language.control-structures.php'>Control Structures</a></dd>
	<dd><a href='/manual/en/language.functions.php'>Functions</a></dd>
	<dd><a href='/manual/en/language.oop5.php'>Classes and Objects</a></dd>
	<dd><a href='/manual/en/language.namespaces.php'>Namespaces</a></dd>
	<dd><a href='/manual/en/language.errors.php'>Errors</a></dd>
	<dd><a href='/manual/en/language.exceptions.php'>Exceptions</a></dd>
	<dd><a href='/manual/en/language.generators.php'>Generators</a></dd>
	<dd><a href='/manual/en/language.references.php'>References Explained</a></dd>
	<dd><a href='/manual/en/reserved.variables.php'>Predefined Variables</a></dd>
	<dd><a href='/manual/en/reserved.exceptions.php'>Predefined Exceptions</a></dd>
	<dd><a href='/manual/en/reserved.interfaces.php'>Predefined Interfaces and Classes</a></dd>
	<dd><a href='/manual/en/context.php'>Context options and parameters</a></dd>
	<dd><a href='/manual/en/wrappers.php'>Supported Protocols and Wrappers</a></dd>
</dl>
<dl>
<dt><a href='/manual/en/security.php'>Security</a></dt>
	<dd><a href='/manual/en/security.intro.php'>Introduction</a></dd>
	<dd><a href='/manual/en/security.general.php'>General considerations</a></dd>
	<dd><a href='/manual/en/security.cgi-bin.php'>Installed as CGI binary</a></dd>
	<dd><a href='/manual/en/security.apache.php'>Installed as an Apache module</a></dd>
	<dd><a href='/manual/en/security.sessions.php'>Session Security</a></dd>
	<dd><a href='/manual/en/security.filesystem.php'>Filesystem Security</a></dd>
	<dd><a href='/manual/en/security.database.php'>Database Security</a></dd>
	<dd><a href='/manual/en/security.errors.php'>Error Reporting</a></dd>
	<dd><a href='/manual/en/security.globals.php'>Using Register Globals</a></dd>
	<dd><a href='/manual/en/security.variables.php'>User Submitted Data</a></dd>
	<dd><a href='/manual/en/security.magicquotes.php'>Magic Quotes</a></dd>
	<dd><a href='/manual/en/security.hiding.php'>Hiding PHP</a></dd>
	<dd><a href='/manual/en/security.current.php'>Keeping Current</a></dd>
<dt><a href='/manual/en/features.php'>Features</a></dt>
	<dd><a href='/manual/en/features.http-auth.php'>HTTP authentication with PHP</a></dd>
	<dd><a href='/manual/en/features.cookies.php'>Cookies</a></dd>
	<dd><a href='/manual/en/features.sessions.php'>Sessions</a></dd>
	<dd><a href='/manual/en/features.xforms.php'>Dealing with XForms</a></dd>
	<dd><a href='/manual/en/features.file-upload.php'>Handling file uploads</a></dd>
	<dd><a href='/manual/en/features.remote-files.php'>Using remote files</a></dd>
	<dd><a href='/manual/en/features.connection-handling.php'>Connection handling</a></dd>
	<dd><a href='/manual/en/features.persistent-connections.php'>Persistent Database Connections</a></dd>
	<dd><a href='/manual/en/features.safe-mode.php'>Safe Mode</a></dd>
	<dd><a href='/manual/en/features.commandline.php'>Command line usage</a></dd>
	<dd><a href='/manual/en/features.gc.php'>Garbage Collection</a></dd>
	<dd><a href='/manual/en/features.dtrace.php'>DTrace Dynamic Tracing</a></dd>
</dl>
<dl>
<dt><a href='/manual/en/funcref.php'>Function Reference</a></dt>
	<dd><a href='/manual/en/refs.basic.php.php'>Affecting PHP's Behaviour</a></dd>
	<dd><a href='/manual/en/refs.utilspec.audio.php'>Audio Formats Manipulation</a></dd>
	<dd><a href='/manual/en/refs.remote.auth.php'>Authentication Services</a></dd>
	<dd><a href='/manual/en/refs.utilspec.cmdline.php'>Command Line Specific Extensions</a></dd>
	<dd><a href='/manual/en/refs.compression.php'>Compression and Archive Extensions</a></dd>
	<dd><a href='/manual/en/refs.creditcard.php'>Credit Card Processing</a></dd>
	<dd><a href='/manual/en/refs.crypto.php'>Cryptography Extensions</a></dd>
	<dd><a href='/manual/en/refs.database.php'>Database Extensions</a></dd>
	<dd><a href='/manual/en/refs.calendar.php'>Date and Time Related Extensions</a></dd>
	<dd><a href='/manual/en/refs.fileprocess.file.php'>File System Related Extensions</a></dd>
	<dd><a href='/manual/en/refs.international.php'>Human Language and Character Encoding Support</a></dd>
	<dd><a href='/manual/en/refs.utilspec.image.php'>Image Processing and Generation</a></dd>
	<dd><a href='/manual/en/refs.remote.mail.php'>Mail Related Extensions</a></dd>
	<dd><a href='/manual/en/refs.math.php'>Mathematical Extensions</a></dd>
	<dd><a href='/manual/en/refs.utilspec.nontext.php'>Non-Text MIME Output</a></dd>
	<dd><a href='/manual/en/refs.fileprocess.process.php'>Process Control Extensions</a></dd>
	<dd><a href='/manual/en/refs.basic.other.php'>Other Basic Extensions</a></dd>
	<dd><a href='/manual/en/refs.remote.other.php'>Other Services</a></dd>
	<dd><a href='/manual/en/refs.search.php'>Search Engine Extensions</a></dd>
	<dd><a href='/manual/en/refs.utilspec.server.php'>Server Specific Extensions</a></dd>
	<dd><a href='/manual/en/refs.basic.session.php'>Session Extensions</a></dd>
	<dd><a href='/manual/en/refs.basic.text.php'>Text Processing</a></dd>
	<dd><a href='/manual/en/refs.basic.vartype.php'>Variable and Type Related Extensions</a></dd>
	<dd><a href='/manual/en/refs.webservice.php'>Web Services</a></dd>
	<dd><a href='/manual/en/refs.utilspec.windows.php'>Windows Only Extensions</a></dd>
	<dd><a href='/manual/en/refs.xml.php'>XML Manipulation</a></dd>
	<dd><a href='/manual/en/refs.ui.php'>GUI Extensions</a></dd>
</dl>
<dl>
<dt>Keyboard Shortcuts</dt><dt>?</dt>
<dd>This help</dd>
<dt>j</dt>
<dd>Next menu item</dd>
<dt>k</dt>
<dd>Previous menu item</dd>
<dt>g p</dt>
<dd>Previous man page</dd>
<dt>g n</dt>
<dd>Next man page</dd>
<dt>G</dt>
<dd>Scroll to bottom</dd>
<dt>g g</dt>
<dd>Scroll to top</dd>
<dt>g h</dt>
<dd>Goto homepage</dd>
<dt>g s</dt>
<dd>Goto search<br>(current page)</dd>
<dt>/</dt>
<dd>Focus search box</dd>
</dl></div></nav>
<div id="goto">
    <div class="search">
         <div class="text"></div>
         <div class="results"><ul></ul></div>
   </div>
</div>





<div id="layout" class="clearfix">
  <section id="layout-content">
<h1>PHP 7 ChangeLog</h1>

<a href="#PHP_7_4">7.4</a> |
<a href="#PHP_7_3">7.3</a> | <a href="#PHP_7_2">7.2</a> |
<a href="#PHP_7_1">7.1</a> | <a href="#PHP_7_0">7.0</a>

<a name="PHP_7_4"></a>
<section class="version" id="7.4.5"><!-- {{{ 7.4.5 -->
<h3>Version 7.4.5</h3>
<b><time class='releasedate' datetime='2020-04-16'>16 Apr 2020</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79364">#79364</a> (When copy empty array, next key is unspecified).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78210">#78210</a> (Invalid pointer address).</li>
</ul></li>
<li>CURL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79199">#79199</a> (curl_copy_handle() memory leak).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79396">#79396</a> (DateTime hour incorrect during DST jump forward).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74940">#74940</a> (DateTimeZone loose comparison always true).</li>
</ul></li>
<li>FPM:
<ul>
  <li>Implement request <a href="http://bugs.php.net/77062">#77062</a> (Allow numeric [UG]ID in FPM listen.{owner,group}) (Andre Nathan)</li>
</ul></li>
<li>Iconv:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79200">#79200</a> (Some iconv functions cut Windows-1258).</li>
</ul></li>
<li>OPcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79412">#79412</a> (Opcache chokes and uses 100% CPU on specific script).</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79413">#79413</a> (session_create_id() fails for active sessions).</li>
</ul></li>
<li>Shmop:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79427">#79427</a> (Integer Overflow in shmop_open()).</li>
</ul></li>
<li>SimpleXML:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/61597">#61597</a> (SXE properties may lack attributes and content).</li>
</ul></li>
<li>SOAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79357">#79357</a> (SOAP request segfaults when any request parameter is missing).</li>
</ul></li>
<li>Spl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75673">#75673</a> (SplStack::unserialize() behavior).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79393">#79393</a> (Null coalescing operator failing with SplFixedArray).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79330">#79330</a> (shell_exec() silently truncates after a null byte).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79410">#79410</a> (system() swallows last chunk if it is exactly 4095 bytes without newline).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79465">#79465</a> (OOB Read in urldecode()). (CVE-2020-7067)</li>
</ul></li>
<li>Zip:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79296">#79296</a> (ZipArchive::open fails on empty file).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79424">#79424</a> (php_zip_glob uses gl_pathc after call to globfree).</li>
</ul></li>
</ul>
<!-- }}} --></section>


<section class="version" id="7.4.4"><!-- {{{ 7.4.4 -->
<h3>Version 7.4.4</h3>
<b><time class='releasedate' datetime='2020-03-19'>19 Mar 2020</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79329">#79329</a> (get_headers() silently truncates after a null byte) (CVE-2020-7066)</li>
  <li>Fixed bug <a href="http://bugs.php.net/79244">#79244</a> (php crashes during parsing INI file).</li>
  <li>Fixed bug <a href="http://bugs.php.net/63206">#63206</a> (restore_error_handler does not restore previous errors mask).</li>
</ul></li>
<li>COM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/66322">#66322</a> (COMPersistHelper::SaveToFile can save to wrong location).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79242">#79242</a> (COM error constants don't match com_exception codes on x86).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79247">#79247</a> (Garbage collecting variant objects segfaults).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79248">#79248</a> (Traversing empty VT_ARRAY throws com_exception).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79299">#79299</a> (com_print_typeinfo prints duplicate variables).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79332">#79332</a> (php_istreams are never freed).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79333">#79333</a> (com_print_typeinfo() leaks memory).</li>
</ul></li>
<li>CURL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79019">#79019</a> (Copied cURL handles upload empty file).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79013">#79013</a> (Content-Length missing when posting a curlFile with curl).</li>
</ul></li>
<li>DOM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77569">#77569</a>: (Write Access Violation in DomImplementation).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79271">#79271</a> (DOMDocumentType::$childNodes is NULL).</li>
</ul></li>
<li>Enchant:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79311">#79311</a> (enchant_dict_suggest() fails on big endian architecture).</li>
</ul></li>
<li>EXIF:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79282">#79282</a> (Use-of-uninitialized-value in exif) (CVE-2020-7064).</li>
</ul></li>
<li>Fileinfo:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79283">#79283</a> (Segfault in libmagic patch contains a buffer overflow).</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77653">#77653</a> (operator displayed instead of the real error message).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79014">#79014</a> (PHP-FPM &amp; Primary script unknown).</li>
</ul></li>
<li>MBstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79371">#79371</a> (mb_strtolower (UTF-32LE): stack-buffer-overflow at php_unicode_tolower_full) (CVE-2020-7065).</li>
</ul></li>
<li>MySQLi:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/64032">#64032</a> (mysqli reports different client_version).</li>
</ul></li>
<li>MySQLnd:
<ul>
  <li>Implemented FR <a href="http://bugs.php.net/79275">#79275</a> (Support auth_plugin_caching_sha2_password on Windows).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79252">#79252</a> (preloading causes php-fpm to segfault during exit).</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79188">#79188</a> (Memory corruption in preg_replace/preg_replace_callback and unicode).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79241">#79241</a> (Segmentation fault on preg_match()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79257">#79257</a> (Duplicate named groups (?J) prefer last alternative even if not matched).</li>
</ul></li>
<li>PDO_ODBC:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79038">#79038</a> (PDOStatement::nextRowset() leaks column values).</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79062">#79062</a> (Property with heredoc default value returns false for getDocComment).</li>
</ul></li>
<li>SQLite3:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79294">#79294</a> (::columnType() may fail after SQLite3Stmt::reset()).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79254">#79254</a> (getenv() w/o arguments not showing changes).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79265">#79265</a> (Improper injection of Host header when using fopen for http requests).</li>
</ul></li>
<li>Zip:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79315">#79315</a> (ZipArchive::addFile doesn't honor start/length parameters).</li>
</ul></li>
</ul>
<!-- }}} --></section>


<section class="version" id="7.4.3"><!-- {{{ 7.4.3 -->
<h3>Version 7.4.3</h3>
<b><time class='releasedate' datetime='2020-02-20'>20 Feb 2020</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79146">#79146</a> (cscript can fail to run on some systems).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79155">#79155</a> (Property nullability lost when using multiple property definition).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78323">#78323</a> (Code 0 is returned on invalid options).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78989">#78989</a> (Delayed variance check involving trait segfaults).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79174">#79174</a> (cookie values with spaces fail to round-trip).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76047">#76047</a> (Use-after-free when accessing already destructed backtrace arguments).</li>
</ul></li>
<li>COM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79247">#79247</a> (Garbage collecting variant objects segfaults).</li>
</ul></li>
<li>CURL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79078">#79078</a> (Hypothetical use-after-free in curl_multi_add_handle()).</li>
</ul></li>
<li>FFI:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79096">#79096</a> (FFI Struct Segfault).</li>
</ul></li>
<li>IMAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79112">#79112</a> (IMAP extension can't find OpenSSL libraries at configure time).</li>
</ul></li>
<li>Intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79212">#79212</a> (NumberFormatter::format() may detect wrong type).</li>
</ul></li>
<li>Libxml:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79191">#79191</a> (Error in SoapClient ctor disables DOMDocument::save()).</li>
</ul></li>
<li>MBString:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79149">#79149</a> (SEGV in mb_convert_encoding with non-string encodings).</li>
</ul></li>
<li>MySQLi:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78666">#78666</a> (Properties may emit a warning on var_dump()).</li>
</ul></li>
<li>MySQLnd:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79084">#79084</a> (mysqlnd may fetch wrong column indexes with MYSQLI_BOTH).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79011">#79011</a> (MySQL caching_sha2_password Access denied for password with more than 20 chars).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79114">#79114</a> (Eval class during preload causes class to be only half available).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79128">#79128</a> (Preloading segfaults if preload_user is used).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79193">#79193</a> (Incorrect type inference for self::$field =&amp; $field).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79145">#79145</a> (openssl memory leak).</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79082">#79082</a> (Files added to tar with Phar::buildFromIterator have all-access permissions). (CVE-2020-7063)</li>
  <li>Fixed bug <a href="http://bugs.php.net/79171">#79171</a> (heap-buffer-overflow in phar_extract_file). (CVE-2020-7061)</li>
  <li>Fixed bug <a href="http://bugs.php.net/76584">#76584</a> (PharFileInfo::decompress not working).</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79115">#79115</a> (ReflectionClass::isCloneable call reflected class __destruct).</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79221">#79221</a> (Null Pointer Dereference in PHP Session Upload Progress). (CVE-2020-7062)</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78902">#78902</a> (Memory leak when using stream_filter_append).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78969">#78969</a> (PASSWORD_DEFAULT should match PASSWORD_BCRYPT instead of being null).</li>
</ul></li>
<li>Testing:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78090">#78090</a> (bug45161.phpt takes forever to finish).</li>
</ul></li>
<li>XSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70078">#70078</a> (XSL callbacks with nodes as parameter leak memory).</li>
</ul></li>
<li>Zip:
<ul>
  <li>Add ZipArchive::CM_LZMA2 and ZipArchive::CM_XZ constants (since libzip 1.6.0).</li>
  <li>Add ZipArchive::RDONLY (since libzip 1.0.0).</li>
  <li>Add ZipArchive::ER_* missing constants.</li>
  <li>Add ZipArchive::LIBZIP_VERSION constant.</li>
  <li>Fixed bug <a href="http://bugs.php.net/73119">#73119</a> (Wrong return for ZipArchive::addEmptyDir Method).</li>
</ul></li>
</ul>
<!-- }}} --></section>


<section class="version" id="7.4.2"><!-- {{{ 7.4.2 -->
<h3>Version 7.4.2</h3>
<b><time class='releasedate' datetime='2020-01-23'>23 Jan 2020</time></b>
<ul><li>Core:
<ul>
  <li>Preloading support on Windows has been disabled.</li>
  <li>Fixed bug <a href="http://bugs.php.net/79022">#79022</a> (class_exists returns True for classes that are not ready to be used).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78929">#78929</a> (plus signs in cookie values are converted to spaces).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78973">#78973</a> (Destructor during CV freeing causes segfault if opline never saved).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78776">#78776</a> (Abstract method implementation from trait does not check "static").</li>
  <li>Fixed bug <a href="http://bugs.php.net/78999">#78999</a> (Cycle leak when using function result as temporary).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79008">#79008</a> (General performance regression with PHP 7.4 on Windows).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79002">#79002</a> (Serializing uninitialized typed properties with __sleep makes unserialize throw).</li>
</ul></li>
<li>CURL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79033">#79033</a> (Curl timeout error with specific url and post).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79063">#79063</a> (curl openssl does not respect PKG_CONFIG_PATH).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79015">#79015</a> (undefined-behavior in php_date.c).</li>
</ul></li>
<li>DBA:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78808">#78808</a> ([LMDB] MDB_MAP_FULL: Environment mapsize limit reached).</li>
</ul></li>
<li>Exif:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79046">#79046</a> (NaN to int cast undefined behavior in exif).</li>
</ul></li>
<li>Fileinfo:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74170">#74170</a> (locale information change after mime_content_type).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79067">#79067</a> (gdTransformAffineCopy() may use unitialized values).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79068">#79068</a> (gdTransformAffineCopy() changes interpolation method).</li>
</ul></li>
<li>Libxml:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79029">#79029</a> (Use After Free's in XMLReader / XMLWriter).</li>
</ul></li>
<li>Mbstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79037">#79037</a> (global buffer-overflow in `mbfl_filt_conv_big5_wchar`). (CVE-2020-7060)</li>
</ul></li>
<li>OPcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78961">#78961</a> (erroneous optimization of re-assigned $GLOBALS).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78950">#78950</a> (Preloading trait method with static variables).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78903">#78903</a> (Conflict in RTD key for closures results in crash).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78986">#78986</a> (Opcache segfaults when inheriting ctor from immutable into mutable class).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79040">#79040</a> (Warning Opcode handlers are unusable due to ASLR).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79055">#79055</a> (Typed property become unknown with OPcache file cache).</li>
</ul></li>
<li>Pcntl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78402">#78402</a> (Converting null to string in error message is bad DX).</li>
</ul></li>
<li>PDO_PgSQL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78983">#78983</a> (pdo_pgsql config.w32 cannot find libpq-fe.h).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78980">#78980</a> (pgsqlGetNotify() overlooks dead connection).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78982">#78982</a> (pdo_pgsql returns dead persistent connection).</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79091">#79091</a> (heap use-after-free in session_create_id()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79031">#79031</a> (Session unserialization problem).</li>
</ul></li>
<li>Shmop:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78538">#78538</a> (shmop memory leak).</li>
</ul></li>
<li>Sqlite3:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79056">#79056</a> (sqlite does not respect PKG_CONFIG_PATH during compilation).</li>
</ul></li>
<li>Spl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78976">#78976</a> (SplFileObject::fputcsv returns -1 on failure).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79099">#79099</a> (OOB read in php_strip_tags_ex). (CVE-2020-7059)</li>
  <li>Fixed bug <a href="http://bugs.php.net/79000">#79000</a> (Non-blocking socket stream reports EAGAIN as error).</li>
  <li>Fixed bug <a href="http://bugs.php.net/54298">#54298</a> (Using empty additional_headers adding extraneous CRLF).</li>
</ul></li>
</ul>
<!-- }}} --></section>


<section class="version" id="7.4.1"><!-- {{{ 7.4.1 -->
<h3>Version 7.4.1</h3>
<b><time class='releasedate' datetime='2019-12-18'>18 Dec 2019</time></b>
<ul><li>Bcmath:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78878">#78878</a> (Buffer underflow in bc_shift_addsub). (CVE-2019-11046).</li>
</ul></li>
<li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78862">#78862</a> (link() silently truncates after a null byte on Windows). (CVE-2019-11044).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78863">#78863</a> (DirectoryIterator class silently truncates after a null byte). (CVE-2019-11045).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78943">#78943</a> (mail() may release string with refcount==1 twice). (CVE-2019-11049).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78810">#78810</a> (RW fetches do not throw "uninitialized property" exception).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78868">#78868</a> (Calling __autoload() with incorrect EG(fake_scope) value).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78296">#78296</a> (is_file fails to detect file).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78883">#78883</a> (fgets(STDIN) fails on Windows).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78898">#78898</a> (call_user_func(['parent', ...]) fails while other succeed).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78904">#78904</a> (Uninitialized property triggers __get()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78926">#78926</a> (Segmentation fault on Symfony cache:clear).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78849">#78849</a> (GD build broken with -D SIGNED_COMPARE_SLOW).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78923">#78923</a> (Artifacts when convoluting image with transparency).</li>
</ul></li>
<li>EXIF:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78793">#78793</a> (Use-after-free in exif parsing under memory sanitizer). (CVE-2019-11050).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78910">#78910</a> (Heap-buffer-overflow READ in exif). (CVE-2019-11047).</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76601">#76601</a> (Partially working php-fpm ater incomplete reload).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78889">#78889</a> (php-fpm service fails to start).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78916">#78916</a> (php-fpm 7.4.0 don't send mail via mail()).</li>
</ul></li>
<li>Intl:
<ul>
  <li>Implemented FR <a href="http://bugs.php.net/78912">#78912</a> (INTL Support for accounting format).</li>
</ul></li>
<li>Mysqlnd:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78823">#78823</a> (ZLIB_LIBS not added to EXTRA_LIBS).</li>
</ul></li>
<li>OPcache:
<ul>
  <li>Fixed $x = (bool)$x; with opcache (should emit undeclared variable notice).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78935">#78935</a> (Preloading removes classes that have dependencies).</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78853">#78853</a> (preg_match() may return integer &gt; 1).</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78895">#78895</a> (Reflection detects abstract non-static class as abstract static. IS_IMPLICIT_ABSTRACT is not longer used).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77638">#77638</a> (var_export'ing certain class instances segfaults).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78840">#78840</a> (imploding $GLOBALS crashes).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78833">#78833</a> (Integer overflow in pack causes out-of-bound access).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78814">#78814</a> (strip_tags allows / in tag name =&gt; whitelist bypass).</li>
</ul></li>
</ul>
<!-- }}} --></section>


<section class="version" id="7.4.0"><!-- {{{ 7.4.0 -->
<h3>Version 7.4.0</h3>
<b><time class='releasedate' datetime='2019-11-28'>28 Nov 2019</time></b>
<ul>
<li>Core:
  <ul>
    <li>Implemented RFC: <a href="https://wiki.php.net/rfc/deprecate_curly_braces_array_access">Deprecate curly brace syntax for accessing array elements and string offsets</a>.</li>
    <li>Implemented RFC: <a href="https://wiki.php.net/rfc/deprecations_php_7_4">Deprecations for PHP 7.4</a>.</li>
    <li>Fixed bug <a href="http://bugs.php.net/52752">#52752</a> (Crash when lexing).</li>
    <li>Fixed bug <a href="http://bugs.php.net/60677">#60677</a> (CGI doesn't properly validate shebang line contains #!).</li>
    <li>Fixed bug <a href="http://bugs.php.net/71030">#71030</a> (Self-assignment in list() may have inconsistent behavior).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72530">#72530</a> (Use After Free in GC with Certain Destructors).</li>
    <li>Fixed bug <a href="http://bugs.php.net/75921">#75921</a> (Inconsistent: No warning in some cases when stdObj is created on the fly).</li>
    <li>Implemented FR <a href="http://bugs.php.net/76148">#76148</a> (Add array_key_exists() to the list of specially compiled functions).</li>
    <li>Fixed bug <a href="http://bugs.php.net/76430">#76430</a> (__METHOD__ inconsistent outside of method).</li>
    <li>Fixed bug <a href="http://bugs.php.net/76451">#76451</a> (Aliases during inheritance type checks affected by opcache).</li>
    <li>Implemented FR <a href="http://bugs.php.net/77230">#77230</a> (Support custom CFLAGS and LDFLAGS from environment).</li>
    <li>Fixed bug <a href="http://bugs.php.net/77345">#77345</a> (Stack Overflow caused by circular reference in garbage collection).</li>
    <li>Fixed bug <a href="http://bugs.php.net/77812">#77812</a> (Interactive mode does not support PHP 7.3-style heredoc).</li>
    <li>Fixed bug <a href="http://bugs.php.net/77877">#77877</a> (call_user_func() passes $this to static methods).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78066">#78066</a> (PHP eats the first byte of a program that comes from process substitution).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78151">#78151</a> (Segfault caused by indirect expressions in PHP 7.4a1).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78154">#78154</a> (SEND_VAR_NO_REF does not always send reference).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78182">#78182</a> (Segmentation fault during by-reference property assignment).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78212">#78212</a> (Segfault in built-in webserver).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78220">#78220</a> (Can't access OneDrive folder).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78226">#78226</a> (Unexpected __set behavior with typed properties).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78239">#78239</a> (Deprecation notice during string conversion converted to exception hangs).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78335">#78335</a> (Static properties/variables containing cycles report as leak).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78340">#78340</a> (Include of stream wrapper not reading whole file).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78344">#78344</a> (Segmentation fault on zend_check_protected).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78356">#78356</a> (Array returned from ArrayAccess is incorrectly unpacked as argument).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78379">#78379</a> (Cast to object confuses GC, causes crash).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78386">#78386</a> (fstat mode has unexpected value on PHP 7.4).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78396">#78396</a> (Second file_put_contents in Shutdown hangs script).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78406">#78406</a> (Broken file includes with user-defined stream filters).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78438">#78438</a> (Corruption when __unserializing deeply nested structures).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78441">#78441</a> (Parse error due to heredoc identifier followed by digit).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78454">#78454</a> (Consecutive numeric separators cause OOM error).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78460">#78460</a> (PEAR installation failure).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78531">#78531</a> (Crash when using undefined variable as object).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78535">#78535</a> (auto_detect_line_endings value not parsed as bool).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78604">#78604</a> (token_get_all() does not properly tokenize FOO&lt;?php with short_open_tag=0).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78614">#78614</a> (Does not compile with DTRACE anymore).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78620">#78620</a> (Out of memory error).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78632">#78632</a> (method_exists() in php74 works differently from php73 in checking priv. methods).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78644">#78644</a> (SEGFAULT in ZEND_UNSET_OBJ_SPEC_VAR_CONST_HANDLER).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78658">#78658</a> (Memory corruption using Closure::bindTo).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78656">#78656</a> (Parse errors classified as highest log-level).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78662">#78662</a> (stream_write bad error detection).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78768">#78768</a> (redefinition of typedef zend_property_info).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78788">#78788</a> (./configure generates invalid php_version.h).</li>
    <li>Fixed incorrect usage of QM_ASSIGN instruction. It must not return IS_VAR. As a side effect, this allowed passing left hand list() "by reference", instead of compile-time error.</li>
  </ul>

<li>CLI:
  <ul>
    <li>The built-in CLI server now reports the request method in log files.</li>
  </ul>

<li>COM:
  <ul>
    <li>Deprecated registering of case-insensitive constants from typelibs.</li>
    <li>Fixed bug <a href="http://bugs.php.net/78650">#78650</a> (new COM Crash).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78694">#78694</a> (Appending to a variant array causes segfault).</li>
  </ul>

<li>CURL:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/76480">#76480</a> (Use curl_multi_wait() so that timeouts are respected).</li>
    <li>Implemented FR <a href="http://bugs.php.net/77711">#77711</a> (CURLFile should support UNICODE filenames).</li>
    <li>Deprecated CURLPIPE_HTTP1.</li>
    <li>Deprecated $version parameter of curl_version().</li>
  </ul>

<li>Date:
  <ul>
    <li>Updated timelib to 2018.02.</li>
    <li>Fixed bug <a href="http://bugs.php.net/69044">#69044</a> (discrepency between time and microtime).</li>
    <li>Fixed bug <a href="http://bugs.php.net/70153">#70153</a> (\DateInterval incorrectly unserialized).</li>
    <li>Fixed bug <a href="http://bugs.php.net/75232">#75232</a> (print_r of DateTime creating side-effect).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78383">#78383</a> (Casting a DateTime to array no longer returns its properties).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78751">#78751</a> (Serialising DatePeriod converts DateTimeImmutable).</li>
  </ul>

<li>Exif:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/78333">#78333</a> (Exif crash (bus error) due to wrong alignment and invalid cast).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78256">#78256</a> (heap-buffer-overflow on exif_process_user_comment). (CVE-2019-11042)</li>
    <li>Fixed bug <a href="http://bugs.php.net/78222">#78222</a> (heap-buffer-overflow on exif_scan_thumbnail). (CVE-2019-11041)</li>
  </ul>

<li>Fileinfo:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/78075">#78075</a> (finfo_file treats JSON file as text/plain).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78183">#78183</a> (finfo_file shows wrong mime-type for .tga file).</li>
  </ul>

<li>Filter:
  <ul>
    <li>The filter extension no longer has the --with-pcre-dir on Unix builds, allowing the extension to be once more compiled as shared using ./configure.</li>
    <li>Added min_range and max_range options for FILTER_VALIDATE_FLOAT.</li>
  </ul>

<li>FFI:
  <ul>
    <li>Added FFI extension.</li>
    <li>Fixed bug <a href="http://bugs.php.net/78488">#78488</a> (OOB in ZEND_FUNCTION(ffi_trampoline)).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78543">#78543</a> (is_callable() on FFI\CData throws Exception).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78716">#78716</a> (Function name mangling is wrong for some parameter types).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78762">#78762</a> (Failing FFI::cast() may leak memory).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78761">#78761</a> (Zend memory heap corruption with preload and casting).</li>
    <li>Implement FR <a href="http://bugs.php.net/78270">#78270</a> (Support __vectorcall convention with FFI).</li>
    <li>Added missing FFI::isNull().</li>
  </ul>

<li>FPM:
  <ul>
    <li>Implemented FR <a href="http://bugs.php.net/72510">#72510</a> (systemd service should be hardened).</li>
    <li>Fixed bug <a href="http://bugs.php.net/74083">#74083</a> (master PHP-fpm is stopped on multiple reloads).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78334">#78334</a> (fpm log prefix message includes wrong stdout/stderr notation).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78599">#78599</a> (env_path_info underflow in fpm_main.c can lead to RCE). (CVE-2019-11043)</li>
  </ul>

<li>GD:
  <ul>
    <li>Implemented the scatter filter (IMG_FILTER_SCATTER).</li>
    <li>The bundled libgd behaves now like system libgd wrt. IMG_CROP_DEFAULT never falling back to IMG_CROP_SIDES.</li>
    <li>The default $mode parameter of imagecropauto() has been changed to IMG_CROP_DEFAULT; passing -1 is now deprecated.</li>
    <li>Added support for aspect ratio preserving scaling to a fixed height for imagescale().</li>
    <li>Added TGA read support.</li>
    <li>Fixed bug <a href="http://bugs.php.net/73291">#73291</a> (imagecropauto() $threshold differs from external libgd).</li>
    <li>Fixed bug <a href="http://bugs.php.net/76324">#76324</a> (cannot detect recent versions of freetype with pkg-config).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78314">#78314</a> (missing freetype support/functions with external gd).</li>
  </ul>

<li>GMP:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/78574">#78574</a> (broken shared build).</li>
  </ul>

<li>Hash:
  <ul>
    <li>Implemented RFC: <a href="https://wiki.php.net/rfc/permanent_hash_ext">The hash extension is now an integral part of PHP and cannot be disabled</a>.</li>
    <li>Implemented FR <a href="http://bugs.php.net/71890">#71890</a> (crc32c checksum algorithm).</li>
  </ul>

<li>Iconv:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/78342">#78342</a> (Bus error in configure test for iconv //IGNORE).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78642">#78642</a> (Wrong libiconv version displayed).</li>
  </ul>

<li>Libxml:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/78279">#78279</a> (libxml_disable_entity_loader settings is shared between requests (cgi-fcgi)).</li>
  </ul>

<li>InterBase:
  <ul>
    <li>Unbundled the InterBase extension and moved it to PECL.</li>
  </ul>

<li>Intl:
  <ul>
    <li>Raised requirements to ICU ≥ 50.1.</li>
    <li>Changed ResourceBundle to implement Countable.</li>
    <li>Changed default of $variant parameter of idn_to_ascii() and idn_to_utf8().</li>
  </ul>

<li>LDAP:
  <ul>
    <li>Deprecated ldap_control_paged_result_response and ldap_control_paged_result</li>
  </ul>

<li>LiteSpeed:
  <ul>
    <li>Updated to LiteSpeed SAPI V7.5 (Fixed clean shutdown).</li>
    <li>Updated to LiteSpeed SAPI V7.4.3 (increased response header count limit from 100 to 1000, added crash handler to cleanly shutdown PHP request, added CloudLinux mod_lsapi mode).</li>
    <li>Fixed bug <a href="http://bugs.php.net/76058">#76058</a> (After "POST data can't be buffered", using php://input makes huge tmp files).</li>
  </ul>

<li>MBString:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/77907">#77907</a> (mb-functions do not respect default_encoding).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78579">#78579</a> (mb_decode_numericentity: args number inconsistency).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78609">#78609</a> (mb_check_encoding() no longer supports stringable objects).</li>
  </ul>

<li>MySQLi:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/67348">#67348</a> (Reading $dbc-&gt;stat modifies $dbc-&gt;affected_rows).</li>
    <li>Fixed bug <a href="http://bugs.php.net/76809">#76809</a> (SSL settings aren't respected when persistent connections are used).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78179">#78179</a> (MariaDB server version incorrectly detected).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78213">#78213</a> (Empty row pocket).</li>
  </ul>

<li>MySQLnd:
  <ul>
    <li>Fixed connect_attr issues and added the _server_host connection attribute.</li>
    <li>Fixed bug <a href="http://bugs.php.net/60594">#60594</a> (mysqlnd exposes 160 lines of stats in phpinfo).</li>
  </ul>

<li>ODBC:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/78473">#78473</a> (odbc_close() closes arbitrary resources).</li>
  </ul>

<li>Opcache:
  <ul>
    <li>Implemented <a href="https://wiki.php.net/rfc/preload">preloading RFC</a>.</li>
    <li>Add opcache.preload_user INI directive.</li>
    <li>Added new INI directive opcache.cache_id (Windows only).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78106">#78106</a> (Path resolution fails if opcache disabled during request).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78175">#78175</a> (Preloading segfaults at preload time and at runtime).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78202">#78202</a> (Opcache stats for cache hits are capped at 32bit NUM).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78271">#78271</a> (Invalid result of if-else).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78341">#78341</a> (Failure to detect smart branch in DFA pass).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78376">#78376</a> (Incorrect preloading of constant static properties).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78429">#78429</a> (opcache_compile_file(__FILE__); segfaults).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78512">#78512</a> (Cannot make preload work).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78514">#78514</a> (Preloading segfaults with inherited typed property).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78654">#78654</a> (Incorrectly computed opcache checksum on files with non-ascii characters).</li>
  </ul>

<li>OpenSSL:
  <ul>
    <li>Added TLS 1.3 support to streams including new tlsv1.3 stream.</li>
    <li>Added openssl_x509_verify function.</li>
    <li>openssl_random_pseudo_bytes() now throws in error conditions.</li>
    <li>Changed the default config path (Windows only).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78231">#78231</a> (Segmentation fault upon stream_socket_accept of exported socket-to-stream).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78391">#78391</a> (Assertion failure in openssl_random_pseudo_bytes).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78775">#78775</a> (TLS issues from HTTP request affecting other encrypted connections).</li>
  </ul>

<li>Pcntl:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/77335">#77335</a> (PHP is preventing SIGALRM from specifying SA_RESTART).</li>
  </ul>

<li>PCRE:
  <ul>
    <li>Implemented FR <a href="http://bugs.php.net/77094">#77094</a> (Support flags in preg_replace_callback).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72685">#72685</a> (Repeated UTF-8 validation of same string in UTF-8 mode).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73948">#73948</a> (Preg_match_all should return NULLs on trailing optional capture groups).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78338">#78338</a> (Array cross-border reading in PCRE).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78349">#78349</a> (Bundled pcre2 library missing LICENCE file).</li>
  </ul>

<li>PDO:
  <ul>
    <li>Implemented FR <a href="http://bugs.php.net/71885">#71885</a> (Allow escaping question mark placeholders). https://wiki.php.net/rfc/pdo_escape_placeholders</li>
    <li>Fixed bug <a href="http://bugs.php.net/77849">#77849</a> (Disable cloning of PDO handle/connection objects).</li>
    <li>Implemented FR <a href="http://bugs.php.net/78033">#78033</a> (PDO - support username and password specified in DSN).</li>
  </ul>

<li>PDO_Firebird:
  <ul>
    <li>Implemented FR <a href="http://bugs.php.net/65690">#65690</a> (PDO_Firebird should also support dialect 1).</li>
    <li>Implemented FR <a href="http://bugs.php.net/77863">#77863</a> (PDO firebird support type Boolean in input parameters).</li>
  </ul>

<li>PDO_MySQL:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/41997">#41997</a> (SP call yields additional empty result set).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78623">#78623</a> (Regression caused by "SP call yields additional empty result set").</li>
  </ul>

<li>PDO_OCI:
  <ul>
    <li>Support Oracle Database tracing attributes ACTION, MODULE, CLIENT_INFO, and CLIENT_IDENTIFIER.</li>
    <li>Implemented FR <a href="http://bugs.php.net/76908">#76908</a> (PDO_OCI getColumnMeta() not implemented).</li>
  </ul>

<li>PDO_SQLite:
  <ul>
    <li>Implemented sqlite_stmt_readonly in PDO_SQLite.</li>
    <li>Raised requirements to SQLite 3.5.0.</li>
    <li>Fixed bug <a href="http://bugs.php.net/78192">#78192</a> (SegFault when reuse statement after schema has changed).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78348">#78348</a> (Remove -lrt from pdo_sqlite.so).</li>
  </ul>

<li>Phar:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/77919">#77919</a> (Potential UAF in Phar RSHUTDOWN).</li>
  </ul>

<li>phpdbg:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/76596">#76596</a> (phpdbg support for display_errors=stderr).</li>
    <li>Fixed bug <a href="http://bugs.php.net/76801">#76801</a> (too many open files).</li>
    <li>Fixed bug <a href="http://bugs.php.net/77800">#77800</a> (phpdbg segfaults on listing some conditional breakpoints).</li>
    <li>Fixed bug <a href="http://bugs.php.net/77805">#77805</a> (phpdbg build fails when readline is shared).</li>
  </ul>

<li>Recode:
  <ul>
    <li>Unbundled the recode extension.</li>
  </ul>

<li>Reflection:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/76737">#76737</a> (Unserialized reflection objects are broken, they shouldn't be serializable).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78263">#78263</a> (\ReflectionReference::fromArrayElement() returns null while item is a reference).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78410">#78410</a> (Cannot "manually" unserialize class that is final and extends an internal one).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78697">#78697</a> (ReflectionClass::implementsInterface - inaccurate error message with traits).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78774">#78774</a> (ReflectionNamedType on Typed Properties Crash).</li>
  </ul>

<li>Session:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/78624">#78624</a> (session_gc return value for user defined session handlers).</li>
  </ul>

<li>SimpleXML:
  <ul>
    <li>Implemented FR <a href="http://bugs.php.net/65215">#65215</a> (SimpleXMLElement could register as implementing Countable).</li>
    <li>Fixed bug <a href="http://bugs.php.net/75245">#75245</a> (Don't set content of elements with only whitespaces).</li>
  </ul>

<li>Sockets:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/67619">#67619</a> (Validate length on socket_write).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78665">#78665</a> (Multicasting may leak memory).</li>
  </ul>

<li>sodium:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/77646">#77646</a> (sign_detached() strings not terminated).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78510">#78510</a> (Partially uninitialized buffer returned by sodium_crypto_generichash_init()).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78516">#78516</a> (password_hash(): Memory cost is not in allowed range).</li>
  </ul>

<li>SPL:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/77518">#77518</a> (SeekableIterator::seek() should accept 'int' typehint as documented).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78409">#78409</a> (Segfault when creating instance of ArrayIterator without constructor).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78436">#78436</a> (Missing addref in SplPriorityQueue EXTR_BOTH mode).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78456">#78456</a> (Segfault when serializing SplDoublyLinkedList).</li>
  </ul>

<li>SQLite3:
  <ul>
    <li>Unbundled libsqlite.</li>
    <li>Raised requirements to SQLite 3.7.4.</li>
    <li>Forbid (un)serialization of SQLite3, SQLite3Stmt and SQLite3Result.</li>
    <li>Added support for the SQLite @name notation.</li>
    <li>Added SQLite3Stmt::getSQL() to retrieve the SQL of the statement.</li>
    <li>Implement FR ##70950 (Make SQLite3 Online Backup API available).</li>
  </ul>

<li>Standard:
  <ul>
    <li>Implemented RFC <a href="https://wiki.php.net/rfc/password_registry">password hashing registry</a>.</li>
    <li>Implemented RFC where password_hash() has <a href="https://wiki.php.net/rfc/sodium.argon.hash">argon2i(d) implementations</a> from ext/sodium when PHP is built without libargon.</li>
    <li>Implemented FR <a href="http://bugs.php.net/38301">#38301</a> (field enclosure behavior in fputcsv).</li>
    <li>Implemented FR <a href="http://bugs.php.net/51496">#51496</a> (fgetcsv should take empty string as an escape).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73535">#73535</a> (php_sockop_write() returns 0 on error, can be used to trigger Denial of Service).</li>
    <li>Fixed bug <a href="http://bugs.php.net/74764">#74764</a> (Bindto IPv6 works with file_get_contents but fails with stream_socket_client).</li>
    <li>Fixed bug <a href="http://bugs.php.net/76859">#76859</a> (stream_get_line skips data if used with data-generating filter).</li>
    <li>Implemented FR <a href="http://bugs.php.net/77377">#77377</a> (No way to handle CTRL+C in Windows).</li>
    <li>Fixed bug <a href="http://bugs.php.net/77930">#77930</a> (stream_copy_to_stream should use mmap more often).</li>
    <li>Implemented FR <a href="http://bugs.php.net/78177">#78177</a> (Make proc_open accept command array).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78208">#78208</a> (password_needs_rehash() with an unknown algo should always return true).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78241">#78241</a> (touch() does not handle dates after 2038 in PHP 64-bit).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78282">#78282</a> (atime and mtime mismatch).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78326">#78326</a> (improper memory deallocation on stream_get_contents() with fixed length buffer).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78346">#78346</a> (strip_tags no longer handling nested php tags).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78506">#78506</a> (Error in a php_user_filter::filter() is not reported).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78549">#78549</a> (Stack overflow due to nested serialized input).</li>
    <li>Fixed bug <a href="http://bugs.php.net/78759">#78759</a> (array_search in $GLOBALS).</li>
  </ul>

<li>Testing:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/78684">#78684</a> (PCRE bug72463_2 test is sending emails on Linux).</li>
  </ul>

<li>Tidy:
  <ul>
    <li>Added TIDY_TAG_* constants for HTML5 elements.</li>
    <li>Fixed bug <a href="http://bugs.php.net/76736">#76736</a> (wrong reflection for tidy_get_head, tidy_get_html, tidy_get_root, and tidy_getopt)</li>
  </ul>

<li>WDDX:
  <ul>
    <li>Deprecated and unbundled the WDDX extension.</li>
  </ul>

<li>Zip:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/78641">#78641</a> (addGlob can modify given remove_path value).</li>
  </ul>
</ul>

<!-- }}} --></section>

<a name="PHP_7_3"></a>

<section class="version" id="7.3.17"><!-- {{{ 7.3.17 -->
<h3>Version 7.3.17</h3>
<b><time class='releasedate' datetime='2020-04-16'>16 Apr 2020</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79364">#79364</a> (When copy empty array, next key is unspecified).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78210">#78210</a> (Invalid pointer address).</li>
</ul></li>
<li>CURL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79199">#79199</a> (curl_copy_handle() memory leak).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79396">#79396</a> (DateTime hour incorrect during DST jump forward).</li>
</ul></li>
<li>Iconv:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79200">#79200</a> (Some iconv functions cut Windows-1258).</li>
</ul></li>
<li>OPcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79412">#79412</a> (Opcache chokes and uses 100% CPU on specific script).</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79413">#79413</a> (session_create_id() fails for active sessions).</li>
</ul></li>
<li>Shmop:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79427">#79427</a> (Integer Overflow in shmop_open()).</li>
</ul></li>
<li>SimpleXML:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/61597">#61597</a> (SXE properties may lack attributes and content).</li>
</ul></li>
<li>Spl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75673">#75673</a> (SplStack::unserialize() behavior).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79393">#79393</a> (Null coalescing operator failing with SplFixedArray).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79330">#79330</a> (shell_exec() silently truncates after a null byte).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79465">#79465</a> (OOB Read in urldecode()). (CVE-2020-7067)</li>
  <li>Fixed bug <a href="http://bugs.php.net/79410">#79410</a> (system() swallows last chunk if it is exactly 4095 bytes without newline).</li>
</ul></li>
<li>Zip:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79296">#79296</a> (ZipArchive::open fails on empty file).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79424">#79424</a> (php_zip_glob uses gl_pathc after call to globfree).</li>
</ul></li>
</ul>
<!-- }}} --></section>



<section class="version" id="7.3.16"><!-- {{{ 7.3.16 -->
<h3>Version 7.3.16</h3>
<b><time class='releasedate' datetime='2020-03-19'>19 Mar 2020</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/63206">#63206</a> (restore_error_handler does not restore previous errors mask).</li>
</ul></li>
<li>COM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/66322">#66322</a> (COMPersistHelper::SaveToFile can save to wrong location).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79242">#79242</a> (COM error constants don't match com_exception codes on x86).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79248">#79248</a> (Traversing empty VT_ARRAY throws com_exception).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79299">#79299</a> (com_print_typeinfo prints duplicate variables).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79332">#79332</a> (php_istreams are never freed).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79333">#79333</a> (com_print_typeinfo() leaks memory).</li>
</ul></li>
<li>DOM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77569">#77569</a>: (Write Access Violation in DomImplementation).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79271">#79271</a> (DOMDocumentType::$childNodes is NULL).</li>
</ul></li>
<li>Enchant:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79311">#79311</a> (enchant_dict_suggest() fails on big endian architecture).</li>
</ul></li>
<li>EXIF:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79282">#79282</a> (Use-of-uninitialized-value in exif). (CVE-2020-7064)</li>
</ul></li>
<li>MBstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79371">#79371</a> (mb_strtolower (UTF-32LE): stack-buffer-overflow at php_unicode_tolower_full). (CVE-2020-7065)</li>
</ul></li>
<li>MySQLi:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/64032">#64032</a> (mysqli reports different client_version).</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79188">#79188</a> (Memory corruption in preg_replace/preg_replace_callback and unicode).</li>
</ul></li>
<li>PDO_ODBC:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79038">#79038</a> (PDOStatement::nextRowset() leaks column values).</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79062">#79062</a> (Property with heredoc default value returns false for getDocComment).</li>
</ul></li>
<li>SQLite3:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79294">#79294</a> (::columnType() may fail after SQLite3Stmt::reset()).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79329">#79329</a> (get_headers() silently truncates after a null byte). (CVE-2020-7066)</li>
  <li>Fixed bug <a href="http://bugs.php.net/79254">#79254</a> (getenv() w/o arguments not showing changes).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79265">#79265</a> (Improper injection of Host header when using fopen for http requests).</li>
</ul></li>
</ul>
<!-- }}} --></section>



<section class="version" id="7.3.15"><!-- {{{ 7.3.15 -->
<h3>Version 7.3.15</h3>
<b><time class='releasedate' datetime='2020-02-20'>20 Feb 2020</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71876">#71876</a> (Memory corruption htmlspecialchars(): charset `*' not supported).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79146">#79146</a> (cscript can fail to run on some systems).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78323">#78323</a> (Code 0 is returned on invalid options).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76047">#76047</a> (Use-after-free when accessing already destructed backtrace arguments).</li>
</ul></li>
<li>CURL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79078">#79078</a> (Hypothetical use-after-free in curl_multi_add_handle()).</li>
</ul></li>
<li>Intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79212">#79212</a> (NumberFormatter::format() may detect wrong type).</li>
</ul></li>
<li>Libxml:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79191">#79191</a> (Error in SoapClient ctor disables DOMDocument::save()).</li>
</ul></li>
<li>MBString:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79154">#79154</a> (mb_convert_encoding() can modify $from_encoding).</li>
</ul></li>
<li>MySQLnd:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79084">#79084</a> (mysqlnd may fetch wrong column indexes with MYSQLI_BOTH).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79145">#79145</a> (openssl memory leak).</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79082">#79082</a> (Files added to tar with Phar::buildFromIterator have all-access permissions). (CVE-2020-7063)</li>
  <li>Fixed bug <a href="http://bugs.php.net/79171">#79171</a> (heap-buffer-overflow in phar_extract_file). (CVE-2020-7061)</li>
  <li>Fixed bug <a href="http://bugs.php.net/76584">#76584</a> (PharFileInfo::decompress not working).</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79115">#79115</a> (ReflectionClass::isCloneable call reflected class __destruct).</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79221">#79221</a> (Null Pointer Dereference in PHP Session Upload Progress). (CVE-2020-7062)</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79151">#79151</a> (heap use after free caused by spl_dllist_it_helper_move_forward).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78902">#78902</a> (Memory leak when using stream_filter_append).</li>
</ul></li>
<li>Testing:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78090">#78090</a> (bug45161.phpt takes forever to finish).</li>
</ul></li>
<li>XSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70078">#70078</a> (XSL callbacks with nodes as parameter leak memory).</li>
</ul></li>
</ul>
<!-- }}} --></section>



<section class="version" id="7.3.14"><!-- {{{ 7.3.14 -->
<h3>Version 7.3.14</h3>
<b><time class='releasedate' datetime='2020-01-23'>23 Jan 2020</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78999">#78999</a> (Cycle leak when using function result as temporary).</li>
</ul></li>
<li>CURL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79033">#79033</a> (Curl timeout error with specific url and post).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79015">#79015</a> (undefined-behavior in php_date.c).</li>
</ul></li>
<li>DBA:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78808">#78808</a> ([LMDB] MDB_MAP_FULL: Environment mapsize limit reached).</li>
</ul></li>
<li>Fileinfo:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74170">#74170</a> (locale information change after mime_content_type).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78923">#78923</a> (Artifacts when convoluting image with transparency).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79067">#79067</a> (gdTransformAffineCopy() may use unitialized values).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79068">#79068</a> (gdTransformAffineCopy() changes interpolation method).</li>
</ul></li>
<li>Libxml:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79029">#79029</a> (Use After Free's in XMLReader / XMLWriter).</li>
</ul></li>
<li>Mbstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79037">#79037</a> (global buffer-overflow in `mbfl_filt_conv_big5_wchar`). (CVE-2020-7060)</li>
</ul></li>
<li>OPcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79040">#79040</a> (Warning Opcode handlers are unusable due to ASLR).</li>
</ul></li>
<li>Pcntl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78402">#78402</a> (Converting null to string in error message is bad DX).</li>
</ul></li>
<li>PDO_PgSQL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78983">#78983</a> (pdo_pgsql config.w32 cannot find libpq-fe.h).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78980">#78980</a> (pgsqlGetNotify() overlooks dead connection).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78982">#78982</a> (pdo_pgsql returns dead persistent connection).</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79091">#79091</a> (heap use-after-free in session_create_id()).</li>
</ul></li>
<li>Shmop:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78538">#78538</a> (shmop memory leak).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79099">#79099</a> (OOB read in php_strip_tags_ex). (CVE-2020-7059)</li>
  <li>Fixed bug <a href="http://bugs.php.net/54298">#54298</a> (Using empty additional_headers adding extraneous CRLF).</li>
</ul></li>
</ul>
<!-- }}} --></section>



<section class="version" id="7.3.13"><!-- {{{ 7.3.13 -->
<h3>Version 7.3.13</h3>
<b><time class='releasedate' datetime='2019-12-18'>18 Dec 2019</time></b>
<ul><li>Bcmath:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78878">#78878</a> (Buffer underflow in bc_shift_addsub). (CVE-2019-11046)</li>
</ul></li>
<li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78862">#78862</a> (link() silently truncates after a null byte on Windows). (CVE-2019-11044)</li>
  <li>Fixed bug <a href="http://bugs.php.net/78863">#78863</a> (DirectoryIterator class silently truncates after a null byte). (CVE-2019-11045)</li>
  <li>Fixed bug <a href="http://bugs.php.net/78943">#78943</a> (mail() may release string with refcount==1 twice). (CVE-2019-11049)</li>
  <li>Fixed bug <a href="http://bugs.php.net/78787">#78787</a> (Segfault with trait overriding inherited private shadow property).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78868">#78868</a> (Calling __autoload() with incorrect EG(fake_scope) value).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78296">#78296</a> (is_file fails to detect file).</li>
</ul></li>
<li>EXIF:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78793">#78793</a> (Use-after-free in exif parsing under memory sanitizer). (CVE-2019-11050)</li>
  <li>Fixed bug <a href="http://bugs.php.net/78910">#78910</a> (Heap-buffer-overflow READ in exif) (CVE-2019-11047).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78849">#78849</a> (GD build broken with -D SIGNED_COMPARE_SLOW).</li>
</ul></li>
<li>MBString:
<ul>
  <li>Upgraded bundled Oniguruma to 6.9.4.</li>
</ul></li>
<li>OPcache:
<ul>
  <li>Fixed potential ASLR related invalid opline handler issues.</li>
  <li>Fixed $x = (bool)$x; with opcache (should emit undeclared variable notice).</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78853">#78853</a> (preg_match() may return integer &gt; 1).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78759">#78759</a> (array_search in $GLOBALS).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77638">#77638</a> (var_export'ing certain class instances segfaults).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78840">#78840</a> (imploding $GLOBALS crashes).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78833">#78833</a> (Integer overflow in pack causes out-of-bound access).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78814">#78814</a> (strip_tags allows / in tag name =&gt; whitelist bypass).</li>
</ul></li>
</ul>
<!-- }}} --></section>



<section class="version" id="7.3.12"><!-- {{{ 7.3.12 -->
<h3>Version 7.3.12</h3>
<b><time class='releasedate' datetime='2019-11-21'>21 Nov 2019</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78658">#78658</a> (Memory corruption using Closure::bindTo).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78656">#78656</a> (Parse errors classified as highest log-level).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78752">#78752</a> (Segfault if GC triggered while generator stack frame is being destroyed).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78689">#78689</a> (Closure::fromCallable() doesn't handle [Closure, '__invoke']).</li>
</ul></li>
<li>COM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78694">#78694</a> (Appending to a variant array causes segfault).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70153">#70153</a> (\DateInterval incorrectly unserialized).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78751">#78751</a> (Serialising DatePeriod converts DateTimeImmutable).</li>
</ul></li>
<li>Iconv:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78642">#78642</a> (Wrong libiconv version displayed).</li>
</ul></li>
<li>OpCache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78654">#78654</a> (Incorrectly computed opcache checksum on files with non-ascii characters).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78747">#78747</a> (OpCache corrupts custom extension result).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78775">#78775</a> (TLS issues from HTTP request affecting other encrypted connections).</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78697">#78697</a> (ReflectionClass::ImplementsInterface - inaccurate error message with traits).</li>
</ul></li>
<li>Sockets:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78665">#78665</a> (Multicasting may leak memory).</li>
</ul></li>
</ul>
<!-- }}} --></section>



<section class="version" id="7.3.11"><!-- {{{ 7.3.11 -->
<h3>Version 7.3.11</h3>
<b><time class='releasedate' datetime='2019-10-24'>24 Oct 2019</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78535">#78535</a> (auto_detect_line_endings value not parsed as bool).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78620">#78620</a> (Out of memory error).</li>
</ul></li>
<li>Exif:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78442">#78442</a> ('Illegal component' on exif_read_data since PHP7) (Kalle)</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78599">#78599</a> (env_path_info underflow in fpm_main.c can lead to RCE). (CVE-2019-11043)</li>
  <li>Fixed bug <a href="http://bugs.php.net/78413">#78413</a> (request_terminate_timeout does not take effect after fastcgi_finish_request).</li>
</ul></li>
<li>MBString:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78633">#78633</a> (Heap buffer overflow (read) in mb_eregi).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78579">#78579</a> (mb_decode_numericentity: args number inconsistency).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78609">#78609</a> (mb_check_encoding() no longer supports stringable objects).</li>
</ul></li>
<li>MySQLi:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76809">#76809</a> (SSL settings aren't respected when persistent connections are used).</li>
</ul></li>
<li>Mysqlnd:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78525">#78525</a> (Memory leak in pdo when reusing native prepared statements).</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78272">#78272</a> (calling preg_match() before pcntl_fork() will freeze child process).</li>
</ul></li>
<li>PDO_MySQL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78623">#78623</a> (Regression caused by "SP call yields additional empty result set").</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78624">#78624</a> (session_gc return value for user defined session handlers).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76342">#76342</a> (file_get_contents waits twice specified timeout).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78612">#78612</a> (strtr leaks memory when integer keys are used and the subject string shorter).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76859">#76859</a> (stream_get_line skips data if used with data-generating filter).</li>
</ul></li>
<li>Zip:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78641">#78641</a> (addGlob can modify given remove_path value).</li>
</ul></li>
</ul>
<!-- }}} --></section>



<section class="version" id="7.3.10"><!-- {{{ 7.3.10 -->
<h3>Version 7.3.10</h3>
<b><time class='releasedate' datetime='2019-09-26'>26 Sep 2019</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78220">#78220</a> (Can't access OneDrive folder).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77922">#77922</a> (Double release of doc comment on inherited shadow property).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78441">#78441</a> (Parse error due to heredoc identifier followed by digit).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77812">#77812</a> (Interactive mode does not support PHP 7.3-style heredoc).</li>
</ul></li>
<li>FastCGI:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78469">#78469</a> (FastCGI on_accept hook is not called when using named pipes on Windows).</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78334">#78334</a> (fpm log prefix message includes wrong stdout/stderr notation).</li>
</ul></li>
<li>Intl:
<ul>
  <li>Ensure IDNA2003 rules are used with idn_to_ascii() and idn_to_utf8() when requested.</li>
</ul></li>
<li>MBString:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78559">#78559</a> (Heap buffer overflow in mb_eregi).</li>
</ul></li>
<li>MySQLnd:
<ul>
  <li>Fixed connect_attr issues and added the _server_host connection attribute.</li>
</ul></li>
<li>ODBC:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78473">#78473</a> (odbc_close() closes arbitrary resources).</li>
</ul></li>
<li>PDO_MySQL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/41997">#41997</a> (SP call yields additional empty result set).</li>
</ul></li>
<li>sodium:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78510">#78510</a> (Partially uninitialized buffer returned by sodium_crypto_generichash_init()).</li>
</ul></li>
</ul>
<!-- }}} --></section>



<section class="version" id="7.3.9"><!-- {{{ 7.3.9 -->
<h3>Version 7.3.9</h3>
<b><time class='releasedate' datetime='2019-08-29'>29 Aug 2019</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78363">#78363</a> (Buffer overflow in zendparse).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78379">#78379</a> (Cast to object confuses GC, causes crash).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78412">#78412</a> (Generator incorrectly reports non-releasable $this as GC child).</li>
</ul></li>
<li>Curl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77946">#77946</a> (Bad cURL resources returned by curl_multi_info_read()).</li>
</ul></li>
<li>Exif:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78333">#78333</a> (Exif crash (bus error) due to wrong alignment and invalid cast).</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77185">#77185</a> (Use-after-free in FPM master event handling).</li>
</ul></li>
<li>Iconv:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78342">#78342</a> (Bus error in configure test for iconv //IGNORE).</li>
</ul></li>
<li>LiteSpeed:
<ul>
  <li>Updated to LiteSpeed SAPI V7.5 (Fixed clean shutdown).</li>
</ul></li>
<li>MBString:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78380">#78380</a> (Oniguruma 6.9.3 fixes CVEs). (CVE-2019-13224)</li>
</ul></li>
<li>MySQLnd:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78179">#78179</a> (MariaDB server version incorrectly detected).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78213">#78213</a> (Empty row pocket).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77191">#77191</a> (Assertion failure in dce_live_ranges() when silencing is used).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69100">#69100</a> (Bus error from stream_copy_to_stream (file -&gt; SSL stream) with invalid length).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78282">#78282</a> (atime and mtime mismatch).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78326">#78326</a> (improper memory deallocation on stream_get_contents() with fixed length buffer).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78346">#78346</a> (strip_tags no longer handling nested php tags).</li>
</ul></li>
</ul>
<!-- }}} --></section>



<section class="version" id="7.3.8"><!-- {{{ 7.3.8 -->
<h3>Version 7.3.8</h3>
<b><time class='releasedate' datetime='2019-08-01'>01 Aug 2019</time></b>
<ul><li>Core:
<ul>
  <li>Added syslog.filter=raw option.</li>
  <li>Fixed bug <a href="http://bugs.php.net/78212">#78212</a> (Segfault in built-in webserver).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69044">#69044</a> (discrepency between time and microtime).</li>
  <li>Updated timelib to 2018.02.</li>
</ul></li>
<li>EXIF:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78256">#78256</a> (heap-buffer-overflow on exif_process_user_comment). (CVE-2019-11042)</li>
  <li>Fixed bug <a href="http://bugs.php.net/78222">#78222</a> (heap-buffer-overflow on exif_scan_thumbnail). (CVE-2019-11041)</li>
</ul></li>
<li>FTP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78039">#78039</a> (FTP with SSL memory leak).</li>
</ul></li>
<li>Libxml:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78279">#78279</a> (libxml_disable_entity_loader settings is shared between requests (cgi-fcgi)).</li>
</ul></li>
<li>LiteSpeed:
<ul>
  <li>Updated to LiteSpeed SAPI V7.4.3 (increased response header count limit from 100 to 1000, added crash handler to cleanly shutdown PHP request, added CloudLinux mod_lsapi mode).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76058">#76058</a> (After "POST data can't be buffered", using php://input makes huge tmp files).</li>
</ul></li>
<li>Openssl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78231">#78231</a> (Segmentation fault upon stream_socket_accept of exported socket-to-stream).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78341">#78341</a> (Failure to detect smart branch in DFA pass).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78189">#78189</a> (file cache strips last character of uname hash).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78202">#78202</a> (Opcache stats for cache hits are capped at 32bit NUM).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78271">#78271</a> (Invalid result of if-else).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78291">#78291</a> (opcache_get_configuration doesn't list all directives).</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78338">#78338</a> (Array cross-border reading in PCRE).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78197">#78197</a> (PCRE2 version check in configure fails for "##.##-xxx" version strings).</li>
</ul></li>
<li>PDO_Sqlite:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78192">#78192</a> (SegFault when reuse statement after schema has changed).</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77919">#77919</a> (Potential UAF in Phar RSHUTDOWN).</li>
</ul></li>
<li>Phpdbg:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78297">#78297</a> (Include unexistent file memory leak).</li>
</ul></li>
<li>SQLite:
<ul>
  <li>Upgraded to SQLite 3.28.0.</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78241">#78241</a> (touch() does not handle dates after 2038 in PHP 64-bit).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78269">#78269</a> (password_hash uses weak options for argon2).</li>
</ul></li>
</ul>
<!-- }}} --></section>



<section class="version" id="7.3.7"><!-- {{{ 7.3.7 -->
<h3>Version 7.3.7</h3>
<b><time class='releasedate' datetime='2019-07-04'>04 Jul 2019</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76980">#76980</a> (Interface gets skipped if autoloader throws an exception).</li>
</ul></li>
<li>DOM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78025">#78025</a> (segfault when accessing properties of DOMDocumentType).</li>
</ul></li>
<li>MySQLi:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77956">#77956</a> (When mysqli.allow_local_infile = Off, use a meaningful error message).</li>
  <li>Fixed bug <a href="http://bugs.php.net/38546">#38546</a> (bindParam incorrect processing of bool types).</li>
</ul></li>
<li>MySQLnd:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77955">#77955</a> (Random segmentation fault in mysqlnd from php-fpm).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78015">#78015</a> (Incorrect evaluation of expressions involving partials arrays in SCCP).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78106">#78106</a> (Path resolution fails if opcache disabled during request).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78079">#78079</a> (openssl_encrypt_ccm.phpt fails with OpenSSL 1.1.1c).</li>
</ul></li>
<li>phpdbg:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78050">#78050</a> (SegFault phpdbg + opcache on include file twice).</li>
</ul></li>
<li>Sockets:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78038">#78038</a> (Socket_select fails when resource array contains references).</li>
</ul></li>
<li>Sodium:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78114">#78114</a> (segfault when calling sodium_* functions from eval).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77135">#77135</a> (Extract with EXTR_SKIP should skip $this).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77937">#77937</a> (preg_match failed).</li>
</ul></li>
<li>Zip:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76345">#76345</a> (zip.h not found).</li>
</ul></li>
</ul>
<!-- }}} --></section>


<section class="version" id="7.3.6"><!-- {{{ 7.3.6 -->
<h3>Version 7.3.6</h3>
<b><time class='releasedate' datetime='2019-05-30'>30 May 2019</time></b>
<ul><li>cURL:
<ul>
  <li>Implemented FR <a href="http://bugs.php.net/72189">#72189</a> (Add missing CURL_VERSION_* constants).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77909">#77909</a> (DatePeriod::__construct() with invalid recurrence count value).</li>
</ul></li>
<li>EXIF:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77988">#77988</a> (heap-buffer-overflow on php_jpg_get16) (CVE-2019-11040).</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77934">#77934</a> (php-fpm kill -USR2 not working).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77921">#77921</a> (static.php.net doesn't work anymore).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77943">#77943</a> (imageantialias($image, false); does not work).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77973">#77973</a> (Uninitialized read in gdImageCreateFromXbm) (CVE-2019-11038).</li>
</ul></li>
<li>Iconv:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78069">#78069</a> (Out-of-bounds read in iconv.c:_php_iconv_mime_decode() due to integer overflow) (CVE-2019-11039).</li>
</ul></li>
<li>JSON:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77843">#77843</a> (Use after free with json serializer).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed possible crashes, because of inconsistent PCRE cache and opcache SHM reset.</li>
</ul></li>
<li>PDO_MySQL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77944">#77944</a> (Wrong meta pdo_type for bigint on LLP64).</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75186">#75186</a> (Inconsistent reflection of Closure:::__invoke()).</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77911">#77911</a> (Wrong warning for session.sid_bits_per_character).</li>
</ul></li>
<li>SOAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77945">#77945</a> (Segmentation fault when constructing SoapClient with WSDL_CACHE_BOTH).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77024">#77024</a> (SplFileObject::__toString() may return array).</li>
</ul></li>
<li>SQLite:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77967">#77967</a> (Bypassing open_basedir restrictions via file uris).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77931">#77931</a> (Warning for array_map mentions wrong type).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78003">#78003</a> (strip_tags output change since PHP 7.3).</li>
</ul></li>
</ul>
<!-- }}} --></section>
<section class="version" id="7.3.5"><!-- {{{ 7.3.5 -->
<h3>Version 7.3.5</h3>
<b><time class='releasedate' datetime='2019-05-02'>02 May 2019</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77903">#77903</a> (ArrayIterator stops iterating after offsetSet call).</li>
</ul></li>
<li>CLI:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77794">#77794</a> (Incorrect Date header format in built-in server).</li>
</ul></li>
<li>EXIF:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77950">#77950</a> (Heap-buffer-overflow in _estrndup via exif_process_IFD_TAG) (CVE-2019-11036).</li>
</ul></li>
<li>Interbase:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72175">#72175</a> (Impossibility of creating multiple connections to Interbase with php 7.x).</li>
</ul></li>
<li>Intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77895">#77895</a> (IntlDateFormatter::create fails in strict mode if $locale = null).</li>
</ul></li>
<li>litespeed:
<ul>
  <li>LiteSpeed SAPI 7.3.1, better process management, new API function litespeed_finish_request().</li>
</ul></li>
<li>LDAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77869">#77869</a> (Core dump when using server controls) (mcmic)</li>
</ul></li>
<li>Mail:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77821">#77821</a> (Potential heap corruption in TSendMail()).</li>
</ul></li>
<li>mbstring:
<ul>
  <li>Implemented FR <a href="http://bugs.php.net/72777">#72777</a> (Implement regex stack limits for mbregex functions).</li>
</ul></li>
<li>MySQLi:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77773">#77773</a> (Unbuffered queries leak memory - MySQLi / mysqlnd).</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77827">#77827</a> (preg_match does not ignore \r in regex flags).</li>
</ul></li>
<li>PDO:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77849">#77849</a> (Disable cloning of PDO handle/connection objects).</li>
</ul></li>
<li>phpdbg:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76801">#76801</a> (too many open files).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77800">#77800</a> (phpdbg segfaults on listing some conditional breakpoints).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77805">#77805</a> (phpdbg build fails when readline is shared).</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77772">#77772</a> (ReflectionClass::getMethods(null) doesn't work).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77882">#77882</a> (Different behavior: always calls destructor).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77793">#77793</a> (Segmentation fault in extract() when overwriting reference with itself).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77844">#77844</a> (Crash due to null pointer in parse_ini_string with INI_SCANNER_TYPED).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77853">#77853</a> (Inconsistent substr_compare behaviour with empty haystack).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.3.4"><!-- {{{ 7.3.4 -->
<h3>Version 7.3.4</h3>
<b><time class='releasedate' datetime='2019-04-04'>04 Apr 2019</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77738">#77738</a> (Nullptr deref in zend_compile_expr).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77660">#77660</a> (Segmentation fault on break 2147483648).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77652">#77652</a> (Anonymous classes can lose their interface information).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77345">#77345</a> (Stack Overflow caused by circular reference in garbage collection).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76956">#76956</a> (Wrong value for 'syslog.filter' documented in php.ini).</li>
</ul></li>
<li>Apache2Handler:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77648">#77648</a> (BOM in sapi/apache2handler/php_functions.c).</li>
</ul></li>
<li>Bcmath:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77742">#77742</a> (bcpow() implementation related to gcc compiler optimization).</li>
</ul></li>
<li>CLI Server:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77722">#77722</a> (Incorrect IP set to $_SERVER['REMOTE_ADDR'] on the localhost).</li>
</ul></li>
<li>COM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77578">#77578</a> (Crash when php unload).</li>
</ul></li>
<li>EXIF:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77753">#77753</a> (Heap-buffer-overflow in php_ifd_get32s). (CVE-2019-11034)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77831">#77831</a> (Heap-buffer-overflow in exif_iif_add_value). (CVE-2019-11035)</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77677">#77677</a> (FPM fails to build on AIX due to missing WCOREDUMP).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77700">#77700</a> (Writing truecolor images as GIF ignores interlace flag).</li>
</ul></li>
<li>MySQLi:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77597">#77597</a> (mysqli_fetch_field hangs scripts).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77743">#77743</a> (Incorrect pi node insertion for jmpznz with identical successors).</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76127">#76127</a> (preg_split does not raise an error on invalid UTF-8).</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77697">#77697</a> (Crash on Big_Endian platform).</li>
</ul></li>
<li>phpdbg:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77767">#77767</a> (phpdbg break cmd aliases listed in help do not match actual aliases).</li>
</ul></li>
<li>sodium:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77646">#77646</a> (sign_detached() strings not terminated).</li>
</ul></li>
<li>SQLite3:
<ul>
  <li>Added sqlite3.defensive INI directive.</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77664">#77664</a> (Segmentation fault when using undefined constant in custom wrapper).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77669">#77669</a> (Crash in extract() when overwriting extracted array).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76717">#76717</a> (var_export() does not create a parsable value for PHP_INT_MIN).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77765">#77765</a> (FTP stream wrapper should set the directory as executable).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.3.3"><!-- {{{ 7.3.3 -->
<h3>Version 7.3.3</h3>
<b><time class='releasedate' datetime='2019-03-07'>07 Mar 2019</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77589">#77589</a> (Core dump using parse_ini_string with numeric sections).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77329">#77329</a> (Buffer Overflow via overly long Error Messages).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77494">#77494</a> (Disabling class causes segfault on member access).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77498">#77498</a> (Custom extension Segmentation fault when declare static property).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77530">#77530</a> (PHP crashes when parsing `(2)::class`).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77546">#77546</a> (iptcembed broken function).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77630">#77630</a> (rename() across the device may allow unwanted access during processing). (CVE-2019-9637)</li>
</ul></li>
<li>COM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77621">#77621</a> (Already defined constants are not properly reported).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77626">#77626</a> (Persistence confusion in php_com_import_typelib()).</li>
</ul></li>
<li>EXIF:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77509">#77509</a> (Uninitialized read in exif_process_IFD_in_TIFF). (CVE-2019-9641)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77540">#77540</a> (Invalid Read on exif_process_SOFn). (CVE-2019-9640)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77563">#77563</a> (Uninitialized read in exif_process_IFD_in_MAKERNOTE). (CVE-2019-9638)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77659">#77659</a> (Uninitialized read in exif_process_IFD_in_MAKERNOTE). (CVE-2019-9639)</li>
</ul></li>
<li>Mbstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77514">#77514</a> (mb_ereg_replace() with trailing backslash adds null byte).</li>
</ul></li>
<li>MySQL:
<ul>
  <li>Disabled LOCAL INFILE by default, can be enabled using php.ini directive mysqli.allow_local_infile for mysqli, or PDO::MYSQL_ATTR_LOCAL_INFILE attribute for pdo_mysql.</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77390">#77390</a> (feof might hang on TLS streams in case of fragmented TLS records).</li>
</ul></li>
<li>PDO_OCI:
<ul>
  <li>Support Oracle Database tracing attributes ACTION, MODULE, CLIENT_INFO, and CLIENT_IDENTIFIER.</li>
</ul></li>
<li>PHAR:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77396">#77396</a> (Null Pointer Dereference in phar_create_or_parse_filename).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77586">#77586</a> (phar_tar_writeheaders_int() buffer overflow).</li>
</ul></li>
<li>phpdbg:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76596">#76596</a> (phpdbg support for display_errors=stderr).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/51068">#51068</a> (DirectoryIterator glob:// don't support current path relative queries).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77431">#77431</a> (openFile() silently truncates after a null byte).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77552">#77552</a> (Unintialized php_stream_statbuf in stat functions).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77612">#77612</a> (setcookie() sets incorrect SameSite header if all of its options filled).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.3.2"><!-- {{{ 7.3.2 -->
<h3>Version 7.3.2</h3>
<b><time class='releasedate' datetime='2019-02-07'>07 Feb 2019</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77369">#77369</a> (memcpy with negative length via crafted DNS response). (CVE-2019-9022)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77387">#77387</a> (Recursion detection broken when printing GLOBALS).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77376">#77376</a> ("undefined function" message no longer includes namespace).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77357">#77357</a> (base64_encode / base64_decode doest not work on nested VM).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77339">#77339</a> (__callStatic may get incorrect arguments).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77317">#77317</a> (__DIR__, __FILE__, realpath() reveal physical path for subst virtual drive).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77263">#77263</a> (Segfault when using 2 RecursiveFilterIterator).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77447">#77447</a> (PHP 7.3 built with ASAN crashes in zend_cpu_supports_avx2).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77484">#77484</a> (Zend engine crashes when calling realpath in invalid working dir).</li>
</ul></li>
<li>Curl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76675">#76675</a> (Segfault with H2 server push).</li>
</ul></li>
<li>Fileinfo:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77346">#77346</a> (webm files incorrectly detected as application/octet-stream).</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77430">#77430</a> (php-fpm crashes with Main process exited, code=dumped, status=11/SEGV).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73281">#73281</a> (imagescale(…, IMG_BILINEAR_FIXED) can cause black border).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73614">#73614</a> (gdImageFilledArc() doesn't properly draw pies).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77272">#77272</a> (imagescale() may return image resource on failure).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77391">#77391</a> (1bpp BMPs may fail to be loaded).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77479">#77479</a> (imagewbmp() segfaults with very large images).</li>
</ul></li>
<li>ldap:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77440">#77440</a> (ldap_bind using ldaps or ldap_start_tls()=exception in libcrypto-1_1-x64.dll).</li>
</ul></li>
<li>Mbstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77428">#77428</a> (mb_ereg_replace() doesn't replace a substitution variable).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77454">#77454</a> (mb_scrub() silently truncates after a null byte).</li>
</ul></li>
<li>MySQLnd:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77308">#77308</a> (Unbuffered queries memory leak).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75684">#75684</a> (In mysqlnd_ext_plugin.h the plugin methods family has no external visibility).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77266">#77266</a> (Assertion failed in dce_live_ranges).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77257">#77257</a> (value of variable assigned in a switch() construct gets lost).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77434">#77434</a> (php-fpm workers are segfaulting in zend_gc_addre).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77361">#77361</a> (configure fails on 64-bit AIX when opcache enabled).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77287">#77287</a> (Opcache literal compaction is incompatible with EXT opcodes).</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77338">#77338</a> (get_browser with empty string).</li>
</ul></li>
<li>PDO:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77273">#77273</a> (array_walk_recursive corrupts value types leading to PDO failure).</li>
</ul></li>
<li>PDO MySQL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77289">#77289</a> (PDO MySQL segfaults with persistent connection).</li>
</ul></li>
<li>SOAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77410">#77410</a> (Segmentation Fault when executing method with an empty parameter).</li>
</ul></li>
<li>Sockets:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76839">#76839</a> (socket_recvfrom may return an invalid 'from' address on MacOS).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77298">#77298</a> (segfault occurs when add property to unserialized empty ArrayObject).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77395">#77395</a> (segfault about array_multisort).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77439">#77439</a> (parse_str segfaults when inserting item into existing array).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.3.1"><!-- {{{ 7.3.1 -->
<h3>Version 7.3.1</h3>
<b><time class='releasedate' datetime='2019-01-10'>10 Jan 2019</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76654">#76654</a> (Build failure on Mac OS X on 32-bit Intel).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71041">#71041</a> (zend_signal_startup() needs ZEND_API).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76046">#76046</a> (PHP generates "FE_FREE" opcode on the wrong line).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77291">#77291</a> (magic methods inherited from a trait may be ignored).</li>
</ul></li>
<li>CURL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77264">#77264</a> (curl_getinfo returning microseconds, not seconds).</li>
</ul></li>
<li>COM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77177">#77177</a> (Serializing or unserializing COM objects crashes).</li>
</ul></li>
<li>Exif:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77184">#77184</a> (Unsigned rational numbers are written out as signed rationals).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77195">#77195</a> (Incorrect error handling of imagecreatefromjpeg()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77198">#77198</a> (auto cropping has insufficient precision).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77200">#77200</a> (imagecropauto(…, GD_CROP_SIDES) crops left but not right).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77269">#77269</a> (efree() on uninitialized Heap data in imagescale leads to use-after-free). (CVE-2016-10166)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77270">#77270</a> (imagecolormatch Out Of Bounds Write on Heap). (CVE-2019-6977)</li>
</ul></li>
<li>MBString:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77367">#77367</a> (Negative size parameter in mb_split). (CVE-2019-9025)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77370">#77370</a> (Buffer overflow on mb regex functions - fetch_token). (CVE-2019-9023)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77371">#77371</a> (heap buffer overflow in mb regex functions - compile_string_node). (CVE-2019-9023)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77381">#77381</a> (heap buffer overflow in multibyte match_at). (CVE-2019-9023)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77382">#77382</a> (heap buffer overflow due to incorrect length in expand_case_fold_string). (CVE-2019-9023)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77385">#77385</a> (buffer overflow in fetch_token). (CVE-2019-9023)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77394">#77394</a> (Buffer overflow in multibyte case folding - unicode). (CVE-2019-9023)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77418">#77418</a> (Heap overflow in utf32be_mbc_to_code). (CVE-2019-9023)</li>
</ul></li>
<li>OCI8:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76804">#76804</a> (oci_pconnect with OCI_CRED_EXT not working).</li>
  <li>Added oci_set_call_timeout() for call timeouts.</li>
  <li>Added oci_set_db_operation() for the DBOP end-to-end-tracing attribute.</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77215">#77215</a> (CFG assertion failure on multiple finalizing switch frees in one block).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77275">#77275</a> (OPcache optimization problem for ArrayAccess-&gt;offsetGet).</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77193">#77193</a> (Infinite loop in preg_replace_callback).</li>
</ul></li>
<li>PDO:
<ul>
  <li>Handle invalid index passed to PDOStatement::fetchColumn() as error.</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77247">#77247</a> (heap buffer overflow in phar_detect_phar_fname_ext). (CVE-2019-9021)</li>
</ul></li>
<li>Soap:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77088">#77088</a> (Segfault when using SoapClient with null options).</li>
</ul></li>
<li>Sockets:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77136">#77136</a> (Unsupported IPV6_RECVPKTINFO constants on macOS).</li>
</ul></li>
<li>Sodium:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77297">#77297</a> (SodiumException segfaults on PHP 7.3).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77359">#77359</a> (spl_autoload causes segfault).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77360">#77360</a> (class_uses causes segfault).</li>
</ul></li>
<li>SQLite3:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77051">#77051</a> (Issue with re-binding on SQLite3).</li>
</ul></li>
<li>Xmlrpc:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77242">#77242</a> (heap out of bounds read in xmlrpc_decode()). (CVE-2019-9020)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77380">#77380</a> (Global out of bounds read in xmlrpc base64 code). (CVE-2019-9024)</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.3.0"><!-- {{{ 7.3.0 -->
<h3>Version 7.3.0</h3>
<b><time class='releasedate' datetime='2018-12-06'>06 Dec 2018</time></b>
<ul><li>Core:
<ul>
  <li>Improved PHP GC.</li>
  <li>Redesigned the old ext_skel program written in PHP, run: 'php ext_skel.php' for all options. This means there are no dependencies, thus making it work on Windows out of the box.</li>
  <li>Removed support for BeOS.</li>
  <li>Add PHP_VERSION to phpinfo() &lt;title/&gt;.</li>
  <li>Add net_get_interfaces().</li>
  <li>Implemented flexible heredoc and nowdoc syntax, per RFC https://wiki.php.net/rfc/flexible_heredoc_nowdoc_syntaxes.</li>
  <li>Added support for references in list() and array destructuring, per RFC https://wiki.php.net/rfc/list_reference_assignment.</li>
  <li>Improved effectiveness of ZEND_SECURE_ZERO for NetBSD and systems without native similar feature.</li>
  <li>Added syslog.facility and syslog.ident INI entries for customizing syslog logging.</li>
  <li>Fixed bug <a href="http://bugs.php.net/75683">#75683</a> (Memory leak in zend_register_functions() in ZTS mode).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75031">#75031</a> (support append mode in temp/memory streams).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74860">#74860</a> (Uncaught exceptions not being formatted properly when error_log set to "syslog").</li>
  <li>Fixed bug <a href="http://bugs.php.net/75220">#75220</a> (Segfault when calling is_callable on parent).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69954">#69954</a> (broken links and unused config items in distributed ini files).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74922">#74922</a> (Composed class has fatal error with duplicate, equal const properties).</li>
  <li>Fixed bug <a href="http://bugs.php.net/63911">#63911</a> (identical trait methods raise errors during composition).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75677">#75677</a> (Clang ignores fastcall calling convention on variadic function).</li>
  <li>Fixed bug <a href="http://bugs.php.net/54043">#54043</a> (Remove inconsitency of internal exceptions and user defined exceptions).</li>
  <li>Fixed bug <a href="http://bugs.php.net/53033">#53033</a> (Mathematical operations convert objects to integers).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73108">#73108</a> (Internal class cast handler uses integer instead of float).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75765">#75765</a> (Fatal error instead of Error exception when base class is not found).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76198">#76198</a> (Wording: "iterable" is not a scalar type).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76137">#76137</a> (config.guess/config.sub do not recognize RISC-V).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76427">#76427</a> (Segfault in zend_objects_store_put).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76422">#76422</a> (ftruncate fails on files &gt; 2GB).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76509">#76509</a> (Inherited static properties can be desynchronized from their parent by ref).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76439">#76439</a> (Changed behaviour in unclosed HereDoc).</li>
  <li>Fixed bug <a href="http://bugs.php.net/63217">#63217</a> (Constant numeric strings become integers when used as ArrayAccess offset).</li>
  <li>Fixed bug <a href="http://bugs.php.net/33502">#33502</a> (Some nullary functions don't check the number of arguments).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76392">#76392</a> (Error relocating sapi/cli/php: unsupported relocation type 37).</li>
  <li>The declaration and use of case-insensitive constants has been deprecated.</li>
  <li>Added syslog.filter INI entry for syslog filtering.</li>
  <li>Fixed bug <a href="http://bugs.php.net/76667">#76667</a> (Segfault with divide-assign op and __get + __set).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76030">#76030</a> (RE2C_FLAGS rarely honoured) (Cristian Rodríguez)</li>
  <li>Fixed broken zend_read_static_property (Laruence)</li>
  <li>Fixed bug <a href="http://bugs.php.net/76773">#76773</a> (Traits used on the parent are ignored for child classes).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76767">#76767</a> (‘asm’ operand has impossible constraints in zend_operators.h).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76752">#76752</a> (Crash in ZEND_COALESCE_SPEC_TMP_HANDLER - assertion in _get_zval_ptr_tmp failed).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76820">#76820</a> (Z_COPYABLE invalid definition).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76510">#76510</a> (file_exists() stopped working for phar://).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76869">#76869</a> (Incorrect bypassing protected method accessibilty check).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72635">#72635</a> (Undefined class used by class constant in constexpr generates fatal error).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76947">#76947</a> (file_put_contents() blocks the directory of the file (__DIR__)).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76979">#76979</a> (define() error message does not mention resources as valid values).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76825">#76825</a> (Undefined symbols ___cpuid_count).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77110">#77110</a> (undefined symbol zend_string_equal_val in C++ build).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77231">#77231</a> (Segfault when using convert.quoted-printable-encode filter).</li>
</ul></li>
<li>BCMath:
<ul>
  <li>Implemented FR <a href="http://bugs.php.net/67855">#67855</a> (No way to get current scale in use).</li>
  <li>Fixed bug <a href="http://bugs.php.net/66364">#66364</a> (BCMath bcmul ignores scale parameter).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75164">#75164</a> (split_bc_num() is pointless).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75169">#75169</a> (BCMath errors/warnings bypass PHP's error handling).</li>
</ul></li>
<li>CLI:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/44217">#44217</a> (Output after stdout/stderr closed cause immediate exit with status 0).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77111">#77111</a> (php-win.exe corrupts unicode symbols from cli parameters).</li>
</ul></li>
<li>cURL:
<ul>
  <li>Expose curl constants from curl 7.50 to 7.61.</li>
  <li>Fixed bug <a href="http://bugs.php.net/74125">#74125</a> (Fixed finding CURL on systems with multiarch support).</li>
</ul></li>
<li>Date:
<ul>
  <li>Implemented FR <a href="http://bugs.php.net/74668">#74668</a>: Add DateTime::createFromImmutable() method.</li>
  <li>Fixed bug <a href="http://bugs.php.net/75222">#75222</a> (DateInterval microseconds property always 0).</li>
  <li>Fixed bug <a href="http://bugs.php.net/68406">#68406</a> (calling var_dump on a DateTimeZone object modifies it).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76131">#76131</a> (mismatch arginfo for date_create).</li>
  <li>Updated timelib to 2018.01RC1 to address several bugs:</li>
  <li>Fixed bug <a href="http://bugs.php.net/75577">#75577</a> (DateTime::createFromFormat does not accept 'v' format specifier).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75642">#75642</a> (Wrap around behaviour for microseconds is not working).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77097">#77097</a> (DateTime::diff gives wrong diff when the actual diff is less than 1 second).</li>
</ul></li>
<li>DBA:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75264">#75264</a> (compiler warnings emitted).</li>
</ul></li>
<li>DOM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76285">#76285</a> (DOMDocument::formatOutput attribute sometimes ignored).</li>
</ul></li>
<li>Fileinfo:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77095">#77095</a> (slowness regression in 7.2/7.3 (compared to 7.1)).</li>
</ul></li>
<li>Filter:
<ul>
  <li>Added the 'add_slashes' sanitization mode (FILTER_SANITIZE_ADD_SLASHES).</li>
</ul></li>
<li>FPM:
<ul>
  <li>Added fpm_get_status function.</li>
  <li>Fixed bug <a href="http://bugs.php.net/62596">#62596</a> (getallheaders() missing with PHP-FPM).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69031">#69031</a> (Long messages into stdout/stderr are truncated incorrectly) - added new log related FPM configuration options: log_limit, log_buffering and decorate_workers_output.</li>
</ul></li>
<li>ftp:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77151">#77151</a> (ftp_close(): SSL_read on shutdown).</li>
</ul></li>
<li>GD:
<ul>
  <li>Added support for WebP in imagecreatefromstring().</li>
</ul></li>
<li>GMP:
<ul>
  <li>Export internal structures and accessor helpers for GMP object.</li>
  <li>Added gmp_binomial(n, k).</li>
  <li>Added gmp_lcm(a, b).</li>
  <li>Added gmp_perfect_power(a).</li>
  <li>Added gmp_kronecker(a, b).</li>
</ul></li>
<li>iconv:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/53891">#53891</a> (iconv_mime_encode() fails to Q-encode UTF-8 string).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77147">#77147</a> (Fixing 60494 ignored ICONV_MIME_DECODE_CONTINUE_ON_ERROR).</li>
</ul></li>
<li>IMAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77153">#77153</a> (imap_open allows to run arbitrary shell commands via mailbox parameter). (CVE-2018-19518)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77020">#77020</a> (null pointer dereference in imap_mail).</li>
</ul></li>
<li>Interbase:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75453">#75453</a> (Incorrect reflection for ibase_[p]connect).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76443">#76443</a> (php+php_interbase.dll crash on module_shutdown).</li>
</ul></li>
<li>intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75317">#75317</a> (UConverter::setDestinationEncoding changes source instead of destination).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76829">#76829</a> (Incorrect validation of domain on idn_to_utf8() function).</li>
</ul></li>
<li>JSON:
<ul>
  <li>Added JSON_THROW_ON_ERROR flag.</li>
</ul></li>
<li>LDAP:
<ul>
  <li>Added ldap_exop_refresh helper for EXOP REFRESH operation with dds overlay.</li>
  <li>Added full support for sending and parsing ldap controls.</li>
  <li>Fixed bug <a href="http://bugs.php.net/49876">#49876</a> (Fix LDAP path lookup on 64-bit distros).</li>
</ul></li>
<li>libxml2:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75871">#75871</a> (use pkg-config where available).</li>
</ul></li>
<li>litespeed:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75248">#75248</a> (Binary directory doesn't get created when building only litespeed SAPI).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75251">#75251</a> (Missing program prefix and suffix).</li>
</ul></li>
<li>MBstring:
<ul>
  <li>Updated to Oniguruma 6.9.0.</li>
  <li>Fixed bug <a href="http://bugs.php.net/65544">#65544</a> (mb title case conversion-first word in quotation isn't capitalized).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71298">#71298</a> (MB_CASE_TITLE misbehaves with curled apostrophe/quote).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73528">#73528</a> (Crash in zif_mb_send_mail).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74929">#74929</a> (mbstring functions version 7.1.1 are slow compared to 5.3 on Windows).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76319">#76319</a> (mb_strtolower with invalid UTF-8 causes segmentation fault).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76574">#76574</a> (use of undeclared identifiers INT_MAX and LONG_MAX).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76594">#76594</a> (Bus Error due to unaligned access in zend_ini.c OnUpdateLong).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76706">#76706</a> (mbstring.http_output_conv_mimetypes is ignored).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76958">#76958</a> (Broken UTF7-IMAP conversion).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77025">#77025</a> (mb_strpos throws Unknown encoding or conversion error).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77165">#77165</a> (mb_check_encoding crashes when argument given an empty array).</li>
</ul></li>
<li>Mysqlnd:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76386">#76386</a> (Prepared Statement formatter truncates fractional seconds from date/time column).</li>
</ul></li>
<li>ODBC:
<ul>
  <li>Removed support for ODBCRouter.</li>
  <li>Removed support for Birdstep.</li>
  <li>Fixed bug <a href="http://bugs.php.net/77079">#77079</a> (odbc_fetch_object has incorrect type signature).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76466">#76466</a> (Loop variable confusion).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76463">#76463</a> (var has array key type but not value type).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76446">#76446</a> (zend_variables.c:73: zend_string_destroy: Assertion `!(zval_gc_flags((str)-&gt;gc)).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76711">#76711</a> (OPcache enabled triggers false-positive "Illegal string offset").</li>
  <li>Fixed bug <a href="http://bugs.php.net/77058">#77058</a> (Type inference in opcache causes side effects).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77092">#77092</a> (array_diff_key() - segmentation fault).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Added openssl_pkey_derive function.</li>
  <li>Add min_proto_version and max_proto_version ssl stream options as well as related constants for possible TLS protocol values.</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Implemented https://wiki.php.net/rfc/pcre2-migration.</li>
  <li>Upgrade PCRE2 to 10.32.</li>
  <li>Fixed bug <a href="http://bugs.php.net/75355">#75355</a> (preg_quote() does not quote # control character).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76512">#76512</a> (\w no longer includes unicode characters).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76514">#76514</a> (Regression in preg_match makes it fail with PREG_JIT_STACKLIMIT_ERROR).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76909">#76909</a> (preg_match difference between 7.3 and &lt; 7.3).</li>
</ul></li>
<li>PDO_DBlib:
<ul>
  <li>Implemented FR <a href="http://bugs.php.net/69592">#69592</a> (allow 0-column rowsets to be skipped automatically).</li>
  <li>Expose TDS version as \PDO::DBLIB_ATTR_TDS_VERSION attribute on \PDO instance.</li>
  <li>Treat DATETIME2 columns like DATETIME.</li>
  <li>Fixed bug <a href="http://bugs.php.net/74243">#74243</a> (allow locales.conf to drive datetime format).</li>
</ul></li>
<li>PDO_Firebird:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74462">#74462</a> (PDO_Firebird returns only NULLs for results with boolean for FIREBIRD &gt;= 3.0).</li>
</ul></li>
<li>PDO_OCI:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74631">#74631</a> (PDO_PCO with PHP-FPM: OCI environment initialized before PHP-FPM sets it up).</li>
</ul></li>
<li>PDO SQLite:
<ul>
  <li>Add support for additional open flags</li>
</ul></li>
<li>pgsql:
<ul>
  <li>Added new error constants for pg_result_error(): PGSQL_DIAG_SCHEMA_NAME, PGSQL_DIAG_TABLE_NAME, PGSQL_DIAG_COLUMN_NAME, PGSQL_DIAG_DATATYPE_NAME, PGSQL_DIAG_CONSTRAINT_NAME and PGSQL_DIAG_SEVERITY_NONLOCALIZED.</li>
  <li>Fixed bug <a href="http://bugs.php.net/77047">#77047</a> (pg_convert has a broken regex for the 'TIME WITHOUT TIMEZONE' data type).</li>
</ul></li>
<li>phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74991">#74991</a> (include_path has a 4096 char limit in some cases).</li>
  <li>Fixed bug <a href="http://bugs.php.net/65414">#65414</a> (deal with leading slash when adding files correctly).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77022">#77022</a> (PharData always creates new files with mode 0666).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77143">#77143</a> (Heap Buffer Overflow (READ: 4) in phar_parse_pharfile). (CVE-2018-20783)</li>
</ul></li>
<li>readline:
<ul>
  <li>Added completion_append_character and completion_suppress_append options to readline_info() if linked against libreadline.</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74941">#74941</a> (session fails to start after having headers sent).</li>
</ul></li>
<li>SimpleXML:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/54973">#54973</a> (SimpleXML casts integers wrong).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76712">#76712</a> (Assignment of empty string creates extraneous text node).</li>
</ul></li>
<li>Sockets:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/67619">#67619</a> (Validate length on socket_write).</li>
</ul></li>
<li>SOAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75464">#75464</a> (Wrong reflection on SoapClient::__setSoapHeaders).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70469">#70469</a> (SoapClient generates E_ERROR even if exceptions=1 is used).</li>
  <li>Fixed bug <a href="http://bugs.php.net/50675">#50675</a> (SoapClient can't handle object references correctly).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76348">#76348</a> (WSDL_CACHE_MEMORY causes Segmentation fault).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77141">#77141</a> (Signedness issue in SOAP when precision=-1).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74977">#74977</a> (Appending AppendIterator leads to segfault).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75173">#75173</a> (incorrect behavior of AppendIterator::append in foreach loop).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74372">#74372</a> (autoloading file with syntax error uses next autoloader, may hide parse error).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75878">#75878</a> (RecursiveTreeIterator::setPostfix has wrong signature).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74519">#74519</a> (strange behavior of AppendIterator).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76131">#76131</a> (mismatch arginfo for splarray constructor).</li>
</ul></li>
<li>SQLite3:
<ul>
  <li>Updated bundled libsqlite to 3.24.0.</li>
</ul></li>
<li>Standard:
<ul>
  <li>Added is_countable() function.</li>
  <li>Added support for the SameSite cookie directive, including an alternative signature for setcookie(), setrawcookie() and session_set_cookie_params().</li>
  <li>Remove superfluous warnings from inet_ntop()/inet_pton().</li>
  <li>Fixed bug <a href="http://bugs.php.net/75916">#75916</a> (DNS_CAA record results contain garbage).</li>
  <li>Fixed unserialize(), to disable creation of unsupported data structures through manually crafted strings.</li>
  <li>Fixed bug <a href="http://bugs.php.net/75409">#75409</a> (accept EFAULT in addition to ENOSYS as indicator that getrandom() is missing).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74719">#74719</a> (fopen() should accept NULL as context).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69948">#69948</a> (path/domain are not sanitized in setcookie).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75996">#75996</a> (incorrect url in header for mt_rand).</li>
  <li>Added hrtime() function, to get high resolution time.</li>
  <li>Fixed bug <a href="http://bugs.php.net/48016">#48016</a> (stdClass::__setState is not defined although var_export() uses it).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76136">#76136</a> (stream_socket_get_name should enclose IPv6 in brackets).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76688">#76688</a> (Disallow excessive parameters after options array).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76713">#76713</a> (Segmentation fault caused by property corruption).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76755">#76755</a> (setcookie does not accept "double" type for expire time).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76674">#76674</a> (improve array_* failure messages exposing what was passed instead of an array).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76803">#76803</a> (ftruncate changes file pointer).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76818">#76818</a> (Memory corruption and segfault).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77081">#77081</a> (ftruncate() changes seek pointer in c mode).</li>
</ul></li>
<li>Testing:
<ul>
  <li>Implemented FR <a href="http://bugs.php.net/62055">#62055</a> (Make run-tests.php support --CGI-- sections).</li>
</ul></li>
<li>Tidy:
<ul>
  <li>Support using tidyp instead of tidy.</li>
  <li>Fixed bug <a href="http://bugs.php.net/74707">#74707</a> (Tidy has incorrect ReflectionFunction param counts for functions taking tidy).</li>
  <li>Fixed arginfo for tidy::__construct().</li>
</ul></li>
<li>Tokenizer:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76437">#76437</a> (token_get_all with TOKEN_PARSE flag fails to recognise close tag).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75218">#75218</a> (Change remaining uncatchable fatal errors for parsing into ParseError).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76538">#76538</a> (token_get_all with TOKEN_PARSE flag fails to recognise close tag with newline).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76991">#76991</a> (Incorrect tokenization of multiple invalid flexible heredoc strings).</li>
</ul></li>
<li>XML:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71592">#71592</a> (External entity processing never fails).</li>
</ul></li>
<li>Zlib:
<ul>
  <li>Added zlib/level context option for compress.zlib wrapper.</li>
</ul></li>
</ul>
<!-- }}} --></section>

<a name="PHP_7_2"></a>

<section class="version" id="7.2.30"><!-- {{{ 7.2.30 -->
<h3>Version 7.2.30</h3>
<b><time class='releasedate' datetime='2020-04-16'>16 Apr 2020</time></b>
<ul><li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79468">#79468</a> (SIGSEGV when closing stream handle with a stream filter appended).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79330">#79330</a> (shell_exec() silently truncates after a null byte).</li>
  <li>Fixed bug <a href="http://bugs.php.net/79465">#79465</a> (OOB Read in urldecode()).</li>
</ul></li>
</ul>
<!-- }}} --></section>



<section class="version" id="7.2.29"><!-- {{{ 7.2.29 -->
<h3>Version 7.2.29</h3>
<b><time class='releasedate' datetime='2020-03-19'>19 Mar 2020</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79329">#79329</a> (get_headers() silently truncates after a null byte) (CVE-2020-7066) (cmb)</li>
</ul></li>
<li>EXIF:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79282">#79282</a> (Use-of-uninitialized-value in exif) (CVE-2020-7064) (Nikita)</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.2.28"><!-- {{{ 7.2.28 -->
<h3>Version 7.2.28</h3>
<b><time class='releasedate' datetime='2020-02-20'>20 Feb 2020</time></b>
<ul><li>DOM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77569">#77569</a>: (Write Access Violation in DomImplementation).</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79082">#79082</a> (Files added to tar with Phar::buildFromIterator have all-access permissions). (CVE-2020-7063)</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79221">#79221</a> (Null Pointer Dereference in PHP Session Upload Progress). (CVE-2020-7062)</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.2.27"><!-- {{{ 7.2.27 -->
<h3>Version 7.2.27</h3>
<b><time class='releasedate' datetime='2020-01-23'>23 Jan 2020</time></b>
<ul><li>Mbstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79037">#79037</a> (global buffer-overflow in `mbfl_filt_conv_big5_wchar`). (CVE-2020-7060)</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79091">#79091</a> (heap use-after-free in session_create_id()).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/79099">#79099</a> (OOB read in php_strip_tags_ex). (CVE-2020-7059)</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.2.26"><!-- {{{ 7.2.26 -->
<h3>Version 7.2.26</h3>
<b><time class='releasedate' datetime='2019-12-18'>18 Dec 2019</time></b>
<ul><li>Bcmath:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78878">#78878</a> (Buffer underflow in bc_shift_addsub). (CVE-2019-11046)</li>
</ul></li>
<li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78862">#78862</a> (link() silently truncates after a null byte on Windows). (CVE-2019-11044)</li>
  <li>Fixed bug <a href="http://bugs.php.net/78863">#78863</a> (DirectoryIterator class silently truncates after a null byte). (CVE-2019-11045)</li>
</ul></li>
<li>EXIF:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78793">#78793</a> (Use-after-free in exif parsing under memory sanitizer). (CVE-2019-11050)</li>
  <li>Fixed bug <a href="http://bugs.php.net/78910">#78910</a> (Heap-buffer-overflow READ in exif). (CVE-2019-11047)</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78849">#78849</a> (GD build broken with -D SIGNED_COMPARE_SLOW).</li>
</ul></li>
<li>Intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78804">#78804</a> (Segmentation fault in Locale::filterMatches).</li>
</ul></li>
<li>OPcache:
<ul>
  <li>Fixed $x = (bool)$x; with opcache (should emit undeclared variable notice).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78759">#78759</a> (array_search in $GLOBALS).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78833">#78833</a> (Integer overflow in pack causes out-of-bound access).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78814">#78814</a> (strip_tags allows / in tag name =&gt; whitelist bypass).</li>
</ul></li>
</ul>
<!-- }}} --></section>


<section class="version" id="7.2.25"><!-- {{{ 7.2.25 -->
<h3>Version 7.2.25</h3>
<b><time class='releasedate' datetime='2019-11-21'>21 Nov 2019</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78656">#78656</a> (Parse errors classified as highest log-level).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78752">#78752</a> (Segfault if GC triggered while generator stack frame is being destroyed).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78689">#78689</a> (Closure::fromCallable() doesn't handle [Closure, '__invoke']).</li>
</ul></li>
<li>COM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78694">#78694</a> (Appending to a variant array causes segfault).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70153">#70153</a> (\DateInterval incorrectly unserialized).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78751">#78751</a> (Serialising DatePeriod converts DateTimeImmutable).</li>
</ul></li>
<li>Iconv:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78642">#78642</a> (Wrong libiconv version displayed). (gedas at martynas, cmb).</li>
</ul></li>
<li>OpCache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78654">#78654</a> (Incorrectly computed opcache checksum on files with non-ascii characters).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78747">#78747</a> (OpCache corrupts custom extension result).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78775">#78775</a> (TLS issues from HTTP request affecting other encrypted connections).</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78697">#78697</a> (ReflectionClass::ImplementsInterface - inaccurate error message with traits).</li>
</ul></li>
<li>Sockets:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78665">#78665</a> (Multicasting may leak memory).</li>
</ul></li>
</ul>
<!-- }}} --></section>



<section class="version" id="7.2.24"><!-- {{{ 7.2.24 -->
<h3>Version 7.2.24</h3>
<b><time class='releasedate' datetime='2019-10-24'>24 Oct 2019</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78535">#78535</a> (auto_detect_line_endings value not parsed as bool).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78620">#78620</a> (Out of memory error).</li>
</ul></li>
<li>Exif:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78442">#78442</a> ('Illegal component' on exif_read_data since PHP7) (Kalle)</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78599">#78599</a> (env_path_info underflow in fpm_main.c can lead to RCE). (CVE-2019-11043)</li>
</ul></li>
<li>MBString:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78579">#78579</a> (mb_decode_numericentity: args number inconsistency).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78609">#78609</a> (mb_check_encoding() no longer supports stringable objects).</li>
</ul></li>
<li>MySQLi:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76809">#76809</a> (SSL settings aren't respected when persistent connections are used).</li>
</ul></li>
<li>PDO_MySQL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78623">#78623</a> (Regression caused by "SP call yields additional empty result set").</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78624">#78624</a> (session_gc return value for user defined session handlers).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76342">#76342</a> (file_get_contents waits twice specified timeout).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78612">#78612</a> (strtr leaks memory when integer keys are used and the subject string shorter).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76859">#76859</a> (stream_get_line skips data if used with data-generating filter).</li>
</ul></li>
<li>Zip:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78641">#78641</a> (addGlob can modify given remove_path value).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.2.23"><!-- {{{ 7.2.23 -->
<h3>Version 7.2.23</h3>
<b><time class='releasedate' datetime='2019-09-26'>26 Sep 2019</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78220">#78220</a> (Can't access OneDrive folder).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78412">#78412</a> (Generator incorrectly reports non-releasable $this as GC child).</li>
</ul></li>
<li>FastCGI:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78469">#78469</a> (FastCGI on_accept hook is not called when using named pipes on Windows).</li>
</ul></li>
<li>MySQLnd:
<ul>
  <li>Fixed connect_attr issues and added the _server_host connection attribute.</li>
</ul></li>
<li>ODBC:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78473">#78473</a> (odbc_close() closes arbitrary resources).</li>
</ul></li>
<li>PDO_MySQL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/41997">#41997</a> (SP call yields additional empty result set).</li>
</ul></li>
<li>sodium:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78510">#78510</a> (Partially uninitialized buffer returned by sodium_crypto_generichash_init()).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72884">#72884</a> (SplObject isCloneable() returns true but errs on clone).</li>
</ul></li>
</ul>
<!-- }}} --></section>



<section class="version" id="7.2.22"><!-- {{{ 7.2.22 -->
<h3>Version 7.2.22</h3>
<b><time class='releasedate' datetime='2019-08-29'>29 Aug 2019</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78363">#78363</a> (Buffer overflow in zendparse).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78379">#78379</a> (Cast to object confuses GC, causes crash).</li>
</ul></li>
<li>Curl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77946">#77946</a> (Bad cURL resources returned by curl_multi_info_read()).</li>
</ul></li>
<li>Exif:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78333">#78333</a> (Exif crash (bus error) due to wrong alignment and invalid cast).</li>
</ul></li>
<li>Iconv:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78342">#78342</a> (Bus error in configure test for iconv //IGNORE).</li>
</ul></li>
<li>LiteSpeed:
<ul>
  <li>Updated to LiteSpeed SAPI V7.5 (Fixed clean shutdown).</li>
</ul></li>
<li>MySQLnd:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78179">#78179</a> (MariaDB server version incorrectly detected).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77191">#77191</a> (Assertion failure in dce_live_ranges() when silencing is used).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69100">#69100</a> (Bus error from stream_copy_to_stream (file -&gt; SSL stream) with invalid length).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78282">#78282</a> (atime and mtime mismatch).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78326">#78326</a> (improper memory deallocation on stream_get_contents() with fixed length buffer).</li>
</ul></li>
</ul>
<!-- }}} --></section>



<section class="version" id="7.2.21"><!-- {{{ 7.2.21 -->
<h3>Version 7.2.21</h3>
<b><time class='releasedate' datetime='2019-08-01'>01 Aug 2019</time></b>
<ul><li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69044">#69044</a> (discrepency between time and microtime).</li>
</ul></li>
<li>EXIF:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78256">#78256</a> (heap-buffer-overflow on exif_process_user_comment). (CVE-2019-11042)</li>
  <li>Fixed bug <a href="http://bugs.php.net/78222">#78222</a> (heap-buffer-overflow on exif_scan_thumbnail). (CVE-2019-11041)</li>
</ul></li>
<li>Fileinfo:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78183">#78183</a> (finfo_file shows wrong mime-type for .tga file).</li>
</ul></li>
<li>FTP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77124">#77124</a> (FTP with SSL memory leak).</li>
</ul></li>
<li>Libxml:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78279">#78279</a> (libxml_disable_entity_loader settings is shared between requests (cgi-fcgi)).</li>
</ul></li>
<li>LiteSpeed:
<ul>
  <li>Updated to LiteSpeed SAPI V7.4.3 (increased response header count limit from 100 to 1000, added crash handler to cleanly shutdown PHP request, added CloudLinux mod_lsapi mode).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76058">#76058</a> (After "POST data can't be buffered", using php://input makes huge tmp files).</li>
</ul></li>
<li>Openssl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78231">#78231</a> (Segmentation fault upon stream_socket_accept of exported socket-to-stream).</li>
</ul></li>
<li>OPcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78189">#78189</a> (file cache strips last character of uname hash).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78202">#78202</a> (Opcache stats for cache hits are capped at 32bit NUM).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78291">#78291</a> (opcache_get_configuration doesn't list all directives).</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77919">#77919</a> (Potential UAF in Phar RSHUTDOWN).</li>
</ul></li>
<li>Phpdbg:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78297">#78297</a> (Include unexistent file memory leak).</li>
</ul></li>
<li>PDO_Sqlite:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78192">#78192</a> (SegFault when reuse statement after schema has changed).</li>
</ul></li>
<li>SQLite:
<ul>
  <li>Upgraded to SQLite 3.28.0.</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78241">#78241</a> (touch() does not handle dates after 2038 in PHP 64-bit).</li>
  <li>Fixed bug <a href="http://bugs.php.net/78269">#78269</a> (password_hash uses weak options for argon2).</li>
</ul></li>
<li>XMLRPC:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78173">#78173</a> (XML-RPC mutates immutable objects during encoding).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.2.20"><!-- {{{ 7.2.20 -->
<h3>Version 7.2.20</h3>
<b><time class='releasedate' datetime='2019-07-04'>04 Jul 2019</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76980">#76980</a> (Interface gets skipped if autoloader throws an exception).</li>
</ul></li>
<li>DOM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78025">#78025</a> (segfault when accessing properties of DOMDocumentType).</li>
</ul></li>
<li>MySQLi:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77956">#77956</a> (When mysqli.allow_local_infile = Off, use a meaningful error message).</li>
  <li>Fixed bug <a href="http://bugs.php.net/38546">#38546</a> (bindParam incorrect processing of bool types).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78106">#78106</a> (Path resolution fails if opcache disabled during request).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78079">#78079</a> (openssl_encrypt_ccm.phpt fails with OpenSSL 1.1.1c).</li>
</ul></li>
<li>Sockets:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78038">#78038</a> (Socket_select fails when resource array contains references).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77135">#77135</a> (Extract with EXTR_SKIP should skip $this).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77937">#77937</a> (preg_match failed).</li>
</ul></li>
<li>Zip:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76345">#76345</a> (zip.h not found).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.2.19"><!-- {{{ 7.2.19 -->
<h3>Version 7.2.19</h3>
<b><time class='releasedate' datetime='2019-05-30'>30 May 2019</time></b>
<ul><li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77909">#77909</a> (DatePeriod::__construct() with invalid recurrence count value).</li>
</ul></li>
<li>EXIF:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77988">#77988</a> (heap-buffer-overflow on php_jpg_get16) (CVE-2019-11040).</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77934">#77934</a> (php-fpm kill -USR2 not working).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77921">#77921</a> (static.php.net doesn't work anymore).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77943">#77943</a> (imageantialias($image, false); does not work).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77973">#77973</a> (Uninitialized read in gdImageCreateFromXbm) (CVE-2019-11038).</li>
</ul></li>
<li>Iconv:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78069">#78069</a> (Out-of-bounds read in iconv.c:_php_iconv_mime_decode() due to integer overflow) (CVE-2019-11039).</li>
</ul></li>
<li>JSON:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77843">#77843</a> (Use after free with json serializer).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed possible crashes, because of inconsistent PCRE cache and opcache SHM reset.</li>
</ul></li>
<li>PDO_MySQL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77944">#77944</a> (Wrong meta pdo_type for bigint on LLP64).</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75186">#75186</a> (Inconsistent reflection of Closure:::__invoke()).</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77911">#77911</a> (Wrong warning for session.sid_bits_per_character).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77024">#77024</a> (SplFileObject::__toString() may return array).</li>
</ul></li>
<li>SQLite:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77967">#77967</a> (Bypassing open_basedir restrictions via file uris).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.2.18"><!-- {{{ 7.2.18 -->
<h3>Version 7.2.18</h3>
<b><time class='releasedate' datetime='2019-05-02'>02 May 2019</time></b>
<ul><li>CLI:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77794">#77794</a> (Incorrect Date header format in built-in server).</li>
</ul></li>
<li>EXIF:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77950">#77950</a> (Heap-buffer-overflow in _estrndup via exif_process_IFD_TAG) (CVE-2019-11036).</li>
</ul></li>
<li>Interbase:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72175">#72175</a> (Impossibility of creating multiple connections to Interbase with php 7.x).</li>
</ul></li>
<li>Intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77895">#77895</a> (IntlDateFormatter::create fails in strict mode if $locale = null).</li>
</ul></li>
<li>litespeed:
<ul>
  <li>LiteSpeed SAPI 7.3.1, better process management, new API function litespeed_finish_request().</li>
</ul></li>
<li>Mail:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77821">#77821</a> (Potential heap corruption in TSendMail()).</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77827">#77827</a> (preg_match does not ignore \r in regex flags).</li>
</ul></li>
<li>PDO:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77849">#77849</a> (Disable cloning of PDO handle/connection objects).</li>
</ul></li>
<li>phpdbg:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76801">#76801</a> (too many open files).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77800">#77800</a> (phpdbg segfaults on listing some conditional breakpoints).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77805">#77805</a> (phpdbg build fails when readline is shared).</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77772">#77772</a> (ReflectionClass::getMethods(null) doesn't work).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77882">#77882</a> (Different behavior: always calls destructor).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77680">#77680</a> (recursive mkdir on ftp stream wrapper is incorrect).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77844">#77844</a> (Crash due to null pointer in parse_ini_string with INI_SCANNER_TYPED).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77853">#77853</a> (Inconsistent substr_compare behaviour with empty haystack).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.2.17"><!-- {{{ 7.2.17 -->
<h3>Version 7.2.17</h3>
<b><time class='releasedate' datetime='2019-04-04'>04 Apr 2019</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77738">#77738</a> (Nullptr deref in zend_compile_expr).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77660">#77660</a> (Segmentation fault on break 2147483648).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77652">#77652</a> (Anonymous classes can lose their interface information).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77676">#77676</a> (Unable to run tests when building shared extension on AIX).</li>
</ul></li>
<li>Bcmath:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77742">#77742</a> (bcpow() implementation related to gcc compiler optimization).</li>
</ul></li>
<li>COM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77578">#77578</a> (Crash when php unload).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/50020">#50020</a> (DateInterval:createDateFromString() silently fails).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75113">#75113</a> (Added DatePeriod::getRecurrences() method).</li>
</ul></li>
<li>EXIF:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77753">#77753</a> (Heap-buffer-overflow in php_ifd_get32s). (CVE-2019-11034)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77831">#77831</a> (Heap-buffer-overflow in exif_iif_add_value). (CVE-2019-11035)</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77677">#77677</a> (FPM fails to build on AIX due to missing WCOREDUMP).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77700">#77700</a> (Writing truecolor images as GIF ignores interlace flag).</li>
</ul></li>
<li>MySQLi:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77597">#77597</a> (mysqli_fetch_field hangs scripts).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77691">#77691</a> (Opcache passes wrong value for inline array push assignments).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77743">#77743</a> (Incorrect pi node insertion for jmpznz with identical successors).</li>
</ul></li>
<li>phpdbg:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77767">#77767</a> (phpdbg break cmd aliases listed in help do not match actual aliases).</li>
</ul></li>
<li>sodium:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77646">#77646</a> (sign_detached() strings not terminated).</li>
</ul></li>
<li>SQLite3:
<ul>
  <li>Added sqlite3.defensive INI directive.</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77664">#77664</a> (Segmentation fault when using undefined constant in custom wrapper).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77669">#77669</a> (Crash in extract() when overwriting extracted array).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76717">#76717</a> (var_export() does not create a parsable value for PHP_INT_MIN).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77765">#77765</a> (FTP stream wrapper should set the directory as executable).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.2.16"><!-- {{{ 7.2.16 -->
<h3>Version 7.2.16</h3>
<b><time class='releasedate' datetime='2019-03-07'>07 Mar 2019</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77589">#77589</a> (Core dump using parse_ini_string with numeric sections).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77630">#77630</a> (rename() across the device may allow unwanted access during processing). (CVE-2019-9637)</li>
</ul></li>
<li>COM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77621">#77621</a> (Already defined constants are not properly reported).</li>
</ul></li>
<li>EXIF:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77509">#77509</a> (Uninitialized read in exif_process_IFD_in_TIFF). (CVE-2019-9641)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77540">#77540</a> (Invalid Read on exif_process_SOFn). (CVE-2019-9640)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77563">#77563</a> (Uninitialized read in exif_process_IFD_in_MAKERNOTE). (CVE-2019-9638)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77659">#77659</a> (Uninitialized read in exif_process_IFD_in_MAKERNOTE). (CVE-2019-9639)</li>
</ul></li>
<li>PDO_OCI:
<ul>
  <li>Support Oracle Database tracing attributes ACTION, MODULE, CLIENT_INFO, and CLIENT_IDENTIFIER.</li>
</ul></li>
<li>PHAR:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77396">#77396</a> (Null Pointer Dereference in phar_create_or_parse_filename).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/51068">#51068</a> (DirectoryIterator glob:// don't support current path relative queries).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77431">#77431</a> (openFile() silently truncates after a null byte).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77552">#77552</a> (Unintialized php_stream_statbuf in stat functions).</li>
</ul></li>
<li>MySQL:
<ul>
  <li>Disabled LOCAL INFILE by default, can be enabled using php.ini directive mysqli.allow_local_infile for mysqli, or PDO::MYSQL_ATTR_LOCAL_INFILE attribute for pdo_mysql.</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.2.15"><!-- {{{ 7.2.15 -->
<h3>Version 7.2.15</h3>
<b><time class='releasedate' datetime='2019-02-07'>07 Feb 2019</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77339">#77339</a> (__callStatic may get incorrect arguments).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77494">#77494</a> (Disabling class causes segfault on member access).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77530">#77530</a> (PHP crashes when parsing `(2)::class`).</li>
</ul></li>
<li>Curl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76675">#76675</a> (Segfault with H2 server push).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73281">#73281</a> (imagescale(…, IMG_BILINEAR_FIXED) can cause black border).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73614">#73614</a> (gdImageFilledArc() doesn't properly draw pies).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77272">#77272</a> (imagescale() may return image resource on failure).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77391">#77391</a> (1bpp BMPs may fail to be loaded).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77479">#77479</a> (imagewbmp() segfaults with very large images).</li>
</ul></li>
<li>ldap:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77440">#77440</a> (ldap_bind using ldaps or ldap_start_tls()=exception in libcrypto-1_1-x64.dll).</li>
</ul></li>
<li>Mbstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77454">#77454</a> (mb_scrub() silently truncates after a null byte).</li>
</ul></li>
<li>MySQLnd:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75684">#75684</a> (In mysqlnd_ext_plugin.h the plugin methods family has no external visibility).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77361">#77361</a> (configure fails on 64-bit AIX when opcache enabled).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77390">#77390</a> (feof might hang on TLS streams in case of fragmented TLS records).</li>
</ul></li>
<li>PDO:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77273">#77273</a> (array_walk_recursive corrupts value types leading to PDO failure).</li>
</ul></li>
<li>Sockets:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76839">#76839</a> (socket_recvfrom may return an invalid 'from' address on MacOS).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77395">#77395</a> (segfault about array_multisort).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77439">#77439</a> (parse_str segfaults when inserting item into existing array).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.2.14"><!-- {{{ 7.2.14 -->
<h3>Version 7.2.14</h3>
<b><time class='releasedate' datetime='2019-01-10'>10 Jan 2019</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77369">#77369</a> (memcpy with negative length via crafted DNS response). (CVE-2019-9022)</li>
  <li>Fixed bug <a href="http://bugs.php.net/71041">#71041</a> (zend_signal_startup() needs ZEND_API).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76046">#76046</a> (PHP generates "FE_FREE" opcode on the wrong line).</li>
</ul></li>
<li>COM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77177">#77177</a> (Serializing or unserializing COM objects crashes).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77097">#77097</a> (DateTime::diff gives wrong diff when the actual diff is less than 1 second).</li>
</ul></li>
<li>Exif:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77184">#77184</a> (Unsigned rational numbers are written out as signed rationals).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77269">#77269</a> (efree() on uninitialized Heap data in imagescale leads to use-after-free). (CVE-2016-10166)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77270">#77270</a> (imagecolormatch Out Of Bounds Write on Heap). (CVE-2019-6977)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77195">#77195</a> (Incorrect error handling of imagecreatefromjpeg()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77198">#77198</a> (auto cropping has insufficient precision).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77200">#77200</a> (imagecropauto(…, GD_CROP_SIDES) crops left but not right).</li>
</ul></li>
<li>IMAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77020">#77020</a> (null pointer dereference in imap_mail).</li>
</ul></li>
<li>Mbstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77370">#77370</a> (Buffer overflow on mb regex functions - fetch_token). (CVE-2019-9023)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77371">#77371</a> (heap buffer overflow in mb regex functions - compile_string_node). (CVE-2019-9023)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77381">#77381</a> (heap buffer overflow in multibyte match_at). (CVE-2019-9023)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77382">#77382</a> (heap buffer overflow due to incorrect length in expand_case_fold_string). (CVE-2019-9023)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77385">#77385</a> (buffer overflow in fetch_token). (CVE-2019-9023)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77394">#77394</a> (Buffer overflow in multibyte case folding - unicode). (CVE-2019-9023)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77418">#77418</a> (Heap overflow in utf32be_mbc_to_code). (CVE-2019-9023)</li>
</ul></li>
<li>OCI8:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76804">#76804</a> (oci_pconnect with OCI_CRED_EXT not working).</li>
  <li>Added oci_set_call_timeout() for call timeouts.</li>
  <li>Added oci_set_db_operation() for the DBOP end-to-end-tracing attribute.</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77215">#77215</a> (CFG assertion failure on multiple finalizing switch frees in one block).</li>
</ul></li>
<li>PDO:
<ul>
  <li>Handle invalid index passed to PDOStatement::fetchColumn() as error.</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77247">#77247</a> (heap buffer overflow in phar_detect_phar_fname_ext). (CVE-2019-9021)</li>
</ul></li>
<li>Sockets:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77136">#77136</a> (Unsupported IPV6_RECVPKTINFO constants on macOS).</li>
</ul></li>
<li>SQLite3:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77051">#77051</a> (Issue with re-binding on SQLite3).</li>
</ul></li>
<li>Xmlrpc:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77242">#77242</a> (heap out of bounds read in xmlrpc_decode()). (CVE-2019-9020)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77380">#77380</a> (Global out of bounds read in xmlrpc base64 code). (CVE-2019-9024)</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.2.13"><!-- {{{ 7.2.13 -->
<h3>Version 7.2.13</h3>
<b><time class='releasedate' datetime='2018-12-06'>06 Dec 2018</time></b>
<ul><li>ftp:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77151">#77151</a> (ftp_close(): SSL_read on shutdown).</li>
</ul></li>
<li>CLI:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77111">#77111</a> (php-win.exe corrupts unicode symbols from cli parameters).</li>
</ul></li>
<li>Fileinfo:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77095">#77095</a> (slowness regression in 7.2/7.3 (compared to 7.1)).</li>
</ul></li>
<li>iconv:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77147">#77147</a> (Fixing 60494 ignored ICONV_MIME_DECODE_CONTINUE_ON_ERROR).</li>
</ul></li>
<li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77231">#77231</a> (Segfault when using convert.quoted-printable-encode filter).</li>
</ul></li>
<li>IMAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77153">#77153</a> (imap_open allows to run arbitrary shell commands via mailbox parameter). (CVE-2018-19518)</li>
</ul></li>
<li>ODBC:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77079">#77079</a> (odbc_fetch_object has incorrect type signature).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77058">#77058</a> (Type inference in opcache causes side effects).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77092">#77092</a> (array_diff_key() - segmentation fault).</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77022">#77022</a> (PharData always creates new files with mode 0666).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77143">#77143</a> (Heap Buffer Overflow (READ: 4) in phar_parse_pharfile). (CVE-2018-20783)</li>
</ul></li>
<li>PGSQL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77047">#77047</a> (pg_convert has a broken regex for the 'TIME WITHOUT TIMEZONE' data type).</li>
</ul></li>
<li>SOAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/50675">#50675</a> (SoapClient can't handle object references correctly).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76348">#76348</a> (WSDL_CACHE_MEMORY causes Segmentation fault).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77141">#77141</a> (Signedness issue in SOAP when precision=-1).</li>
</ul></li>
<li>Sockets:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/67619">#67619</a> (Validate length on socket_write).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.2.12"><!-- {{{ 7.2.12 -->
<h3>Version 7.2.12</h3>
<b><time class='releasedate' datetime='2018-11-08'>08 Nov 2018</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76846">#76846</a> (Segfault in shutdown function after memory limit error).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76946">#76946</a> (Cyclic reference in generator not detected).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77035">#77035</a> (The phpize and ./configure create redundant .deps file).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77041">#77041</a> (buildconf should output error messages to stderr) (Mizunashi Mana)</li>
</ul></li>
<li>Date:
<ul>
  <li>Upgraded timelib to 2017.08.</li>
  <li>Fixed bug <a href="http://bugs.php.net/75851">#75851</a> (Year component overflow with date formats "c", "o", "r" and "y").</li>
  <li>Fixed bug <a href="http://bugs.php.net/77007">#77007</a> (fractions in `diff()` are not correctly normalized).</li>
</ul></li>
<li>FCGI:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76948">#76948</a> (Failed shutdown/reboot or end session in Windows).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76954">#76954</a> (apache_response_headers removes last character from header name).</li>
</ul></li>
<li>FTP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76972">#76972</a> (Data truncation due to forceful ssl socket shutdown).</li>
</ul></li>
<li>intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76942">#76942</a> (U_ARGUMENT_TYPE_MISMATCH).</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76936">#76936</a> (Objects cannot access their private attributes while handling reflection errors).</li>
  <li>Fixed bug <a href="http://bugs.php.net/66430">#66430</a> (ReflectionFunction::invoke does not invoke closure with object scope).</li>
</ul></li>
<li>Sodium:
<ul>
  <li>Some base64 outputs were truncated; this is not the case any more.</li>
  <li>block sizes &gt;= 256 bytes are now supposed by sodium_pad() even when an old version of libsodium has been installed.</li>
  <li>Fixed bug <a href="http://bugs.php.net/77008">#77008</a> (sodium_pad() could read (but not return nor write) uninitialized memory when trying to pad an empty input).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76965">#76965</a> (INI_SCANNER_RAW doesn't strip trailing whitespace).</li>
</ul></li>
<li>Tidy:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77027">#77027</a> (tidy::getOptDoc() not available on Windows).</li>
</ul></li>
<li>XML:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/30875">#30875</a> (xml_parse_into_struct() does not resolve entities).</li>
  <li>Add support for getting SKIP_TAGSTART and SKIP_WHITE options.</li>
</ul></li>
<li>XMLRPC:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75282">#75282</a> (xmlrpc_encode_request() crashes).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.2.11"><!-- {{{ 7.2.11 -->
<h3>Version 7.2.11</h3>
<b><time class='releasedate' datetime='2018-10-11'>11 Oct 2018</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76800">#76800</a> (foreach inconsistent if array modified during loop).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76901">#76901</a> (method_exists on SPL iterator passthrough method corrupts memory).</li>
</ul></li>
<li>CURL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76480">#76480</a> (Use curl_multi_wait() so that timeouts are respected).</li>
</ul></li>
<li>iconv:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/66828">#66828</a> (iconv_mime_encode Q-encoding longer than it should be).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76832">#76832</a> (ZendOPcache.MemoryBase periodically deleted by the OS).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76796">#76796</a> (Compile-time evaluation of disabled function in opcache causes segfault).</li>
</ul></li>
<li>POSIX:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75696">#75696</a> (posix_getgrnam fails to print details of group).</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74454">#74454</a> (Wrong exception being thrown when using ReflectionMethod).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73457">#73457</a> (Wrong error message when fopen FTP wrapped fails to open data connection).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74764">#74764</a> (Bindto IPv6 works with file_get_contents but fails with stream_socket_client).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75533">#75533</a> (array_reduce is slow when $carry is large array).</li>
</ul></li>
<li>XMLRPC:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76886">#76886</a> (Can't build xmlrpc with expat).</li>
</ul></li>
<li>Zlib:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75273">#75273</a> (php_zlib_inflate_filter() may not update bytes_consumed).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.2.10"><!-- {{{ 7.2.10 -->
<h3>Version 7.2.10</h3>
<b><time class='releasedate' datetime='2018-09-13'>13 Sep 2018</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76754">#76754</a> (parent private constant in extends class memory leak).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72443">#72443</a> (Generate enabled extension).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75797">#75797</a> (Memory leak when using class_alias() in non-debug mode).</li>
</ul></li>
<li>Apache2:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76582">#76582</a> (XSS due to the header Transfer-Encoding: chunked). (CVE-2018-17082)</li>
</ul></li>
<li>Bz2:
<ul>
  <li>Fixed arginfo for bzcompress.</li>
</ul></li>
<li>gettext:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76517">#76517</a> (incorrect restoring of LDFLAGS).</li>
</ul></li>
<li>iconv:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/68180">#68180</a> (iconv_mime_decode can return extra characters in a header).</li>
  <li>Fixed bug <a href="http://bugs.php.net/63839">#63839</a> (iconv_mime_decode_headers function is skipping headers).</li>
  <li>Fixed bug <a href="http://bugs.php.net/60494">#60494</a> (iconv_mime_decode does ignore special characters).</li>
  <li>Fixed bug <a href="http://bugs.php.net/55146">#55146</a> (iconv_mime_decode_headers() skips some headers).</li>
</ul></li>
<li>intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74484">#74484</a> (MessageFormatter::formatMessage memory corruption with 11+ named placeholders).</li>
</ul></li>
<li>libxml:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76777">#76777</a> ("public id" parameter of libxml_set_external_entity_loader callback undefined).</li>
</ul></li>
<li>mbstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76704">#76704</a> (mb_detect_order return value varies based on argument type).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76747">#76747</a> (Opcache treats path containing "test.pharma.tld" as a phar file).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76705">#76705</a> (unusable ssl =&gt; peer_fingerprint in stream_context_create()).</li>
</ul></li>
<li>phpdbg:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76595">#76595</a> (phpdbg man page contains outdated information).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/68825">#68825</a> (Exception in DirectoryIterator::getLinkTarget()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/68175">#68175</a> (RegexIterator pregFlags are NULL instead of 0).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76778">#76778</a> (array_reduce leaks memory if callback throws exception).</li>
</ul></li>
<li>zlib:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/65988">#65988</a> (Zlib version check fails when an include/zlib/ style dir is passed to the --with-zlib configure option).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76709">#76709</a> (Minimal required zlib library is 1.2.0.4).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.2.9"><!-- {{{ 7.2.9 -->
<h3>Version 7.2.9</h3>
<b><time class='releasedate' datetime='2018-08-16'>16 Aug 2018</time></b>
<ul><li>Calendar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/52974">#52974</a> (jewish.c: compile error under Windows with GBK charset).</li>
</ul></li>
<li>Filter:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76366">#76366</a> (References in sub-array for filtering breaks the filter).</li>
</ul></li>
<li>PDO_Firebird:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76488">#76488</a> (Memory leak when fetching a BLOB field).</li>
</ul></li>
<li>PDO_PgSQL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75402">#75402</a> (Possible Memory Leak using PDO::CURSOR_SCROLL option).</li>
</ul></li>
<li>SQLite3:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76665">#76665</a> (SQLite3Stmt::bindValue() with SQLITE3_FLOAT doesn't juggle).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73817">#73817</a> (Incorrect entries in get_html_translation_table).</li>
  <li>Fixed bug <a href="http://bugs.php.net/68553">#68553</a> (array_column: null values in $index_key become incrementing keys in result).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76643">#76643</a> (Segmentation fault when using `output_add_rewrite_var`).</li>
</ul></li>
<li>Zip:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76524">#76524</a> (ZipArchive memory leak (OVERWRITE flag and empty archive)).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.2.8"><!-- {{{ 7.2.8 -->
<h3>Version 7.2.8</h3>
<b><time class='releasedate' datetime='2018-07-19'>19 Jul 2018</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76534">#76534</a> (PHP hangs on 'illegal string offset on string references with an error handler).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76520">#76520</a> (Object creation leaks memory when executed over HTTP).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76502">#76502</a> (Chain of mixed exceptions and errors does not serialize properly).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76462">#76462</a> (Undefined property: DateInterval::$f).</li>
</ul></li>
<li>EXIF:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76409">#76409</a> (heap use after free in _php_stream_free). (CVE-2018-12882)</li>
  <li>Fixed bug <a href="http://bugs.php.net/76423">#76423</a> (Int Overflow lead to Heap OverFlow in exif_thumbnail_extract of exif.c). (CVE-2018-14883)</li>
  <li>Fixed bug <a href="http://bugs.php.net/76557">#76557</a> (heap-buffer-overflow (READ of size 48) while reading exif data). (CVE-2018-14851)</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73342">#73342</a> (Vulnerability in php-fpm by changing stdin to non-blocking).</li>
</ul></li>
<li>GMP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74670">#74670</a> (Integer Underflow when unserializing GMP and possible other classes).</li>
</ul></li>
<li>intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76556">#76556</a> (get_debug_info handler for BreakIterator shows wrong type).</li>
</ul></li>
<li>mbstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76532">#76532</a> (Integer overflow and excessive memory usage in mb_strimwidth).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76477">#76477</a> (Opcache causes empty return value).</li>
</ul></li>
<li>PGSQL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76548">#76548</a> (pg_fetch_result did not fetch the next row).</li>
</ul></li>
<li>phpdbg:
<ul>
  <li>Fix arginfo wrt. optional/required parameters.</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76536">#76536</a> (PHP crashes with core dump when throwing exception in error handler).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75231">#75231</a> (ReflectionProperty#getValue() incorrectly works with inherited classes).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76505">#76505</a> (array_merge_recursive() is duplicating sub-array keys).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71848">#71848</a> (getimagesize with $imageinfo returns false).</li>
</ul></li>
<li>Win32:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76459">#76459</a> (windows linkinfo lacks openbasedir check). (CVE-2018-15132)</li>
</ul></li>
<li>ZIP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76461">#76461</a> (OPSYS_Z_CPM defined instead of OPSYS_CPM).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.2.7"><!-- {{{ 7.2.7 -->
<h3>Version 7.2.7</h3>
<b><time class='releasedate' datetime='2018-06-21'>21 Jun 2018</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76337">#76337</a> (segfault when opcache enabled + extension use zend_register_class_alias).</li>
</ul></li>
<li>CLI Server:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76333">#76333</a> (PHP built-in server does not find files if root path contains special characters).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76296">#76296</a> (openssl_pkey_get_public does not respect open_basedir).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76174">#76174</a> (openssl extension fails to build with LibreSSL 2.7).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76367">#76367</a> (NoRewindIterator segfault 11).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76410">#76410</a> (SIGV in zend_mm_alloc_small).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76335">#76335</a> ("link(): Bad file descriptor" with non-ASCII path).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.2.6"><!-- {{{ 7.2.6 -->
<h3>Version 7.2.6</h3>
<b><time class='releasedate' datetime='2018-05-24'>24 May 2018</time></b>
<ul><li>EXIF:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76164">#76164</a> (exif_read_data zend_mm_heap corrupted).</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76075">#76075</a> --with-fpm-acl wrongly tries to find libacl on FreeBSD.</li>
</ul></li>
<li>intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74385">#74385</a> (Locale::parseLocale() broken with some arguments).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76205">#76205</a> (PHP-FPM sporadic crash when running Infinitewp).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76275">#76275</a> (Assertion failure in file cache when unserializing empty try_catch_array).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76281">#76281</a> (Opcache causes incorrect "undefined variable" errors).</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed arginfo of array_replace(_recursive) and array_merge(_recursive).</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74892">#74892</a> (Url Rewriting (trans_sid) not working on urls that start with "#").</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.2.5"><!-- {{{ 7.2.5 -->
<h3>Version 7.2.5</h3>
<b><time class='releasedate' datetime='2018-04-26'>26 Apr 2018</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75722">#75722</a> (Convert valgrind detection to configure option).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76131">#76131</a> (mismatch arginfo for date_create).</li>
</ul></li>
<li>Exif:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76130">#76130</a> (Heap Buffer Overflow (READ: 1786) in exif_iif_add_value). (CVE-2018-10549)</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/68440">#68440</a> (ERROR: failed to reload: execvp() failed: Argument list too long).</li>
  <li>Fixed incorrect write to getenv result in FPM reload.</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/52070">#52070</a> (imagedashedline() - dashed line sometimes is not visible).</li>
</ul></li>
<li>iconv:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76249">#76249</a> (stream filter convert.iconv leads to infinite loop on invalid sequence). (CVE-2018-10546)</li>
</ul></li>
<li>intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76153">#76153</a> (Intl compilation fails with icu4c 61.1).</li>
</ul></li>
<li>ldap:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76248">#76248</a> (Malicious LDAP-Server Response causes Crash). (CVE-2018-10548)</li>
</ul></li>
<li>mbstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75944">#75944</a> (Wrong cp1251 detection).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76113">#76113</a> (mbstring does not build with Oniguruma 6.8.1).</li>
</ul></li>
<li>ODBC:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76088">#76088</a> (ODBC functions are not available by default on Windows).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76094">#76094</a> (Access violation when using opcache).</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76129">#76129</a> (fix for CVE-2018-5712 may not be complete). (CVE-2018-10547)</li>
</ul></li>
<li>phpdbg:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76143">#76143</a> (Memory corruption: arbitrary NUL overwrite).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76131">#76131</a> (mismatch arginfo for splarray constructor).</li>
</ul></li>
<li>standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74139">#74139</a> (mail.add_x_header default inconsistent with docs).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75996">#75996</a> (incorrect url in header for mt_rand).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.2.4"><!-- {{{ 7.2.4 -->
<h3>Version 7.2.4</h3>
<b><time class='releasedate' datetime='2018-03-29'>29 Mar 2018</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76025">#76025</a> (Segfault while throwing exception in error_handler).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76044">#76044</a> ('date: illegal option -- -' in ./configure on FreeBSD).</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75605">#75605</a> (Dumpable FPM child processes allow bypassing opcache access controls). (CVE-2018-10545)</li>
</ul></li>
<li>FTP:
<ul>
  <li>Fixed ftp_pasv arginfo.</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73957">#73957</a> (signed integer conversion in imagescale()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76041">#76041</a> (null pointer access crashed php).</li>
  <li>Fixed imagesetinterpolation arginfo.</li>
</ul></li>
<li>iconv:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75867">#75867</a> (Freeing uninitialized pointer).</li>
</ul></li>
<li>Mbstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/62545">#62545</a> (wrong unicode mapping in some charsets).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75969">#75969</a> (Assertion failure in live range DCE due to block pass misoptimization).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed openssl_* arginfos.</li>
</ul></li>
<li>PCNTL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75873">#75873</a> (pcntl_wexitstatus returns incorrect on Big_Endian platform (s390x)).</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76085">#76085</a> (Segmentation fault in buildFromIterator when directory name contains a \n).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75961">#75961</a> (Strange references behavior).</li>
  <li>Fixed some arginfos.</li>
  <li>Fixed bug <a href="http://bugs.php.net/76068">#76068</a> (parse_ini_string fails to parse "[foo]\nbar=1|&gt;baz" with segfault).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.2.3"><!-- {{{ 7.2.3 -->
<h3>Version 7.2.3</h3>
<b><time class='releasedate' datetime='2018-03-01'>01 Mar 2018</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75864">#75864</a> ("stream_isatty" returns wrong value on s390x).</li>
</ul></li>
<li>Apache2Handler:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75882">#75882</a> (a simple way for segfaults in threadsafe php just with configuration).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75857">#75857</a> (Timezone gets truncated when formatted).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75928">#75928</a> (Argument 2 for `DateTimeZone::listIdentifiers()` should accept `null`).</li>
  <li>Fixed bug <a href="http://bugs.php.net/68406">#68406</a> (calling var_dump on a DateTimeZone object modifies it).</li>
</ul></li>
<li>LDAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/49876">#49876</a> (Fix LDAP path lookup on 64-bit distros).</li>
</ul></li>
<li>libxml2:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75871">#75871</a> (use pkg-config where available).</li>
</ul></li>
<li>PGSQL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75838">#75838</a> (Memory leak in pg_escape_bytea()).</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/54289">#54289</a> (Phar::extractTo() does not accept specific directories to be extracted).</li>
  <li>Fixed bug <a href="http://bugs.php.net/65414">#65414</a> (deal with leading slash while adding files correctly).</li>
  <li>Fixed bug <a href="http://bugs.php.net/65414">#65414</a> (deal with leading slash when adding files correctly).</li>
</ul></li>
<li>ODBC:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73725">#73725</a> (Unable to retrieve value of varchar(max) type).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75729">#75729</a> (opcache segfault when installing Bitrix).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75893">#75893</a> (file_get_contents $http_response_header variable bugged with opcache).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75938">#75938</a> (Modulus value not stored in variable).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74519">#74519</a> (strange behavior of AppendIterator).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75916">#75916</a> (DNS_CAA record results contain garbage).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75981">#75981</a> (stack-buffer-overflow while parsing HTTP response). (CVE-2018-7584)</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.2.2"><!-- {{{ 7.2.2 -->
<h3>Version 7.2.2</h3>
<b><time class='releasedate' datetime='2018-02-01'>01 Feb 2018</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75742">#75742</a> (potential memleak in internal classes's static members).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75679">#75679</a> (Path 260 character problem).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75614">#75614</a> (Some non-portable == in shell scripts).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75786">#75786</a> (segfault when using spread operator on generator passed by reference).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75799">#75799</a> (arg of get_defined_functions is optional).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75396">#75396</a> (Exit inside generator finally results in fatal error).</li>
</ul></li>
<li>FCGI:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75794">#75794</a> (getenv() crashes on Windows 7.2.1 when second parameter is false).</li>
</ul></li>
<li>IMAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75774">#75774</a> (imap_append HeapCorruction).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75720">#75720</a> (File cache not populated after SHM runs full).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75687">#75687</a> (var 8 (TMP) has array key type but not value type).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75698">#75698</a> (Using @ crashes php7.2-fpm).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75579">#75579</a> (Interned strings buffer overflow may cause crash).</li>
</ul></li>
<li>PDO:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75616">#75616</a> (PDO extension doesn't allow to be built shared on Darwin).</li>
</ul></li>
<li>PDO MySQL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75615">#75615</a> (PDO Mysql module can't be built as module).</li>
</ul></li>
<li>PGSQL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75671">#75671</a> (pg_version() crashes when called on a connection to cockroach).</li>
</ul></li>
<li>Readline:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75775">#75775</a> (readline_read_history segfaults with empty file).</li>
</ul></li>
<li>SAPI:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75735">#75735</a> ([embed SAPI] Segmentation fault in sapi_register_post_entry).</li>
</ul></li>
<li>SOAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70469">#70469</a> (SoapClient generates E_ERROR even if exceptions=1 is used).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75502">#75502</a> (Segmentation fault in zend_string_release).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75717">#75717</a> (RecursiveArrayIterator does not traverse arrays by reference).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75242">#75242</a> (RecursiveArrayIterator doesn't have constants from parent class).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73209">#73209</a> (RecursiveArrayIterator does not iterate object properties).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75781">#75781</a> (substr_count incorrect result).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75653">#75653</a> (array_values don't work on empty array).</li>
</ul></li>
<li>Zip:
<ul>
  <li>Display headers (buildtime) and library (runtime) versions in phpinfo (with libzip &gt;= 1.3.1).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.2.1"><!-- {{{ 7.2.1 -->
<h3>Version 7.2.1</h3>
<b><time class='releasedate' datetime='2018-01-04'>04 Jan 2018</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75573">#75573</a> (Segmentation fault in 7.1.12 and 7.0.26).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75384">#75384</a> (PHP seems incompatible with OneDrive files on demand).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75525">#75525</a> (Access Violation in vcruntime140.dll).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74862">#74862</a> (Unable to clone instance when private __clone defined).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75074">#75074</a> (php-process crash when is_file() is used with strings longer 260 chars).</li>
</ul></li>
<li>CLI server:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73830">#73830</a> (Directory does not exist).</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/64938">#64938</a> (libxml_disable_entity_loader setting is shared between requests).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75571">#75571</a> (Potential infinite loop in gdImageCreateFromGifCtx). (CVE-2018-5711)</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75608">#75608</a> ("Narrowing occurred during type inference" error).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75579">#75579</a> (Interned strings buffer overflow may cause crash).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75570">#75570</a> ("Narrowing occurred during type inference" error).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75556">#75556</a> (Invalid opcode 138/1/1).</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74183">#74183</a> (preg_last_error not returning error code after error).</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74782">#74782</a> (Reflected XSS in .phar 404 page). (CVE-2018-5712)</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75511">#75511</a> (fread not free unused buffer).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75514">#75514</a> (mt_rand returns value outside [$min,$max]+ on 32-bit) (Remi)</li>
  <li>Fixed bug <a href="http://bugs.php.net/75535">#75535</a> (Inappropriately parsing HTTP response leads to PHP segment fault). (CVE-2018-14884)</li>
  <li>Fixed bug <a href="http://bugs.php.net/75409">#75409</a> (accept EFAULT in addition to ENOSYS as indicator that getrandom() is missing).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73124">#73124</a> (php_ini_scanned_files() not reporting correctly).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75574">#75574</a> (putenv does not work properly if parameter contains non-ASCII unicode character).</li>
</ul></li>
<li>Zip:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75540">#75540</a> (Segfault with libzip 1.3.1).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.2.0"><!-- {{{ 7.2.0 -->
<h3>Version 7.2.0</h3>
<b><time class='releasedate' datetime='2017-11-30'>30 Nov 2017</time></b>
<ul><li>BCMath:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/46564">#46564</a> (bcmod truncates fractionals).</li>
</ul></li>
<li>CLI:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74849">#74849</a> (Process is started as interactive shell in PhpStorm).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74979">#74979</a> (Interactive shell opening instead of script execution with -f flag).</li>
</ul></li>
<li>CLI server:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/60471">#60471</a> (Random "Invalid request (unexpected EOF)" using a router script).</li>
</ul></li>
<li>Core:
<ul>
  <li>Added ZEND_COUNT, ZEND_GET_CLASS, ZEND_GET_CALLED_CLASS, ZEND_GET_TYPE, ZEND_FUNC_NUM_ARGS, ZEND_FUNC_GET_ARGS instructions, to implement corresponding builtin functions.</li>
  <li>"Countable" interface is moved from SPL to Core.</li>
  <li>Added ZEND_IN_ARRAY instruction, implementing optimized in_array() builtin function, through hash lookup in flipped array.</li>
  <li>Removed IS_TYPE_IMMUTABLE (it's the same as COPYABLE &amp; !REFCOUNTED).</li>
  <li>Removed the sql.safe_mode directive.</li>
  <li>Removed support for Netware.</li>
  <li>Renamed ReflectionClass::isIterateable() to ReflectionClass::isIterable() (alias original name for BC).</li>
  <li>Fixed bug <a href="http://bugs.php.net/54535">#54535</a> (WSA cleanup executes before MSHUTDOWN).</li>
  <li>Implemented FR <a href="http://bugs.php.net/69791">#69791</a> (Disallow mail header injections by extra headers) (Yasuo)</li>
  <li>Implemented FR <a href="http://bugs.php.net/49806">#49806</a> (proc_nice() for Windows).</li>
  <li>Fix pthreads detection when cross-compiling (ffontaine)</li>
  <li>Fixed memory leaks caused by exceptions thrown from destructors. (Bob, Dmitry).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73215">#73215</a> (uniqid() should use better random source).</li>
  <li>Implemented FR <a href="http://bugs.php.net/72768">#72768</a> (Add ENABLE_VIRTUAL_TERMINAL_PROCESSING flag for php.exe).</li>
  <li>Implemented "Convert numeric keys in object/array casts" RFC, fixes bugs <a href="http://bugs.php.net/53838">#53838</a>, <a href="http://bugs.php.net/61655">#61655</a>, <a href="http://bugs.php.net/66173">#66173</a>, <a href="http://bugs.php.net/70925">#70925</a>, <a href="http://bugs.php.net/72254">#72254</a>, etc.</li>
  <li>Implemented "Deprecate and Remove Bareword (Unquoted) Strings" RFC.</li>
  <li>Raised minimum supported Windows versions to Windows 7/Server 2008 R2.</li>
  <li>Implemented minor optimization in array_keys/array_values().</li>
  <li>Added PHP_OS_FAMILY constant to determine on which OS we are.</li>
  <li>Fixed bug <a href="http://bugs.php.net/73987">#73987</a> (Method compatibility check looks to original definition and not parent).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73991">#73991</a> (JSON_OBJECT_AS_ARRAY not respected).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74053">#74053</a> (Corrupted class entries on shutdown when a destructor spawns another object).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73971">#73971</a> (Filename got limited to MAX_PATH on Win32 when scan directory).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72359">#72359</a>, bug <a href="http://bugs.php.net/72451">#72451</a>, bug <a href="http://bugs.php.net/73706">#73706</a>, bug <a href="http://bugs.php.net/71115">#71115</a> and others related to interned strings handling in TS builds.</li>
  <li>Implemented "Trailing Commas In List Syntax" RFC for group use lists only.</li>
  <li>Fixed bug <a href="http://bugs.php.net/74269">#74269</a> (It's possible to override trait property with different loosely-equal value).</li>
  <li>Fixed bug <a href="http://bugs.php.net/61970">#61970</a> (Restraining __construct() access level in subclass gives a fatal error).</li>
  <li>Fixed bug <a href="http://bugs.php.net/63384">#63384</a> (Cannot override an abstract method with an abstract method).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74607">#74607</a> (Traits enforce different inheritance rules).</li>
  <li>Fixed misparsing of abstract unix domain socket names.</li>
  <li>Change PHP_OS_FAMILY value from "OSX" to "Darwin".</li>
  <li>Allow loading PHP/Zend extensions by name in ini files (extension=&lt;name&gt;).</li>
  <li>Added object type annotation.</li>
  <li>Fixed bug <a href="http://bugs.php.net/74815">#74815</a> (crash with a combination of INI entries at startup).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74836">#74836</a> (isset on zero-prefixed numeric indexes in array broken).</li>
  <li>Added new VM instuctions ISSET_ISEMPTY_CV and UNSET_CV. Previously they were implemented as ISSET_ISEMPTY_VAR and UNSET_VAR variants with ZEND_QUICK_SET flag.</li>
  <li>Fixed bug <a href="http://bugs.php.net/49649">#49649</a> (unserialize() doesn't handle changes in property visibility).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74866">#74866</a> (extension_dir = "./ext" now use current directory for base).</li>
  <li>Implemented FR <a href="http://bugs.php.net/74963">#74963</a> (Improved error message on fetching property of non-object).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75142">#75142</a> (buildcheck.sh check for autoconf version needs to be updated for v2.64).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74878">#74878</a> (Data race in ZTS builds).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75515">#75515</a> ("stream_copy_to_stream" doesn't stream anymore).</li>
</ul></li>
<li>cURL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75093">#75093</a> (OpenSSL support not detected).</li>
  <li>Better fix for <a href="http://bugs.php.net/74125">#74125</a> (use pkg-config instead of curl-config).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/55407">#55407</a> (Impossible to prototype DateTime::createFromFormat).</li>
  <li>Implemented FR <a href="http://bugs.php.net/71520">#71520</a> (Adding the DateTime constants to the DateTimeInterface interface).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75055">#75055</a> (Out-Of-Bounds Read in timelib_meridian()). (CVE-2017-16642)</li>
  <li>Fixed bug <a href="http://bugs.php.net/75149">#75149</a> (redefinition of typedefs ttinfo and t1info).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75222">#75222</a> (DateInterval microseconds property always 0).</li>
</ul></li>
<li>Dba:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72885">#72885</a> (flatfile: dba_fetch() fails to read replaced entry).</li>
</ul></li>
<li>DOM:
<ul>
  <li>Implemented FR <a href="http://bugs.php.net/74837">#74837</a> (Implement Countable for DomNodeList and DOMNamedNodeMap).</li>
</ul></li>
<li>EXIF:
<ul>
  <li>Added support for vendor specific tags for the following formats: Samsung, DJI, Panasonic, Sony, Pentax, Minolta, Sigma/Foveon, AGFA, Kyocera, Ricoh &amp; Epson.</li>
  <li>Fixed bug <a href="http://bugs.php.net/72682">#72682</a> (exif_read_data() fails to read all data for some images).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71534">#71534</a> (Type confusion in exif_read_data() leading to heap overflow in debug mode).</li>
  <li>Fixed bug <a href="http://bugs.php.net/68547">#68547</a> (Exif Header component value check error).</li>
  <li>Fixed bug <a href="http://bugs.php.net/66443">#66443</a> (Corrupt EXIF header: maximum directory nesting level reached for some cameras).</li>
  <li>Fixed Redhat bug #1362571 (PHP not returning full results for exif_read_data function).</li>
  <li>Implemented FR <a href="http://bugs.php.net/65187">#65187</a> (exif_read_data/thumbnail: add support for stream resource).</li>
  <li>Deprecated the read_exif_data() alias.</li>
  <li>Fixed bug <a href="http://bugs.php.net/74428">#74428</a> (exif_read_data(): "Illegal IFD size" warning occurs with correct exif format).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72819">#72819</a> (EXIF thumbnails not read anymore).</li>
  <li>Fixed bug <a href="http://bugs.php.net/62523">#62523</a> (php crashes with segfault when exif_read_data called).</li>
  <li>Fixed bug <a href="http://bugs.php.net/50660">#50660</a> (exif_read_data(): Illegal IFD offset (works fine with other exif readers).</li>
</ul></li>
<li>Fileinfo:
<ul>
  <li>Upgrade bundled libmagic to 5.31.</li>
</ul></li>
<li>FPM:
<ul>
  <li>Configuration to limit fpm slow log trace callers.</li>
  <li>Fixed bug <a href="http://bugs.php.net/75212">#75212</a> (php_value acts like php_admin_value).</li>
</ul></li>
<li>FTP:
<ul>
  <li>Implement MLSD for structured listing of directories.</li>
  <li>Added ftp_append() function.</li>
</ul></li>
<li>GD:
<ul>
  <li>Implemented imageresolution as getter and setter (Christoph)</li>
  <li>Fixed bug <a href="http://bugs.php.net/74744">#74744</a> (gd.h: stdarg.h include missing for va_list use in gdErrorMethod).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75111">#75111</a> (Memory disclosure or DoS via crafted .bmp image).</li>
</ul></li>
<li>GMP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70896">#70896</a> (gmp_fact() silently ignores non-integer input).</li>
</ul></li>
<li>Hash:
<ul>
  <li>Changed HashContext from resource to object.</li>
  <li>Disallowed usage of non-cryptographic hash functions with HMAC and PBKDF2.</li>
  <li>Fixed bug <a href="http://bugs.php.net/75284">#75284</a> (sha3 is not supported on bigendian machine).</li>
</ul></li>
<li>IMAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72324">#72324</a> (imap_mailboxmsginfo() return wrong size).</li>
</ul></li>
<li>Intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/63790">#63790</a> (test using Spoofchecker which may be unavailable).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75378">#75378</a> ([REGRESSION] IntlDateFormatter::parse() does not change $position argument).</li>
</ul></li>
<li>JSON:
<ul>
  <li>Add JSON_INVALID_UTF8_IGNORE and JSON_INVALID_UTF8_SUBSTITUTE options for json_encode and json_decode to ignore or replace invalid UTF-8 byte sequences - it addresses request <a href="http://bugs.php.net/65082">#65082</a>.</li>
  <li>Fixed bug <a href="http://bugs.php.net/75185">#75185</a> (Buffer overflow in json_decode() with JSON_INVALID_UTF8_IGNORE or JSON_INVALID).</li>
  <li>Fixed bug <a href="http://bugs.php.net/68567">#68567</a> (JSON_PARTIAL_OUTPUT_ON_ERROR can result in JSON with null key).</li>
</ul></li>
<li>LDAP:
<ul>
  <li>Implemented FR <a href="http://bugs.php.net/69445">#69445</a> (Support for LDAP EXOP operations)</li>
  <li>Fixed support for LDAP_OPT_SERVER_CONTROLS and LDAP_OPT_CLIENT_CONTROLS in ldap_get_option</li>
  <li>Fixed passing an empty array to ldap_set_option for client or server controls.</li>
</ul></li>
<li>Mbstring:
<ul>
  <li>Implemented FR <a href="http://bugs.php.net/66024">#66024</a> (mb_chr() and mb_ord()).</li>
  <li>Implemented FR <a href="http://bugs.php.net/65081">#65081</a> (mb_scrub()).</li>
  <li>Implemented FR <a href="http://bugs.php.net/69086">#69086</a> (enhancement for mb_convert_encoding() that handles multibyte replacement char nicely).</li>
  <li>Added array input support to mb_convert_encoding().</li>
  <li>Added array input support to mb_check_encoding().</li>
  <li>Fixed bug <a href="http://bugs.php.net/69079">#69079</a> (enhancement for mb_substitute_character).</li>
  <li>Update to oniguruma version 6.3.0.</li>
  <li>Fixed bug <a href="http://bugs.php.net/69267">#69267</a> (mb_strtolower fails on titlecase characters).</li>
</ul></li>
<li>Mcrypt:
<ul>
  <li>The deprecated mcrypt extension has been moved to PECL.</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Added global optimisation passes based on data flow analysis using Single Static Assignment (SSA) form: Sparse Conditional Constant Propagation (SCCP), Dead Code Elimination (DCE), and removal of unused local variables (Nikita, Dmitry)</li>
  <li>Fixed incorect constant conditional jump elimination.</li>
  <li>Fixed bug <a href="http://bugs.php.net/75230">#75230</a> (Invalid opcode 49/1/8 using opcache).</li>
  <li>Fixed bug (assertion fails with extended info generated).</li>
  <li>Fixed bug (Phi sources removel).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75370">#75370</a> (Webserver hangs on valid PHP text).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75357">#75357</a> (segfault loading WordPress wp-admin).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Use TLS_ANY for default ssl:// and tls:// negotiation.</li>
  <li>Fix leak in openssl_spki_new().</li>
  <li>Added openssl_pkcs7_read() and pk7 parameter to openssl_pkcs7_verify().</li>
  <li>Add ssl security_level stream option to support OpenSSL security levels. (Jakub Zelenka).</li>
  <li>Allow setting SNI cert and private key in separate files.</li>
  <li>Fixed bug <a href="http://bugs.php.net/74903">#74903</a> (openssl_pkcs7_encrypt() uses different EOL than before).</li>
  <li>Automatically load OpenSSL configuration file.</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Added support for PCRE JIT fast path API.</li>
  <li>Fixed bug <a href="http://bugs.php.net/61780">#61780</a> (Inconsistent PCRE captures in match results).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74873">#74873</a> (Minor BC break: PCRE_JIT changes output of preg_match()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75089">#75089</a> (preg_grep() is not reporting PREG_BAD_UTF8_ERROR after first input string).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75223">#75223</a> (PCRE JIT broken in 7.2).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75285">#75285</a> (Broken build when system libpcre don't have jit support).</li>
</ul></li>
<li>phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74196">#74196</a> (phar does not correctly handle names containing dots).</li>
</ul></li>
<li>PDO:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73234">#73234</a> (Emulated statements let value dictate parameter type).</li>
  <li>Add "Sent SQL" to debug dump for emulated prepares.</li>
  <li>Add parameter types for national character set strings.</li>
</ul></li>
<li>PDO_DBlib:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73396">#73396</a> (bigint columns are returned as strings).</li>
  <li>Expose DB-Library version as \PDO::DBLIB_ATTR_VERSION attribute on \PDO instance.</li>
  <li>Add test coverage for bug <a href="http://bugs.php.net/72969">#72969</a>.</li>
</ul></li>
<li>PDO_OCI:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74537">#74537</a> (Align --with-pdo-oci configure option with --with-oci8 syntax).</li>
</ul></li>
<li>PDO_Sqlite:
<ul>
  <li>Switch to sqlite3_prepare_v2() and sqlite3_close_v2() functions (rasmus)</li>
</ul></li>
<li>PHPDBG:
<ul>
  <li>Added extended_value to opcode dump output.</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73461">#73461</a> (Prohibit session save handler recursion).</li>
  <li>PR #2233 Removed register_globals related code and "!" can be used as $_SESSION key name.</li>
  <li>Improved bug <a href="http://bugs.php.net/73100">#73100</a> fix. 'user' save handler can only be set by session_set_save_handler()</li>
  <li>Fixed bug <a href="http://bugs.php.net/74514">#74514</a> (5 session functions incorrectly warn when calling in read-only/getter mode).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74936">#74936</a> (session_cache_expire/cache_limiter/save_path() trigger a warning in read mode).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74941">#74941</a> (session fails to start after having headers sent).</li>
</ul></li>
<li>Sodium:
<ul>
  <li>New cryptographic extension</li>
  <li>Added missing bindings for libsodium &gt; 1.0.13.</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71412">#71412</a> (Incorrect arginfo for ArrayIterator::__construct).</li>
  <li>Added spl_object_id().</li>
</ul></li>
<li>SQLite3:
<ul>
  <li>Implement writing to blobs.</li>
  <li>Update to Sqlite 3.20.1.</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69442">#69442</a> (closing of fd incorrect when PTS enabled).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74300">#74300</a> (unserialize accepts two plus/minus signs for float number exponent part).</li>
  <li>Compatibility with libargon2 versions 20161029 and 20160821.</li>
  <li>Fixed bug <a href="http://bugs.php.net/74737">#74737</a> (mysqli_get_client_info reflection info).</li>
  <li>Add support for extension name as argument to dl().</li>
  <li>Fixed bug <a href="http://bugs.php.net/74851">#74851</a> (uniqid() without more_entropy performs badly).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74103">#74103</a> (heap-use-after-free when unserializing invalid array size). (CVE-2017-12932)</li>
  <li>Fixed bug <a href="http://bugs.php.net/75054">#75054</a> (A Denial of Service Vulnerability was found when performing deserialization).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75170">#75170</a> (mt_rand() bias on 64-bit machines).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75221">#75221</a> (Argon2i always throws NUL at the end).</li>
</ul></li>
<li>Streams:
<ul>
  <li>Default ssl/single_dh_use and ssl/honor_cipher_order to true.</li>
</ul></li>
<li>XML:
<ul>
  <li>Moved utf8_encode() and utf8_decode() to the Standard extension.</li>
</ul></li>
<li>XMLRPC:
<ul>
  <li>Use Zend MM for allocation in bundled libxmlrpc (Joe)</li>
</ul></li>
<li>ZIP:
<ul>
  <li>Add support for encrypted archives.</li>
  <li>Use of bundled libzip is deprecated, --with-libzip option is recommended.</li>
  <li>Fixed bug <a href="http://bugs.php.net/73803">#73803</a> (Reflection of ZipArchive does not show public properties).</li>
  <li>ZipArchive implements countable, added ZipArchive::count() method.</li>
  <li>Fix segfault in php_stream_context_get_option call.</li>
  <li>Fixed bug <a href="http://bugs.php.net/75143">#75143</a> (new method setEncryptionName() seems not to exist in ZipArchive).</li>
</ul></li>
<li>zlib:
<ul>
  <li>Expose inflate_get_status() and inflate_get_read_len() functions.</li>
</ul></li>
</ul>
<!-- }}} --></section>

<a name="PHP_7_1"></a>
<section class="version" id="7.1.33"><!-- {{{ 7.1.33 -->
<h3>Version 7.1.33</h3>
<b><time class='releasedate' datetime='2019-10-24'>24 Oct 2019</time></b>
<ul><li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78599">#78599</a> (env_path_info underflow in fpm_main.c can lead to RCE). (CVE-2019-11043)</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.32"><!-- {{{ 7.1.32 -->
<h3>Version 7.1.32</h3>
<b><time class='releasedate' datetime='2019-08-29'>29 Aug 2019</time></b>
<ul><li>mbstring:
<ul>
  <li>Fixed CVE-2019-13224 (don't allow different encodings for onig_new_deluxe) (stas)</li>
</ul></li>
<li>pcre:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75457">#75457</a> (heap use-after-free in pcrelib) (cmb)</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.31"><!-- {{{ 7.1.31 -->
<h3>Version 7.1.31</h3>
<b><time class='releasedate' datetime='2019-08-01'>01 Aug 2019</time></b>
<ul><li>SQLite:
<ul>
  <li>Upgraded to SQLite 3.28.0.</li>
</ul></li>
<li>EXIF:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78256">#78256</a> (heap-buffer-overflow on exif_process_user_comment). (CVE-2019-11042)</li>
  <li>Fixed bug <a href="http://bugs.php.net/78222">#78222</a> (heap-buffer-overflow on exif_scan_thumbnail). (CVE-2019-11041)</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77919">#77919</a> (Potential UAF in Phar RSHUTDOWN).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.30"><!-- {{{ 7.1.30 -->
<h3>Version 7.1.30</h3>
<b><time class='releasedate' datetime='2019-05-30'>30 May 2019</time></b>
<ul><li>EXIF:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77988">#77988</a> (heap-buffer-overflow on php_jpg_get16) (CVE-2019-11040).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77973">#77973</a> (Uninitialized read in gdImageCreateFromXbm) (CVE-2019-11038).</li>
</ul></li>
<li>Iconv:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/78069">#78069</a> (Out-of-bounds read in iconv.c:_php_iconv_mime_decode() due to integer overflow) (CVE-2019-11039).</li>
</ul></li>
<li>SQLite:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77967">#77967</a> (Bypassing open_basedir restrictions via file uris).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.29"><!-- {{{ 7.1.29 -->
<h3>Version 7.1.29</h3>
<b><time class='releasedate' datetime='2019-05-02'>02 May 2019</time></b>
<ul><li>EXIF:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77950">#77950</a> (Heap-buffer-overflow in _estrndup via exif_process_IFD_TAG) (CVE-2019-11036).</li>
</ul></li>
<li>Mail:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77821">#77821</a> (Potential heap corruption in TSendMail()).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.28"><!-- {{{ 7.1.28 -->
<h3>Version 7.1.28</h3>
<b><time class='releasedate' datetime='2019-04-04'>04 Apr 2019</time></b>
<ul><li>EXIF:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77753">#77753</a> (Heap-buffer-overflow in php_ifd_get32s). (CVE-2019-11034)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77831">#77831</a> (Heap-buffer-overflow in exif_iif_add_value). (CVE-2019-11035)</li>
</ul></li>
<li>SQLite3:
<ul>
  <li>Added sqlite3.defensive INI directive.</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.27"><!-- {{{ 7.1.27 -->
<h3>Version 7.1.27</h3>
<b><time class='releasedate' datetime='2019-03-07'>07 Mar 2019</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77630">#77630</a> (rename() across the device may allow unwanted access during processing). (CVE-2019-9637)</li>
</ul></li>
<li>EXIF:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77509">#77509</a> (Uninitialized read in exif_process_IFD_in_TIFF). (CVE-2019-9641)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77540">#77540</a> (Invalid Read on exif_process_SOFn). (CVE-2019-9640)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77563">#77563</a> (Uninitialized read in exif_process_IFD_in_MAKERNOTE). (CVE-2019-9638)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77659">#77659</a> (Uninitialized read in exif_process_IFD_in_MAKERNOTE). (CVE-2019-9639)</li>
</ul></li>
<li>PHAR:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77396">#77396</a> (Null Pointer Dereference in phar_create_or_parse_filename).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77586">#77586</a> (phar_tar_writeheaders_int() buffer overflow).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77431">#77431</a> (openFile() silently truncates after a null byte).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.26"><!-- {{{ 7.1.26 -->
<h3>Version 7.1.26</h3>
<b><time class='releasedate' datetime='2019-01-10'>10 Jan 2019</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77369">#77369</a> (memcpy with negative length via crafted DNS response). (CVE-2019-9022)</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77269">#77269</a> (efree() on uninitialized Heap data in imagescale leads to use-after-free). (CVE-2016-10166)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77270">#77270</a> (imagecolormatch Out Of Bounds Write on Heap). (CVE-2019-6977)</li>
</ul></li>
<li>IMAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77020">#77020</a> (null pointer dereference in imap_mail).</li>
</ul></li>
<li>Mbstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77370">#77370</a> (Buffer overflow on mb regex functions - fetch_token). (CVE-2019-9023)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77371">#77371</a> (heap buffer overflow in mb regex functions - compile_string_node). (CVE-2019-9023)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77381">#77381</a> (heap buffer overflow in multibyte match_at). (CVE-2019-9023)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77382">#77382</a> (heap buffer overflow due to incorrect length in expand_case_fold_string). (CVE-2019-9023)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77385">#77385</a> (buffer overflow in fetch_token). (CVE-2019-9023)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77394">#77394</a> (Buffer overflow in multibyte case folding - unicode). (CVE-2019-9023)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77418">#77418</a> (Heap overflow in utf32be_mbc_to_code). (CVE-2019-9023)</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77247">#77247</a> (heap buffer overflow in phar_detect_phar_fname_ext). (CVE-2019-9021)</li>
</ul></li>
<li>Xmlrpc:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77242">#77242</a> (heap out of bounds read in xmlrpc_decode()). (CVE-2019-9020)</li>
  <li>Fixed bug <a href="http://bugs.php.net/77380">#77380</a> (Global out of bounds read in xmlrpc base64 code). (CVE-2019-9024)</li>
</ul></li>
</ul>
<!-- }}} --></section>


<section class="version" id="7.1.25"><!-- {{{ 7.1.25 -->
<h3>Version 7.1.25</h3>
<b><time class='releasedate' datetime='2018-12-06'>06 Dec 2018</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71041">#71041</a> (zend_signal_startup() needs ZEND_API).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77231">#77231</a> (Segfault when using convert.quoted-printable-encode filter).</li>
</ul></li>
<li>ftp:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77151">#77151</a> (ftp_close(): SSL_read on shutdown).</li>
</ul></li>
<li>iconv:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77147">#77147</a> (Fixing 60494 ignored ICONV_MIME_DECODE_CONTINUE_ON_ERROR).</li>
</ul></li>
<li>IMAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77153">#77153</a> (imap_open allows to run arbitrary shell commands via mailbox parameter). (CVE-2018-19518)</li>
</ul></li>
<li>ODBC:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77079">#77079</a> (odbc_fetch_object has incorrect type signature).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77058">#77058</a> (Type inference in opcache causes side effects).</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77022">#77022</a> (PharData always creates new files with mode 0666).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77143">#77143</a> (Heap Buffer Overflow (READ: 4) in phar_parse_pharfile). (CVE-2018-20783)</li>
</ul></li>
<li>PGSQL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77047">#77047</a> (pg_convert has a broken regex for the 'TIME WITHOUT TIMEZONE' data type).</li>
</ul></li>
<li>SOAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76348">#76348</a> (WSDL_CACHE_MEMORY causes Segmentation fault).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77141">#77141</a> (Signedness issue in SOAP when precision=-1).</li>
</ul></li>
<li>Sockets:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/67619">#67619</a> (Validate length on socket_write).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.24"><!-- {{{ 7.1.24 -->
<h3>Version 7.1.24</h3>
<b><time class='releasedate' datetime='2018-11-08'>08 Nov 2018</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76946">#76946</a> (Cyclic reference in generator not detected).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77035">#77035</a> (The phpize and ./configure create redundant .deps file).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77041">#77041</a> (buildconf should output error messages to stderr) (Mizunashi Mana)</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75851">#75851</a> (Year component overflow with date formats "c", "o", "r" and "y").</li>
</ul></li>
<li>FCGI:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76948">#76948</a> (Failed shutdown/reboot or end session in Windows).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76954">#76954</a> (apache_response_headers removes last character from header name).</li>
</ul></li>
<li>FTP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76972">#76972</a> (Data truncation due to forceful ssl socket shutdown).</li>
</ul></li>
<li>intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76942">#76942</a> (U_ARGUMENT_TYPE_MISMATCH).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76965">#76965</a> (INI_SCANNER_RAW doesn't strip trailing whitespace).</li>
</ul></li>
<li>Tidy:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77027">#77027</a> (tidy::getOptDoc() not available on Windows).</li>
</ul></li>
<li>XML:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/30875">#30875</a> (xml_parse_into_struct() does not resolve entities).</li>
  <li>Add support for getting SKIP_TAGSTART and SKIP_WHITE options.</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.23"><!-- {{{ 7.1.23 -->
<h3>Version 7.1.23</h3>
<b><time class='releasedate' datetime='2018-10-11'>11 Oct 2018</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76901">#76901</a> (method_exists on SPL iterator passthrough method corrupts memory).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76846">#76846</a> (Segfault in shutdown function after memory limit error).</li>
</ul></li>
<li>CURL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76480">#76480</a> (Use curl_multi_wait() so that timeouts are respected).</li>
</ul></li>
<li>iconv:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/66828">#66828</a> (iconv_mime_encode Q-encoding longer than it should be).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76832">#76832</a> (ZendOPcache.MemoryBase periodically deleted by the OS).</li>
</ul></li>
<li>POSIX:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75696">#75696</a> (posix_getgrnam fails to print details of group).</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74454">#74454</a> (Wrong exception being thrown when using ReflectionMethod).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73457">#73457</a> (Wrong error message when fopen FTP wrapped fails to open data connection).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74764">#74764</a> (Bindto IPv6 works with file_get_contents but fails with stream_socket_client).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75533">#75533</a> (array_reduce is slow when $carry is large array).</li>
</ul></li>
<li>Zlib:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75273">#75273</a> (php_zlib_inflate_filter() may not update bytes_consumed).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.22"><!-- {{{ 7.1.22 -->
<h3>Version 7.1.22</h3>
<b><time class='releasedate' datetime='2018-09-13'>13 Sep 2018</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76754">#76754</a> (parent private constant in extends class memory leak).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72443">#72443</a> (Generate enabled extension).</li>
</ul></li>
<li>Apache2:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76582">#76582</a> (XSS due to the header Transfer-Encoding: chunked). (CVE-2018-17082)</li>
</ul></li>
<li>Bz2:
<ul>
  <li>Fixed arginfo for bzcompress.</li>
</ul></li>
<li>gettext:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76517">#76517</a> (incorrect restoring of LDFLAGS).</li>
</ul></li>
<li>iconv:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/68180">#68180</a> (iconv_mime_decode can return extra characters in a header).</li>
  <li>Fixed bug <a href="http://bugs.php.net/63839">#63839</a> (iconv_mime_decode_headers function is skipping headers).</li>
  <li>Fixed bug <a href="http://bugs.php.net/60494">#60494</a> (iconv_mime_decode does ignore special characters).</li>
  <li>Fixed bug <a href="http://bugs.php.net/55146">#55146</a> (iconv_mime_decode_headers() skips some headers).</li>
</ul></li>
<li>intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74484">#74484</a> (MessageFormatter::formatMessage memory corruption with 11+ named placeholders).</li>
</ul></li>
<li>libxml:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76777">#76777</a> ("public id" parameter of libxml_set_external_entity_loader callback undefined).</li>
</ul></li>
<li>mbstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76704">#76704</a> (mb_detect_order return value varies based on argument type).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76747">#76747</a> (Opcache treats path containing "test.pharma.tld" as a phar file).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76705">#76705</a> (unusable ssl =&gt; peer_fingerprint in stream_context_create()).</li>
</ul></li>
<li>phpdbg:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76595">#76595</a> (phpdbg man page contains outdated information).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/68825">#68825</a> (Exception in DirectoryIterator::getLinkTarget()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/68175">#68175</a> (RegexIterator pregFlags are NULL instead of 0).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76778">#76778</a> (array_reduce leaks memory if callback throws exception).</li>
</ul></li>
<li>zlib:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/65988">#65988</a> (Zlib version check fails when an include/zlib/ style dir is passed to the --with-zlib configure option).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76709">#76709</a> (Minimal required zlib library is 1.2.0.4).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.21"><!-- {{{ 7.1.21 -->
<h3>Version 7.1.21</h3>
<b><time class='releasedate' datetime='2018-08-16'>16 Aug 2018</time></b>
<ul><li>Calendar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/52974">#52974</a> (jewish.c: compile error under Windows with GBK charset).</li>
</ul></li>
<li>Filter:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76366">#76366</a> (References in sub-array for filtering breaks the filter).</li>
</ul></li>
<li>PDO_Firebird:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76488">#76488</a> (Memory leak when fetching a BLOB field).</li>
</ul></li>
<li>PDO_PgSQL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75402">#75402</a> (Possible Memory Leak using PDO::CURSOR_SCROLL option).</li>
</ul></li>
<li>SQLite3:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76665">#76665</a> (SQLite3Stmt::bindValue() with SQLITE3_FLOAT doesn't juggle).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/68553">#68553</a> (array_column: null values in $index_key become incrementing keys in result).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73817">#73817</a> (Incorrect entries in get_html_translation_table).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76643">#76643</a> (Segmentation fault when using `output_add_rewrite_var`).</li>
</ul></li>
<li>Zip:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76524">#76524</a> (ZipArchive memory leak (OVERWRITE flag and empty archive)).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.20"><!-- {{{ 7.1.20 -->
<h3>Version 7.1.20</h3>
<b><time class='releasedate' datetime='2018-07-19'>19 Jul 2018</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76534">#76534</a> (PHP hangs on 'illegal string offset on string references with an error handler).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76502">#76502</a> (Chain of mixed exceptions and errors does not serialize properly).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76462">#76462</a> (Undefined property: DateInterval::$f).</li>
</ul></li>
<li>exif:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76423">#76423</a> (Int Overflow lead to Heap OverFlow in exif_thumbnail_extract of exif.c). (CVE-2018-14883)</li>
  <li>Fixed bug <a href="http://bugs.php.net/76557">#76557</a> (heap-buffer-overflow (READ of size 48) while reading exif data). (CVE-2018-14851)</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73342">#73342</a> (Vulnerability in php-fpm by changing stdin to non-blocking).</li>
</ul></li>
<li>GMP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74670">#74670</a> (Integer Underflow when unserializing GMP and possible other classes).</li>
</ul></li>
<li>intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76556">#76556</a> (get_debug_info handler for BreakIterator shows wrong type).</li>
</ul></li>
<li>mbstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76532">#76532</a> (Integer overflow and excessive memory usage in mb_strimwidth).</li>
</ul></li>
<li>PGSQL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76548">#76548</a> (pg_fetch_result did not fetch the next row).</li>
</ul></li>
<li>phpdbg:
<ul>
  <li>Fix arginfo wrt. optional/required parameters.</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76536">#76536</a> (PHP crashes with core dump when throwing exception in error handler).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75231">#75231</a> (ReflectionProperty#getValue() incorrectly works with inherited classes).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76505">#76505</a> (array_merge_recursive() is duplicating sub-array keys).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71848">#71848</a> (getimagesize with $imageinfo returns false).</li>
</ul></li>
<li>Win32:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76459">#76459</a> (windows linkinfo lacks openbasedir check). (CVE-2018-15132)</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.19"><!-- {{{ 7.1.19 -->
<h3>Version 7.1.19</h3>
<b><time class='releasedate' datetime='2018-06-22'>22 Jun 2018</time></b>
<ul><li>CLI Server:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76333">#76333</a> (PHP built-in server does not find files if root path contains special characters).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76296">#76296</a> (openssl_pkey_get_public does not respect open_basedir).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76174">#76174</a> (openssl extension fails to build with LibreSSL 2.7).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76367">#76367</a> (NoRewindIterator segfault 11).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76335">#76335</a> ("link(): Bad file descriptor" with non-ASCII path).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76383">#76383</a> (array_map on $GLOBALS returns IS_INDIRECT).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.18"><!-- {{{ 7.1.18 -->
<h3>Version 7.1.18</h3>
<b><time class='releasedate' datetime='2018-05-24'>24 May 2018</time></b>
<ul><li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76075">#76075</a> --with-fpm-acl wrongly tries to find libacl on FreeBSD.</li>
</ul></li>
<li>intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74385">#74385</a> (Locale::parseLocale() broken with some arguments).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76205">#76205</a> (PHP-FPM sporadic crash when running Infinitewp).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76275">#76275</a> (Assertion failure in file cache when unserializing empty try_catch_array).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76281">#76281</a> (Opcache causes incorrect "undefined variable" errors).</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed arginfo for array_replace(_recursive) and array_merge(_recursive).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.17"><!-- {{{ 7.1.17 -->
<h3>Version 7.1.17</h3>
<b><time class='releasedate' datetime='2018-04-26'>26 Apr 2018</time></b>
<ul><li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76131">#76131</a> (mismatch arginfo for date_create).</li>
</ul></li>
<li>Exif:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76130">#76130</a> (Heap Buffer Overflow (READ: 1786) in exif_iif_add_value). (CVE-2018-10549)</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/68440">#68440</a> (ERROR: failed to reload: execvp() failed: Argument list too long).</li>
  <li>Fixed incorrect write to getenv result in FPM reload.</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/52070">#52070</a> (imagedashedline() - dashed line sometimes is not visible).</li>
</ul></li>
<li>iconv:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76249">#76249</a> (stream filter convert.iconv leads to infinite loop on invalid sequence). (CVE-2018-10546)</li>
</ul></li>
<li>intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76153">#76153</a> (Intl compilation fails with icu4c 61.1).</li>
</ul></li>
<li>ldap:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76248">#76248</a> (Malicious LDAP-Server Response causes Crash). (CVE-2018-10548)</li>
</ul></li>
<li>mbstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75944">#75944</a> (Wrong cp1251 detection).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76113">#76113</a> (mbstring does not build with Oniguruma 6.8.1).</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76129">#76129</a> (fix for CVE-2018-5712 may not be complete). (CVE-2018-10547)</li>
</ul></li>
<li>phpdbg:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76143">#76143</a> (Memory corruption: arbitrary NUL overwrite).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76131">#76131</a> (mismatch arginfo for splarray constructor).</li>
</ul></li>
<li>standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75996">#75996</a> (incorrect url in header for mt_rand).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.16"><!-- {{{ 7.1.16 -->
<h3>Version 7.1.16</h3>
<b><time class='releasedate' datetime='2018-03-29'>29 Mar 2018</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76025">#76025</a> (Segfault while throwing exception in error_handler).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76044">#76044</a> ('date: illegal option -- -' in ./configure on FreeBSD).</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75605">#75605</a> (Dumpable FPM child processes allow bypassing opcache access controls). (CVE-2018-10545)</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73957">#73957</a> (signed integer conversion in imagescale()).</li>
</ul></li>
<li>ODBC:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76088">#76088</a> (ODBC functions are not available by default on Windows).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76074">#76074</a> (opcache corrupts variable in for-loop).</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76085">#76085</a> (Segmentation fault in buildFromIterator when directory name contains a \n).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74139">#74139</a> (mail.add_x_header default inconsistent with docs).</li>
  <li>Fixed bug <a href="http://bugs.php.net/76068">#76068</a> (parse_ini_string fails to parse "[foo]\nbar=1|&gt;baz" with segfault).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.15"><!-- {{{ 7.1.15 -->
<h3>Version 7.1.15</h3>
<b><time class='releasedate' datetime='2018-03-01'>01 Mar 2018</time></b>
<ul><li>Apache2Handler:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75882">#75882</a> (a simple way for segfaults in threadsafe php just with configuration).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75857">#75857</a> (Timezone gets truncated when formatted).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75928">#75928</a> (Argument 2 for `DateTimeZone::listIdentifiers()` should accept `null`).</li>
  <li>Fixed bug <a href="http://bugs.php.net/68406">#68406</a> (calling var_dump on a DateTimeZone object modifies it).</li>
</ul></li>
<li>PGSQL:
<ul>
  <li>Fixed #75838 (Memory leak in pg_escape_bytea()).</li>
</ul></li>
<li>ODBC:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73725">#73725</a> (Unable to retrieve value of varchar(max) type).</li>
</ul></li>
<li>LDAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/49876">#49876</a> (Fix LDAP path lookup on 64-bit distros).</li>
</ul></li>
<li>libxml2:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75871">#75871</a> (use pkg-config where available).</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/65414">#65414</a> (deal with leading slash when adding files correctly).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74519">#74519</a> (strange behavior of AppendIterator).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75916">#75916</a> (DNS_CAA record results contain garbage).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75981">#75981</a> (stack-buffer-overflow while parsing HTTP response). (CVE-2018-7584)</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.14"><!-- {{{ 7.1.14 -->
<h3>Version 7.1.14</h3>
<b><time class='releasedate' datetime='2018-02-01'>01 Feb 2018</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75679">#75679</a> (Path 260 character problem).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75786">#75786</a> (segfault when using spread operator on generator passed by reference).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75799">#75799</a> (arg of get_defined_functions is optional).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75396">#75396</a> (Exit inside generator finally results in fatal error).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75079">#75079</a> (self keyword leads to incorrectly generated TypeError when in closure in trait).</li>
</ul></li>
<li>FCGI:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75794">#75794</a> (getenv() crashes on Windows 7.2.1 when second parameter is false).</li>
</ul></li>
<li>IMAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75774">#75774</a> (imap_append HeapCorruction).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75720">#75720</a> (File cache not populated after SHM runs full).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75579">#75579</a> (Interned strings buffer overflow may cause crash).</li>
</ul></li>
<li>PGSQL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75671">#75671</a> (pg_version() crashes when called on a connection to cockroach).</li>
</ul></li>
<li>Readline:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75775">#75775</a> (readline_read_history segfaults with empty file).</li>
</ul></li>
<li>SAPI:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75735">#75735</a> ([embed SAPI] Segmentation fault in sapi_register_post_entry).</li>
</ul></li>
<li>SOAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70469">#70469</a> (SoapClient generates E_ERROR even if exceptions=1 is used).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75502">#75502</a> (Segmentation fault in zend_string_release).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75717">#75717</a> (RecursiveArrayIterator does not traverse arrays by reference).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75242">#75242</a> (RecursiveArrayIterator doesn't have constants from parent class).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73209">#73209</a> (RecursiveArrayIterator does not iterate object properties).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75781">#75781</a> (substr_count incorrect result).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.13"><!-- {{{ 7.1.13 -->
<h3>Version 7.1.13</h3>
<b><time class='releasedate' datetime='2018-01-04'>04 Jan 2018</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75573">#75573</a> (Segmentation fault in 7.1.12 and 7.0.26).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75384">#75384</a> (PHP seems incompatible with OneDrive files on demand).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74862">#74862</a> (Unable to clone instance when private __clone defined).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75074">#75074</a> (php-process crash when is_file() is used with strings longer 260 chars).</li>
</ul></li>
<li>CLI Server:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/60471">#60471</a> (Random "Invalid request (unexpected EOF)" using a router script).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73830">#73830</a> (Directory does not exist).</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/64938">#64938</a> (libxml_disable_entity_loader setting is shared between requests).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75571">#75571</a> (Potential infinite loop in gdImageCreateFromGifCtx). (CVE-2018-5711)</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75608">#75608</a> ("Narrowing occurred during type inference" error).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75579">#75579</a> (Interned strings buffer overflow may cause crash).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75570">#75570</a> ("Narrowing occurred during type inference" error).</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74183">#74183</a> (preg_last_error not returning error code after error).</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74782">#74782</a> (Reflected XSS in .phar 404 page). (CVE-2018-5712)</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75511">#75511</a> (fread not free unused buffer).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75514">#75514</a> (mt_rand returns value outside [$min,$max]+ on 32-bit) (Remi)</li>
  <li>Fixed bug <a href="http://bugs.php.net/75535">#75535</a> (Inappropriately parsing HTTP response leads to PHP segment fault). (CVE-2018-14884)</li>
  <li>Fixed bug <a href="http://bugs.php.net/75409">#75409</a> (accept EFAULT in addition to ENOSYS as indicator that getrandom() is missing).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73124">#73124</a> (php_ini_scanned_files() not reporting correctly).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75574">#75574</a> (putenv does not work properly if parameter contains non-ASCII unicode character).</li>
</ul></li>
<li>Zip:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75540">#75540</a> (Segfault with libzip 1.3.1).</li>
</ul></li>
</ul>
<!-- }}} --></section>


<section class="version" id="7.1.12"><!-- {{{ 7.1.12 -->
<h3>Version 7.1.12</h3>
<b><time class='releasedate' datetime='2017-11-23'>23 Nov 2017</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75420">#75420</a> (Crash when modifing property name in __isset for BP_VAR_IS).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75368">#75368</a> (mmap/munmap trashing on unlucky allocations).</li>
</ul></li>
<li>CLI:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75287">#75287</a> (Builtin webserver crash after chdir in a shutdown function).</li>
</ul></li>
<li>Enchant:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/53070">#53070</a> (enchant_broker_get_path crashes if no path is set).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75365">#75365</a> (Enchant still reports version 1.1.0).</li>
</ul></li>
<li>Exif:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75301">#75301</a> (Exif extension has built in revision version).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/65148">#65148</a> (imagerotate may alter image dimensions).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75437">#75437</a> (Wrong reflection on imagewebp).</li>
</ul></li>
<li>intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75317">#75317</a> (UConverter::setDestinationEncoding changes source instead of destination).</li>
</ul></li>
<li>interbase:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75453">#75453</a> (Incorrect reflection for ibase_[p]connect).</li>
</ul></li>
<li>Mysqli:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75434">#75434</a> (Wrong reflection for mysqli_fetch_all function).</li>
</ul></li>
<li>OCI8:
<ul>
  <li>Fixed valgrind issue.</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75363">#75363</a> (openssl_x509_parse leaks memory).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75307">#75307</a> (Wrong reflection for openssl_open function).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75373">#75373</a> (Warning Internal error: wrong size calculation).</li>
</ul></li>
<li>PGSQL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75419">#75419</a> (Default link incorrectly cleared/linked by pg_close()).</li>
</ul></li>
<li>SOAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75464">#75464</a> (Wrong reflection on SoapClient::__setSoapHeaders).</li>
</ul></li>
<li>Zlib:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75299">#75299</a> (Wrong reflection on inflate_init and inflate_add).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.11"><!-- {{{ 7.1.11 -->
<h3>Version 7.1.11</h3>
<b><time class='releasedate' datetime='2017-10-26'>26 Oct 2017</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75241">#75241</a> (Null pointer dereference in zend_mm_alloc_small()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75236">#75236</a> (infinite loop when printing an error-message).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75252">#75252</a> (Incorrect token formatting on two parse errors in one request).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75220">#75220</a> (Segfault when calling is_callable on parent).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75290">#75290</a> (debug info of Closures of internal functions contain garbage argument names).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75055">#75055</a> (Out-Of-Bounds Read in timelib_meridian()). (CVE-2017-16642)</li>
</ul></li>
<li>Apache2Handler:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75311">#75311</a> (error: 'zend_hash_key' has no member named 'arKey' in apache2handler).</li>
</ul></li>
<li>Hash:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75303">#75303</a> (sha3 hangs on bigendian).</li>
</ul></li>
<li>Intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75318">#75318</a> (The parameter of UConverter::getAliases() is not optional).</li>
</ul></li>
<li>litespeed:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75248">#75248</a> (Binary directory doesn't get created when building only litespeed SAPI).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75251">#75251</a> (Missing program prefix and suffix).</li>
</ul></li>
<li>mcrypt:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72535">#72535</a> (arcfour encryption stream filter crashes php).</li>
</ul></li>
<li>MySQLi:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75018">#75018</a> (Data corruption when reading fields of bit type).</li>
</ul></li>
<li>OCI8:
<ul>
  <li>Fixed incorrect reference counting.</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75255">#75255</a> (Request hangs and not finish).</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75207">#75207</a> (applied upstream patch for CVE-2016-1283).</li>
</ul></li>
<li>PDO_mysql:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75177">#75177</a> (Type 'bit' is fetched as unexpected string).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73629">#73629</a> (SplDoublyLinkedList::setIteratorMode masks intern flags).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.10"><!-- {{{ 7.1.10 -->
<h3>Version 7.1.10</h3>
<b><time class='releasedate' datetime='2017-09-28'>28 Sep 2017</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75042">#75042</a> (run-tests.php issues with EXTENSION block).</li>
</ul></li>
<li>BCMath:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/44995">#44995</a> (bcpowmod() fails if scale != 0).</li>
  <li>Fixed bug <a href="http://bugs.php.net/46781">#46781</a> (BC math handles minus zero incorrectly).</li>
  <li>Fixed bug <a href="http://bugs.php.net/54598">#54598</a> (bcpowmod() may return 1 if modulus is 1).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75178">#75178</a> (bcpowmod() misbehaves for non-integer base or modulus).</li>
</ul></li>
<li>CLI server:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70470">#70470</a> (Built-in server truncates headers spanning over TCP packets).</li>
</ul></li>
<li>CURL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75093">#75093</a> (OpenSSL support not detected).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75124">#75124</a> (gdImageGrayScale() may produce colors).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75139">#75139</a> (libgd/gd_interpolation.c:1786: suspicious if ?).</li>
</ul></li>
<li>Gettext:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73730">#73730</a> (textdomain(null) throws in strict mode).</li>
</ul></li>
<li>Intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75090">#75090</a> (IntlGregorianCalendar doesn't have constants from parent class).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75193">#75193</a> (segfault in collator_convert_object_to_string).</li>
</ul></li>
<li>PDO_OCI:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74631">#74631</a> (PDO_PCO with PHP-FPM: OCI environment initialized before PHP-FPM sets it up).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75155">#75155</a> (AppendIterator::append() is broken when appending another AppendIterator).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75173">#75173</a> (incorrect behavior of AppendIterator::append in foreach loop).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75152">#75152</a> (signed integer overflow in parse_iv).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75097">#75097</a> (gethostname fails if your host name is 64 chars long).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.9"><!-- {{{ 7.1.9 -->
<h3>Version 7.1.9</h3>
<b><time class='releasedate' datetime='2017-08-31'>31 Aug 2017</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74947">#74947</a> (Segfault in scanner on INF number).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74954">#74954</a> (null deref and segfault in zend_generator_resume()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74725">#74725</a> (html_errors=1 breaks unhandled exceptions).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75063">#75063</a> (Main CWD initialized with wrong codepage).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75349">#75349</a> (NAN comparison).</li>
</ul></li>
<li>cURL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74125">#74125</a> (Fixed finding CURL on systems with multiarch support).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug #75002 (Null Pointer Dereference in timelib_time_clone).</li>
</ul></li>
<li>Intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74993">#74993</a> (Wrong reflection on some locale_* functions).</li>
</ul></li>
<li>Mbstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71606">#71606</a> (Segmentation fault mb_strcut with HTML-ENTITIES encoding).</li>
  <li>Fixed bug <a href="http://bugs.php.net/62934">#62934</a> (mb_convert_kana() does not convert iteration marks).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75001">#75001</a> (Wrong reflection on mb_eregi_replace).</li>
</ul></li>
<li>MySQLi:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74968">#74968</a> (PHP crashes when calling mysqli_result::fetch_object with an abstract class).</li>
</ul></li>
<li>OCI8:
<ul>
  <li>Expose oci_unregister_taf_callback() (Tianfang Yang)</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74980">#74980</a> (Narrowing occurred during type inference).</li>
</ul></li>
<li>phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74991">#74991</a> (include_path has a 4096 char limit in some cases).</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74949">#74949</a> (null pointer dereference in _function_string).</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74892">#74892</a> (Url Rewriting (trans_sid) not working on urls that start with "#").</li>
  <li>Fixed bug <a href="http://bugs.php.net/74833">#74833</a> (SID constant created with wrong module number).</li>
</ul></li>
<li>SimpleXML:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74950">#74950</a> (nullpointer deref in simplexml_element_getDocNamespaces).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75049">#75049</a> (spl_autoload_unregister can't handle spl_autoload_functions results).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74669">#74669</a> (Unserialize ArrayIterator broken).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74977">#74977</a> (Appending AppendIterator leads to segfault).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75015">#75015</a> (Crash in recursive iterator destructors).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75075">#75075</a> (unpack with X* causes infinity loop).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74103">#74103</a> (heap-use-after-free when unserializing invalid array size). (CVE-2017-12932)</li>
  <li>Fixed bug <a href="http://bugs.php.net/75054">#75054</a> (A Denial of Service Vulnerability was found when performing deserialization).</li>
</ul></li>
<li>WDDX:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73793">#73793</a> (WDDX uses wrong decimal seperator).</li>
</ul></li>
<li>XMLRPC:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74975">#74975</a> (Incorrect xmlrpc serialization for classes with declared properties).</li>
</ul></li>
</ul>
<!-- }}} --></section>


<section class="version" id="7.1.8"><!-- {{{ 7.1.8 -->
<h3>Version 7.1.8</h3>
<b><time class='releasedate' datetime='2017-08-03'>03 Aug 2017</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74832">#74832</a> (Loading PHP extension with already registered function name leads to a crash).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74780">#74780</a> (parse_url() broken when query string contains colon).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74761">#74761</a> (Unary operator expected error on some systems).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73900">#73900</a> (Use After Free in unserialize() SplFixedArray).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74923">#74923</a> (Crash when crawling through network share).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74913">#74913</a> (fixed incorrect poll.h include).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74906">#74906</a> (fixed incorrect errno.h include).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74852">#74852</a> (property_exists returns true on unknown DateInterval property).</li>
</ul></li>
<li>OCI8:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74625">#74625</a> (Integer overflow in oci_bind_array_by_name).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74623">#74623</a> (Infinite loop in type inference when using HTMLPurifier).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74798">#74798</a> (pkcs7_en/decrypt does not work if \x0a is used in content).</li>
  <li>Added OPENSSL_DONT_ZERO_PAD_KEY constant to prevent key padding and fix bug #71917 (openssl_open() returns junk on envelope &lt; 16 bytes) and bug #72362 (OpenSSL Blowfish encryption is incorrect for short keys).</li>
</ul></li>
<li>PDO:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69356">#69356</a> (PDOStatement::debugDumpParams() truncates query).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73471">#73471</a> (PHP freezes with AppendIterator).</li>
</ul></li>
<li>SQLite3:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74883">#74883</a> (SQLite3::__construct() produces "out of memory" exception with invalid flags).</li>
</ul></li>
<li>Wddx:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73173">#73173</a> (huge memleak when wddx_unserialize).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74145">#74145</a> (wddx parsing empty boolean tag leads to SIGSEGV). (CVE-2017-11143)</li>
</ul></li>
<li>zlib:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73944">#73944</a> (dictionary option of inflate_init() does not work).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.7"><!-- {{{ 7.1.7 -->
<h3>Version 7.1.7</h3>
<b><time class='releasedate' datetime='2017-07-06'>06 Jul 2017</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74738">#74738</a> (Multiple [PATH=] and [HOST=] sections not properly parsed).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74658">#74658</a> (Undefined constants in array properties result in broken properties).</li>
  <li>Fixed misparsing of abstract unix domain socket names.</li>
  <li>Fixed bug <a href="http://bugs.php.net/74603">#74603</a> (PHP INI Parsing Stack Buffer Overflow Vulnerability). (CVE-2017-11628)</li>
  <li>Fixed bug <a href="http://bugs.php.net/74101">#74101</a> (Unserialize Heap Use-After-Free (READ: 1) in zval_get_type). (CVE-2017-12934)</li>
  <li>Fixed bug <a href="http://bugs.php.net/74111">#74111</a> (Heap buffer overread (READ: 1) finish_nested_data from unserialize). (CVE-2017-12933)</li>
  <li>Fixed bug <a href="http://bugs.php.net/74819">#74819</a> (wddx_deserialize() heap out-of-bound read via php_parse_date()). (CVE-2017-11145)</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74639">#74639</a> (implement clone for DatePeriod and DateInterval).</li>
</ul></li>
<li>DOM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69373">#69373</a> (References to deleted XPath query results).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74435">#74435</a> (Buffer over-read into uninitialized memory). (CVE-2017-7890)</li>
</ul></li>
<li>Intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73473">#73473</a> (Stack Buffer Overflow in msgfmt_parse_message). (CVE-2017-11362)</li>
  <li>Fixed bug <a href="http://bugs.php.net/74705">#74705</a> (Wrong reflection on Collator::getSortKey and collator_get_sort_key).</li>
</ul></li>
<li>Mbstring:
<ul>
  <li>Add oniguruma upstream fix (CVE-2017-9224, CVE-2017-9226, CVE-2017-9227, CVE-2017-9228, CVE-2017-9229)</li>
</ul></li>
<li>OCI8:
<ul>
  <li>Add TAF callback (PR #2459).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74663">#74663</a> (Segfault with opcache.memory_protect and validate_timestamp).</li>
  <li>Revert opcache.enable_cli to default disabled.</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74720">#74720</a> (pkcs7_en/decrypt does not work if \x1a is used in content).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74651">#74651</a> (negative-size-param (-1) in memcpy in zif_openssl_seal()). (CVE-2017-11144)</li>
</ul></li>
<li>PDO_OCI:
<ul>
  <li>Support Instant Client 12.2 in --with-pdo-oci configure option.</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74673">#74673</a> (Segfault when cast Reflection object to string with undefined constant).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74478">#74478</a> (null coalescing operator failing with SplFixedArray).</li>
</ul></li>
<li>FTP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74598">#74598</a> (ftp:// wrapper ignores context arg).</li>
</ul></li>
<li>PHAR:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74386">#74386</a> (Phar::__construct reflection incorrect).</li>
</ul></li>
<li>SOAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74679">#74679</a> (Incorrect conversion array with WSDL_CACHE_MEMORY).</li>
</ul></li>
<li>Streams:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74556">#74556</a> (stream_socket_get_name() returns '\0').</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.6"><!-- {{{ 7.1.6 -->
<h3>Version 7.1.6</h3>
<b><time class='releasedate' datetime='2017-06-07'>07 Jun 2017</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74600">#74600</a> (crash (SIGSEGV) in _zend_hash_add_or_update_i).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74546">#74546</a> (SIGILL in ZEND_FETCH_CLASS_CONSTANT_SPEC_CONST_CONST).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74589">#74589</a> (__DIR__ wrong for unicode character).</li>
</ul></li>
<li>intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74468">#74468</a> (wrong reflection on Collator::sortWithSortKeys).</li>
</ul></li>
<li>MySQLi:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74547">#74547</a> (mysqli::change_user() doesn't accept null as $database argument w/strict_types).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74596">#74596</a> (SIGSEGV with opcache.revalidate_path enabled).</li>
</ul></li>
<li>phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/51918">#51918</a> (Phar::webPhar() does not handle requests sent through PUT and DELETE method).</li>
</ul></li>
<li>Readline:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74490">#74490</a> (readline() moves the cursor to the beginning of the line).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74510">#74510</a> (win32/sendmail.c anchors CC header but not BCC).</li>
</ul></li>
<li>xmlreader:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74457">#74457</a> (Wrong reflection on XMLReader::expand).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.5"><!-- {{{ 7.1.5 -->
<h3>Version 7.1.5</h3>
<b><time class='releasedate' datetime='2017-05-11'>11 May 2017</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74408">#74408</a> (Endless loop bypassing execution time limit).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74353">#74353</a> (Segfault when killing within bash script trap code).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74340">#74340</a> (Magic function __get has different behavior in php 7.1.x).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74188">#74188</a> (Null coalescing operator fails for undeclared static class properties).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74444">#74444</a> (multiple catch freezes in some cases).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74410">#74410</a> (stream_select() is broken on Windows Nanoserver).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74337">#74337</a> (php-cgi.exe crash on facebook callback).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74404">#74404</a> (Wrong reflection on DateTimeZone::getTransitions).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74080">#74080</a> (add constant for RFC7231 format datetime).</li>
</ul></li>
<li>DOM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74416">#74416</a> (Wrong reflection on DOMNode::cloneNode).</li>
</ul></li>
<li>Fileinfo:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74379">#74379</a> (syntax error compile error in libmagic/apprentice.c).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74343">#74343</a> (compile fails on solaris 11 with system gd2 library).</li>
</ul></li>
<li>MySQLnd:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74376">#74376</a> (Invalid free of persistent results on error/connection loss).</li>
</ul></li>
<li>Intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/65683">#65683</a> (Intl does not support DateTimeImmutable).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74298">#74298</a> (IntlDateFormatter-&gt;format() doesn't return microseconds/fractions).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74433">#74433</a> (wrong reflection for Normalizer methods).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74439">#74439</a> (wrong reflection for Locale methods).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74456">#74456</a> (Segmentation error while running a script in CLI mode).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74431">#74431</a> (foreach infinite loop).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74442">#74442</a> (Opcached version produces a nested array).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73833">#73833</a> (null character not allowed in openssl_pkey_get_private).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73711">#73711</a> (Segfault in openssl_pkey_new when generating DSA or DH key).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74341">#74341</a> (openssl_x509_parse fails to parse ASN.1 UTCTime without seconds).</li>
</ul></li>
<li>phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74383">#74383</a> (phar method parameters reflection correction).</li>
</ul></li>
<li>Readline:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74489">#74489</a> (readline() immediately returns false in interactive console mode).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72071">#72071</a> (setcookie allows max-age to be negative).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74361">#74361</a> (Compaction in array_rand() violates COW).</li>
</ul></li>
<li>Streams:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74429">#74429</a> (Remote socket URI with unique persistence identifier broken).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.4"><!-- {{{ 7.1.4 -->
<h3>Version 7.1.4</h3>
<b><time class='releasedate' datetime='2017-04-13'>13 Apr 2017</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74149">#74149</a> (static embed SAPI linkage error).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73370">#73370</a> (falsely exits with "Out of Memory" when using USE_ZEND_ALLOC=0).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73960">#73960</a> (Leak with instance method calling static method with referenced return).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69676">#69676</a> (Resolution of self::FOO in class constants not correct).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74265">#74265</a> (Build problems after 7.0.17 release: undefined reference to `isfinite').</li>
  <li>Fixed bug <a href="http://bugs.php.net/74302">#74302</a> (yield fromLABEL is over-greedy).</li>
</ul></li>
<li>Apache:
<ul>
  <li>Reverted patch for bug #61471, fixes bug #74318.</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72096">#72096</a> (Swatch time value incorrect for dates before 1970).</li>
</ul></li>
<li>DOM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74004">#74004</a> (LIBXML_NOWARNING flag ingnored on loadHTML*).</li>
</ul></li>
<li>iconv:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74230">#74230</a> (iconv fails to fail on surrogates).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74250">#74250</a> (OPcache compilation performance regression in PHP 5.6/7 with huge classes).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72333">#72333</a> (fwrite() on non-blocking SSL sockets doesn't work).</li>
</ul></li>
<li>PDO MySQL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71003">#71003</a> (Expose MYSQLI_CLIENT_SSL_DONT_VERIFY_SERVER_CERT to PDO interface).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74058">#74058</a> (ArrayObject can not notice changes).</li>
</ul></li>
<li>SQLite:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74217">#74217</a> (Allow creation of deterministic sqlite functions).</li>
</ul></li>
<li>Streams:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74216">#74216</a> (Correctly fail on invalid IP address ports).</li>
</ul></li>
<li>zlib:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74240">#74240</a> (deflate_add can allocate too much memory).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.3"><!-- {{{ 7.1.3 -->
<h3>Version 7.1.3</h3>
<b><time class='releasedate' datetime='2017-03-16'>16 Mar 2017</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74157">#74157</a> (Segfault with nested generators).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74164">#74164</a> (PHP hangs when an invalid value is dynamically passed to typehinted by-ref arg).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74093">#74093</a> (Maximum execution time of n+2 seconds exceed not written in error_log).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73989">#73989</a> (PHP 7.1 Segfaults within Symfony test suite).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74084">#74084</a> (Out of bound read - zend_mm_alloc_small).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73807">#73807</a> (Performance problem with processing large post request). (CVE-2017-11142)</li>
  <li>Fixed bug <a href="http://bugs.php.net/73998">#73998</a> (array_key_exists fails on arrays created by get_object_vars).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73954">#73954</a> (NAN check fails on Alpine Linux with musl).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73677">#73677</a> (Generating phar.phar core dump with gcc ASAN enabled build).</li>
</ul></li>
<li>Apache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/61471">#61471</a> (Incomplete POST does not timeout but is passed to PHP).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73837">#73837</a> ("new DateTime()" sometimes returns 1 second ago value).</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69860">#69860</a> (php-fpm process accounting is broken with keepalive).</li>
</ul></li>
<li>Hash:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73127">#73127</a> (gost-crypto hash incorrect if input data contains long 0xFF sequence).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74031">#74031</a> (ReflectionFunction for imagepng is missing last two parameters).</li>
</ul></li>
<li>Mysqlnd:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74021">#74021</a> (fetch_array broken data. Data more then MEDIUMBLOB).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74019">#74019</a> (Segfault with list).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74022">#74022</a> (PHP Fast CGI crashes when reading from a pfx file).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74099">#74099</a> (Memory leak with openssl_encrypt()).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74005">#74005</a> (mail.add_x_header causes RFC-breaking lone line feed).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74041">#74041</a> (substr_count with length=0 broken).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73118">#73118</a> (is_callable callable name reports misleading value for anonymous classes).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74105">#74105</a> (PHP on Linux should use /dev/urandom when getrandom is not available).</li>
</ul></li>
<li>Streams:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73496">#73496</a> (Invalid memory access in zend_inline_hash_func).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74090">#74090</a> (stream_get_contents maxlength&gt;-1 returns empty string).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.2"><!-- {{{ 7.1.2 -->
<h3>Version 7.1.2</h3>
<b><time class='releasedate' datetime='2017-02-16'>16 Feb 2017</time></b>
<ul><li>Core:
<ul>
  <li>Improved GENERATOR_CREATE opcode handler.</li>
  <li>Fixed bug <a href="http://bugs.php.net/73877">#73877</a> (readlink() returns garbage for UTF-8 paths).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73876">#73876</a> (Crash when exporting **= in expansion of assign op).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73962">#73962</a> (bug with symlink related to cyrillic directory).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73969">#73969</a> (segfault in debug_print_backtrace).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73994">#73994</a> (arginfo incorrect for unpack).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73973">#73973</a> (assertion error in debug_zval_dump).</li>
</ul></li>
<li>DOM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/54382">#54382</a> (getAttributeNodeNS doesn't get xmlns* attributes).</li>
</ul></li>
<li>DTrace:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73965">#73965</a> (DTrace reported as enabled when disabled).</li>
</ul></li>
<li>FCGI:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73904">#73904</a> (php-cgi fails to load -c specified php.ini file).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72898">#72898</a> (PHP_FCGI_CHILDREN is not included in phpinfo()).</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69865">#69865</a> (php-fpm does not close stderr when using syslog).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73968">#73968</a> (Premature failing of XBM reading).</li>
</ul></li>
<li>GMP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69993">#69993</a> (test for gmp.h needs to test machine includes).</li>
</ul></li>
<li>Hash:
<ul>
  <li>Added hash_hkdf() function.</li>
  <li>Fixed bug <a href="http://bugs.php.net/73961">#73961</a> (environmental build dependency in hash sha3 source).</li>
</ul></li>
<li>Intl:
<ul>
  <li>Fix bug #73956 (Link use CC instead of CXX).</li>
</ul></li>
<li>LDAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73933">#73933</a> (error/segfault with ldap_mod_replace and opcache).</li>
</ul></li>
<li>MySQLi:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73949">#73949</a> (leak in mysqli_fetch_object).</li>
</ul></li>
<li>Mysqlnd:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69899">#69899</a> (segfault on close() after free_result() with mysqlnd).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73983">#73983</a> (crash on finish work with phar in cli + opcache).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71519">#71519</a> (add serial hex to return value array).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73692">#73692</a> (Compile ext/openssl with openssl 1.1.0 on Win).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73978">#73978</a> (openssl_decrypt triggers bug in PDO).</li>
</ul></li>
<li>PDO_Firebird:
<ul>
  <li>Implemented FR <a href="http://bugs.php.net/72583">#72583</a> (All data are fetched as strings).</li>
</ul></li>
<li>PDO_PgSQL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73959">#73959</a> (lastInsertId fails to throw an exception for wrong sequence name).</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70417">#70417</a> (PharData::compress() doesn't close temp file).</li>
</ul></li>
<li>posix:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71219">#71219</a> (configure script incorrectly checks for ttyname_r).</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69582">#69582</a> (session not readable by root in CLI).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73896">#73896</a> (spl_autoload() crashes when calls magic _call()).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69442">#69442</a> (closing of fd incorrect when PTS enabled).</li>
  <li>Fixed bug <a href="http://bugs.php.net/47021">#47021</a> (SoapClient stumbles over WSDL delivered with "Transfer-Encoding: chunked").</li>
  <li>Fixed bug <a href="http://bugs.php.net/72974">#72974</a> (imap is undefined service on AIX).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72979">#72979</a> (money_format stores wrong length AIX).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73374">#73374</a> (intval() with base 0 should detect binary).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69061">#69061</a> (mail.log = syslog contains double information).</li>
</ul></li>
<li>ZIP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70103">#70103</a> (ZipArchive::addGlob ignores remove_all_path option).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.1"><!-- {{{ 7.1.1 -->
<h3>Version 7.1.1</h3>
<b><time class='releasedate' datetime='2017-01-19'>19 Jan 2017</time></b>
<ul>
	<li>
	Core
	<ul>
		<li>Fixed bug <a href="http://bugs.php.net/73792">#73792</a> (invalid foreach loop hangs script).</li>
		<li>Fixed bug <a href="http://bugs.php.net/73686">#73686</a> (Adding settype()ed values to ArrayObject results in references).</li>
		<li>Fixed bug <a href="http://bugs.php.net/73663">#73663</a> ("Invalid opcode 65/16/8" occurs with a variable created with list()).</li>
		<li>Fixed bug <a href="http://bugs.php.net/73727">#73727</a> (ZEND_MM_BITSET_LEN is "undefined symbol" in zend_bitset.h).</li>
		<li>Fixed bug <a href="http://bugs.php.net/73753">#73753</a> (unserialized array pointer not advancing).</li>
		<li>Fixed bug <a href="http://bugs.php.net/73783">#73783</a> (SIG_IGN doesn't work when Zend Signals is enabled).</li>
		<li>Fixed bug <a href="http://bugs.php.net/73825">#73825</a> (Heap out of bounds read on unserialize in finish_nested_data()). (CVE-2016-10161)</li>
		<li>Fixed bug <a href="http://bugs.php.net/73831">#73831</a> (NULL Pointer Dereference while unserialize php object). (CVE-2016-10162)</li>
		<li>Fixed bug <a href="http://bugs.php.net/73832">#73832</a> (Use of uninitialized memory in unserialize()). (CVE-2017-5340)</li>
		<li>Fixed bug <a href="http://bugs.php.net/73092">#73092</a> (Unserialize use-after-free when resizing object's properties hash table). (CVE-2016-7479)</li>
	</ul>
	</li>
	<li>
	CLI
	<ul>
		<li>Fixed bug <a href="http://bugs.php.net/72555">#72555</a> (CLI output(japanese) on Windows).</li>
	</ul>
	</li>
	<li>
	COM
	<ul>
		<li>Fixed bug <a href="http://bugs.php.net/73679">#73679</a> (DOTNET read access violation using invalid codepage).</li>
	</ul>
	</li>
	<li>
	DOM
	<ul>
		<li>Fixed bug <a href="http://bugs.php.net/67474">#67474</a> (getElementsByTagNameNS filter on default ns).</li>
	</ul>
	</li>
	<li>
	EXIF
	<ul>
		<li>Fixed bug <a href="http://bugs.php.net/73737">#73737</a> (FPE when parsing a tag format). (CVE-2016-10158)</li>
	</ul>
	</li>
	<li>
	GD
	<ul>
		<li>Fixed bug <a href="http://bugs.php.net/73869">#73869</a> (Signed Integer Overflow gd_io.c). (CVE-2016-10168)</li>
		<li>Fixed bug <a href="http://bugs.php.net/73868">#73868</a> (DOS vulnerability in gdImageCreateFromGd2Ctx()). (CVE-2016-10167)</li>
	</ul>
	</li>
	<li>
	mbstring
	<ul>
		<li>Fixed bug <a href="http://bugs.php.net/73646">#73646</a> (mb_ereg_search_init null pointer dereference).</li>
	</ul>
	</li>
	<li>
	MySQLi
	<ul>
		<li>Fixed bug <a href="http://bugs.php.net/73462">#73462</a> (Persistent connections don't set $connect_errno).</li>
	</ul>
	</li>
	<li>
	mysqlnd
	<ul>
		<li>Optimized handling of BIT fields - less memory copies and lower memory usage.</li>
		<li>Fixed bug <a href="http://bugs.php.net/73800">#73800</a> (sporadic segfault with MYSQLI_OPT_INT_AND_FLOAT_NATIVE).</li>
	</ul>
	</li>
	<li>
	opcache
	<ul>
		<li>Fixed bug <a href="http://bugs.php.net/73789">#73789</a> (Strange behavior of class constants in switch/case block).</li>
		<li>Fixed bug <a href="http://bugs.php.net/73746">#73746</a> (Method that returns string returns UNKNOWN:0 instead).</li>
		<li>Fixed bug <a href="http://bugs.php.net/73654">#73654</a> (Segmentation fault in zend_call_function).</li>
		<li>Fixed bug <a href="http://bugs.php.net/73668">#73668</a> ("SIGFPE Arithmetic exception" in opcache when divide by minus 1).</li>
		<li>Fixed bug <a href="http://bugs.php.net/73847">#73847</a> (Recursion when a variable is redefined as array).</li>
	</ul>
	</li>
	<li>
	PDO Firebird
	<ul>
		<li>Fixed bug <a href="http://bugs.php.net/72931">#72931</a> (PDO_FIREBIRD with Firebird 3.0 not work on returning statement).</li>
	</ul>
	</li>
	<li>Phar:
	<ul>
		<li>Fixed bug <a href="http://bugs.php.net/73773">#73773</a> (Seg fault when loading hostile phar). (CVE-2017-11147)</li>
		<li>Fixed bug <a href="http://bugs.php.net/73768">#73768</a> (Memory corruption when loading hostile phar). (CVE-2016-10160)</li>
		<li>Fixed bug <a href="http://bugs.php.net/73764">#73764</a> (Crash while loading hostile phar archive). (CVE-2016-10159)</li>
	</ul></li>
	<li>
	phpdbg
	<ul>
		<li>Fixed bug <a href="http://bugs.php.net/73794">#73794</a> (Crash (out of memory) when using run and # command separator).</li>
		<li>Fixed bug <a href="http://bugs.php.net/73704">#73704</a> (phpdbg shows the wrong line in files with shebang).</li>
	</ul>
	</li>
	<li>
	SQLite3
	<ul>
		<li>Reverted fix for Fixed bug <a href="http://bugs.php.net/73530">#73530</a> (Unsetting result set may reset other result set).</li>
	</ul>
	</li>
	<li>
	Standard
	<ul>
		<li>Fixed bug <a href="http://bugs.php.net/73594">#73594</a> (dns_get_record does not populate $additional out parameter).</li>
		<li>Fixed bug <a href="http://bugs.php.net/70213">#70213</a> (Unserialize context shared on double class lookup).</li>
		<li>Fixed bug <a href="http://bugs.php.net/73154">#73154</a> (serialize object with __sleep function crash).</li>
		<li>Fixed bug <a href="http://bugs.php.net/70490">#70490</a> (get_browser function is very slow).</li>
		<li>Fixed bug <a href="http://bugs.php.net/73265">#73265</a> (Loading browscap.ini at startup causes high memory usage).</li>
		<li>(add subject to mail log).</li>
		<li>Fixed bug <a href="http://bugs.php.net/31875">#31875</a> (get_defined_functions additional param to exclude disabled functions).</li>
	</ul>
	</li>
	<li>
	zlib
	<ul>
		<li>Fixed bug <a href="http://bugs.php.net/73373">#73373</a> (deflate_add does not verify that output was not truncated).</li>
	</ul>
	</li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.1.0"><!-- {{{ 7.1.0 -->
<h3>Version 7.1.0</h3>
<b><time class='releasedate' datetime='2016-12-01'>01 Dec 2016</time></b>
<ul><li>Core:
  <ul>
    <li>Added nullable types.</li>
    <li>Added DFA optimization framework based on e-SSA form.</li>
    <li>Added specialized opcode handlers (e.g. ZEND_ADD_LONG_NO_OVERFLOW).</li>
    <li>Added [] = as alternative construct to list() =.</li>
    <li>Added void return type.</li>
    <li>Added support for negative string offsets in string offset syntax and various string functions.</li>
    <li>Added a form of the list() construct where keys can be specified.</li>
    <li>Implemented safe execution timeout handling, that prevents random crashes after "Maximum execution time exceeded" error.</li>
    <li>Implemented the RFC `Support Class Constant Visibility`.</li>
    <li>Implemented the RFC `Catching multiple exception types`.</li>
    <li>Implemented logging to syslog with dynamic error levels.</li>
    <li>Implemented FR <a href="http://bugs.php.net/72614">#72614</a> (Support "nmake test" on building extensions by phpize).</li>
    <li>Implemented RFC: Iterable.</li>
    <li>Implemented RFC: Closure::fromCallable (Danack)</li>
    <li>Implemented RFC: Replace "Missing argument" warning with "\ArgumentCountError" exception.</li>
    <li>Implemented RFC: Fix inconsistent behavior of $this variable.</li>
    <li>Fixed bug <a href="http://bugs.php.net/73585">#73585</a> (Logging of "Internal Zend error - Missing class information" missing class name).</li>
    <li>Fixed memory leak(null coalescing operator with Spl hash).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72736">#72736</a> (Slow performance when fetching large dataset with mysqli / PDO).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72978">#72978</a> (Use After Free Vulnerability in unserialize()). (CVE-2016-9936)</li>
    <li>Fixed bug <a href="http://bugs.php.net/72482">#72482</a> (Ilegal write/read access caused by gdImageAALine overflow).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72696">#72696</a> (imagefilltoborder stackoverflow on truecolor images). (CVE-2016-9933)</li>
    <li>Fixed bug <a href="http://bugs.php.net/73350">#73350</a> (Exception::__toString() cause circular references).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73329">#73329</a> ((Float)"Nano" == NAN).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73288">#73288</a> (Segfault in __clone &gt; Exception.toString &gt; __get).</li>
    <li>Fixed for #73240 (Write out of bounds at number_format).</li>
    <li>Fix pthreads detection when cross-compiling (ffontaine)</li>
    <li>Fixed bug <a href="http://bugs.php.net/73337">#73337</a> (try/catch not working with two exceptions inside a same operation).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73156">#73156</a> (segfault on undefined function).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73163">#73163</a> (PHP hangs if error handler throws while accessing undef const in default value).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73172">#73172</a> (parse error: Invalid numeric literal).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73181">#73181</a> (parse_str() without a second argument leads to crash).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73025">#73025</a> (Heap Buffer Overflow in virtual_popen of zend_virtual_cwd.c).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73058">#73058</a> (crypt broken when salt is 'too' long).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72944">#72944</a> (Null pointer deref in zval_delref_p).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72943">#72943</a> (assign_dim on string doesn't reset hval).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72598">#72598</a> (Reference is lost after array_slice()).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72703">#72703</a> (Out of bounds global memory read in BF_crypt triggered by password_verify).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72813">#72813</a> (Segfault with __get returned by ref).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72767">#72767</a> (PHP Segfaults when trying to expand an infinite operator).</li>
    <li>TypeError messages for arg_info type checks will now say "must be ... or null" where the parameter or return type accepts null.</li>
    <li>Fixed bug <a href="http://bugs.php.net/72857">#72857</a> (stream_socket_recvfrom read access violation).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72663">#72663</a> (Create an Unexpected Object and Don't Invoke __wakeup() in Deserialization).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72681">#72681</a> (PHP Session Data Injection Vulnerability).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72742">#72742</a> (memory allocator fails to realloc small block to large one).</li>
    <li>Fixed URL rewriter. It would not rewrite '//example.com/' URL unconditionally. URL rewrite target hosts whitelist is implemented.</li>
    <li>Fixed bug <a href="http://bugs.php.net/72641">#72641</a> (phpize (on Windows) ignores PHP_PREFIX).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72683">#72683</a> (getmxrr broken).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72629">#72629</a> (Caught exception assignment to variables ignores references).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72594">#72594</a> (Calling an earlier instance of an included anonymous class fatals).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72581">#72581</a> (previous property undefined in Exception after deserialization).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72543">#72543</a> (Different references behavior comparing to PHP 5).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72347">#72347</a> (VERIFY_RETURN type casts visible in finally).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72216">#72216</a> (Return by reference with finally is not memory safe).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72215">#72215</a> (Wrong return value if var modified in finally).</li>
    <li>Fixed bug <a href="http://bugs.php.net/71818">#71818</a> (Memory leak when array altered in destructor).</li>
    <li>Fixed bug <a href="http://bugs.php.net/71539">#71539</a> (Memory error on $arr[$a] =&amp; $arr[$b] if RHS rehashes).</li>
    <li>Added new constant PHP_FD_SETSIZE.</li>
    <li>Added optind parameter to getopt().</li>
    <li>Added PHP to SAPI error severity mapping for logs.</li>
    <li>Fixed bug <a href="http://bugs.php.net/71911">#71911</a> (Unable to set --enable-debug on building extensions by phpize on Windows).</li>
    <li>Fixed bug <a href="http://bugs.php.net/29368">#29368</a> (The destructor is called when an exception is thrown from the constructor).</li>
    <li>Implemented RFC: RNG Fixes.</li>
    <li>Implemented email validation as per RFC 6531.</li>
    <li>Fixed bug <a href="http://bugs.php.net/72513">#72513</a> (Stack-based buffer overflow vulnerability in virtual_file_ex).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72573">#72573</a> (HTTP_PROXY is improperly trusted by some PHP libraries and applications).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72523">#72523</a> (dtrace issue with reflection (failed test)).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72508">#72508</a> (strange references after recursive function call and "switch" statement).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72441">#72441</a> (Segmentation fault: RFC list_keys).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72395">#72395</a> (list() regression).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72373">#72373</a> (TypeError after Generator function w/declared return type finishes).</li>
    <li>Fixed bug <a href="http://bugs.php.net/69489">#69489</a> (tempnam() should raise notice if falling back to temp dir).</li>
    <li>Fixed UTF-8 and long path support on Windows.</li>
    <li>Fixed bug <a href="http://bugs.php.net/53432">#53432</a> (Assignment via string index access on an empty string converts to array).</li>
    <li>Fixed bug <a href="http://bugs.php.net/62210">#62210</a> (Exceptions can leak temporary variables).</li>
    <li>Fixed bug <a href="http://bugs.php.net/62814">#62814</a> (It is possible to stiffen child class members visibility).</li>
    <li>Fixed bug <a href="http://bugs.php.net/69989">#69989</a> (Generators don't participate in cycle GC).</li>
    <li>Fixed bug <a href="http://bugs.php.net/70228">#70228</a> (Memleak if return in finally block).</li>
    <li>Fixed bug <a href="http://bugs.php.net/71266">#71266</a> (Missing separation of properties HT in foreach etc).</li>
    <li>Fixed bug <a href="http://bugs.php.net/71604">#71604</a> (Aborted Generators continue after nested finally).</li>
    <li>Fixed bug <a href="http://bugs.php.net/71572">#71572</a> (String offset assignment from an empty string inserts null byte).</li>
    <li>Fixed bug <a href="http://bugs.php.net/71897">#71897</a> (ASCII 0x7F Delete control character permitted in identifiers).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72188">#72188</a> (Nested try/finally blocks losing return value).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72213">#72213</a> (Finally leaks on nested exceptions).</li>
    <li>Fixed bug <a href="http://bugs.php.net/47517">#47517</a> (php-cgi.exe missing UAC manifest).</li>
    <li>Change statement and fcall extension handlers to accept frame.</li>
    <li>Number operators taking numeric strings now emit E_NOTICEs or E_WARNINGs when given malformed numeric strings.</li>
    <li>(int), intval() where $base is 10 or unspecified, settype(), decbin(), decoct(), dechex(), integer operators and other conversions now always respect scientific notation in numeric strings.</li>
    <li>Raise a compile-time warning on octal escape sequence overflow.</li>
  </ul></li>
<li>Apache2handler:
  <ul>
    <li>Enable per-module logging in Apache 2.4+.</li>
  </ul></li>
<li>BCmath:
  <ul>
    <li>Fix bug #73190 (memcpy negative parameter _bc_new_num_ex).</li>
  </ul></li>
<li>Bz2:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/72837">#72837</a> (integer overflow in bzdecompress caused heap corruption).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72613">#72613</a> (Inadequate error handling in bzread()).</li>
  </ul></li>
<li>Calendar:
  <ul>
    <li>Fix integer overflows (Joshua Rogers)</li>
    <li>Fixed bug <a href="http://bugs.php.net/67976">#67976</a> (cal_days_month() fails for final month of the French calendar).</li>
    <li>Fixed bug <a href="http://bugs.php.net/71894">#71894</a> (AddressSanitizer: global-buffer-overflow in zif_cal_from_jd).</li>
  </ul></li>
<li>CLI Server:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/73360">#73360</a> (Unable to work in root with unicode chars).</li>
    <li>Fixed bug <a href="http://bugs.php.net/71276">#71276</a> (Built-in webserver does not send Date header).</li>
  </ul></li>
<li>COM:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/73126">#73126</a> (Cannot pass parameter 1 by reference).</li>
    <li>Fixed bug <a href="http://bugs.php.net/69579">#69579</a> (Invalid free in extension trait).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72922">#72922</a> (COM called from PHP does not return out parameters).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72569">#72569</a> (DOTNET/COM array parameters broke in PHP7).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72498">#72498</a> (variant_date_from_timestamp null dereference).</li>
  </ul></li>
<li>Curl:
  <ul>
    <li>Implement support for handling HTTP/2 Server Push.</li>
    <li>Add curl_multi_errno(), curl_share_errno() and curl_share_strerror() functions.</li>
    <li>Fixed bug <a href="http://bugs.php.net/72674">#72674</a> (Heap overflow in curl_escape).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72541">#72541</a> (size_t overflow lead to heap corruption). (Stas).</li>
    <li>Fixed bug <a href="http://bugs.php.net/71709">#71709</a> (curl_setopt segfault with empty CURLOPT_HTTPHEADER).</li>
    <li>Fixed bug <a href="http://bugs.php.net/71929">#71929</a> (CURLINFO_CERTINFO data parsing error).</li>
  </ul></li>
<li>Date:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/69587">#69587</a> (DateInterval properties and isset).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73426">#73426</a> (createFromFormat with 'z' format char results in incorrect time).</li>
    <li>Fixed bug <a href="http://bugs.php.net/45554">#45554</a> (Inconsistent behavior of the u format char).</li>
    <li>Fixed bug <a href="http://bugs.php.net/48225">#48225</a> (DateTime parser doesn't set microseconds for "now").</li>
    <li>Fixed bug <a href="http://bugs.php.net/52514">#52514</a> (microseconds are missing in DateTime class).</li>
    <li>Fixed bug <a href="http://bugs.php.net/52519">#52519</a> (microseconds in DateInterval are missing).</li>
    <li>Fixed bug <a href="http://bugs.php.net/60089">#60089</a> (DateTime::createFromFormat() U after u nukes microtime).</li>
    <li>Fixed bug <a href="http://bugs.php.net/64887">#64887</a> (Allow DateTime modification with subsecond items).</li>
    <li>Fixed bug <a href="http://bugs.php.net/68506">#68506</a> (General DateTime improvments needed for microseconds to become useful).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73109">#73109</a> (timelib_meridian doesn't parse dots correctly).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73247">#73247</a> (DateTime constructor does not initialise microseconds property).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73147">#73147</a> (Use After Free in PHP7 unserialize()).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73189">#73189</a> (Memcpy negative size parameter php_resolve_path).</li>
    <li>Fixed bug <a href="http://bugs.php.net/66836">#66836</a> (DateTime::createFromFormat 'U' with pre 1970 dates fails parsing).</li>
    <li>Invalid serialization data for a DateTime or DatePeriod object will now throw an instance of Error from __wakeup() or __set_state() instead of resulting in a fatal error.</li>
    <li>Timezone initialization failure from serialized data will now throw an instance of Error from __wakeup() or __set_state() instead of resulting in a fatal error.</li>
    <li>Export date_get_interface_ce() for extension use.</li>
    <li>Fixed bug <a href="http://bugs.php.net/63740">#63740</a> (strtotime seems to use both sunday and monday as start of week).</li>
  </ul></li>
<li>Dba:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/70825">#70825</a> (Cannot fetch multiple values with group in ini file).</li>
    <li>Data modification functions (e.g.: dba_insert()) now throw an instance of Error instead of triggering a catchable fatal error if the key is does not contain exactly two elements.</li>
  </ul></li>
<li>DOM:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/73150">#73150</a> (missing NULL check in dom_document_save_html).</li>
    <li>Fixed bug <a href="http://bugs.php.net/66502">#66502</a> (DOM document dangling reference).</li>
    <li>Invalid schema or RelaxNG validation contexts will throw an instance of Error instead of resulting in a fatal error.</li>
    <li>Attempting to register a node class that does not extend the appropriate base class will now throw an instance of Error instead of resulting in a fatal error.</li>
    <li>Attempting to read an invalid or write to a readonly property will throw an instance of Error instead of resulting in a fatal error.</li>
  </ul></li>
<li>DTrace:
  <ul>
    <li>Disabled PHP call tracing by default (it makes significant overhead). This may be enabled again using envirionment variable USE_ZEND_DTRACE=1.</li>
  </ul></li>
<li>EXIF:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/72735">#72735</a> (Samsung picture thumb not read (zero size)).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72627">#72627</a> (Memory Leakage In exif_process_IFD_in_TIFF).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72603">#72603</a> (Out of bound read in exif_process_IFD_in_MAKERNOTE).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72618">#72618</a> (NULL Pointer Dereference in exif_process_user_comment).</li>
  </ul></li>
<li>Filter:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/72972">#72972</a> (Bad filter for the flags FILTER_FLAG_NO_RES_RANGE and FILTER_FLAG_NO_PRIV_RANGE).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73054">#73054</a> (default option ignored when object passed to int filter).</li>
    <li>Fixed bug <a href="http://bugs.php.net/71745">#71745</a> (FILTER_FLAG_NO_RES_RANGE does not cover whole 127.0.0.0/8 range).</li>
  </ul></li>
<li>FPM:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/72575">#72575</a> (using --allow-to-run-as-root should ignore missing user).</li>
  </ul></li>
<li>FTP:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/70195">#70195</a> (Cannot upload file using ftp_put to FTPES with require_ssl_reuse).</li>
    <li>Implemented FR <a href="http://bugs.php.net/55651">#55651</a> (Option to ignore the returned FTP PASV address).</li>
  </ul></li>
<li>GD:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/73213">#73213</a> (Integer overflow in imageline() with antialiasing).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73272">#73272</a> (imagescale() is not affected by, but affects imagesetinterpolation()).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73279">#73279</a> (Integer overflow in gdImageScaleBilinearPalette()).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73280">#73280</a> (Stack Buffer Overflow in GD dynamicGetbuf).</li>
    <li>Fixed bug <a href="http://bugs.php.net/50194">#50194</a> (imagettftext broken on transparent background w/o alphablending).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73003">#73003</a> (Integer Overflow in gdImageWebpCtx of gd_webp.c).</li>
    <li>Fixed bug <a href="http://bugs.php.net/53504">#53504</a> (imagettfbbox gives incorrect values for bounding box).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73157">#73157</a> (imagegd2() ignores 3rd param if 4 are given).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73155">#73155</a> (imagegd2() writes wrong chunk sizes on boundaries).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73159">#73159</a> (imagegd2(): unrecognized formats may result in corrupted files).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73161">#73161</a> (imagecreatefromgd2() may leak memory).</li>
    <li>Fixed bug <a href="http://bugs.php.net/67325">#67325</a> (imagetruecolortopalette: white is duplicated in palette).</li>
    <li>Fixed bug <a href="http://bugs.php.net/66005">#66005</a> (imagecopy does not support 1bit transparency on truecolor images).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72913">#72913</a> (imagecopy() loses single-color transparency on palette images).</li>
    <li>Fixed bug <a href="http://bugs.php.net/68716">#68716</a> (possible resource leaks in _php_image_convert()).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72709">#72709</a> (imagesetstyle() causes OOB read for empty $styles).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72697">#72697</a> (select_colors write out-of-bounds).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72730">#72730</a> (imagegammacorrect allows arbitrary write access).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72596">#72596</a> (imagetypes function won't advertise WEBP support).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72604">#72604</a> (imagearc() ignores thickness for full arcs).</li>
    <li>Fixed bug <a href="http://bugs.php.net/70315">#70315</a> (500 Server Error but page is fully rendered).</li>
    <li>Fixed bug <a href="http://bugs.php.net/43828">#43828</a> (broken transparency of imagearc for truecolor in blendingmode).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72512">#72512</a> (gdImageTrueColorToPaletteBody allows arbitrary write/read access).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72519">#72519</a> (imagegif/output out-of-bounds access).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72558">#72558</a> (Integer overflow error within _gdContributionsAlloc()).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72482">#72482</a> (Ilegal write/read access caused by gdImageAALine overflow).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72494">#72494</a> (imagecropauto out-of-bounds access).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72404">#72404</a> (imagecreatefromjpeg fails on selfie).</li>
    <li>Fixed bug <a href="http://bugs.php.net/43475">#43475</a> (Thick styled lines have scrambled patterns).</li>
    <li>Fixed bug <a href="http://bugs.php.net/53640">#53640</a> (XBM images require width to be multiple of 8).</li>
    <li>Fixed bug <a href="http://bugs.php.net/64641">#64641</a> (imagefilledpolygon doesn't draw horizontal line).</li>
  </ul></li>
<li>Hash:
  <ul>
    <li>Added SHA3 fixed mode algorithms (224, 256, 384, and 512 bit).</li>
    <li>Added SHA512/256 and SHA512/224 algorithms.</li>
  </ul></li>
<li>iconv:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/72320">#72320</a> (iconv_substr returns false for empty strings).</li>
  </ul></li>
<li>IMAP:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/73418">#73418</a> (Integer Overflow in "_php_imap_mail" leads to crash).</li>
    <li>An email address longer than 16385 bytes will throw an instance of Error instead of resulting in a fatal error.</li>
  </ul></li>
<li>Interbase:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/73512">#73512</a> (Fails to find firebird headers as don't use fb_config output).</li>
  </ul></li>
<li>Intl:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/73007">#73007</a> (add locale length check).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73218">#73218</a> (add mitigation for ICU int overflow).</li>
    <li>Fixed bug <a href="http://bugs.php.net/65732">#65732</a> (grapheme_*() is not Unicode compliant on CR LF sequence).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73007">#73007</a> (add locale length check).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72639">#72639</a> (Segfault when instantiating class that extends IntlCalendar and adds a property).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72658">#72658</a> (Locale::lookup() / locale_lookup() hangs if no match found).</li>
    <li>Partially fixed #72506 (idn_to_ascii for UTS #46 incorrect for long domain names).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72533">#72533</a> (locale_accept_from_http out-of-bounds access).</li>
    <li>Failure to call the parent constructor in a class extending Collator before invoking the parent methods will throw an instance of Error instead of resulting in a recoverable fatal error.</li>
    <li>Cloning a Transliterator object may will now throw an instance of Error instead of resulting in a fatal error if cloning the internal transliterator fails.</li>
    <li>Added IntlTimeZone::getWindowsID() and IntlTimeZone::getIDForWindowsID().</li>
    <li>Fixed bug <a href="http://bugs.php.net/69374">#69374</a> (IntlDateFormatter formatObject returns wrong utf8 value).</li>
    <li>Fixed bug <a href="http://bugs.php.net/69398">#69398</a> (IntlDateFormatter formatObject returns wrong value when time style is NONE).</li>
  </ul></li>
<li>JSON:
  <ul>
    <li>Introduced encoder struct instead of global which fixes bugs #66025 and #73254 related to pretty print indentation.</li>
    <li>Fixed bug <a href="http://bugs.php.net/73113">#73113</a> (Segfault with throwing JsonSerializable).</li>
    <li>Implemented earlier return when json_encode fails, fixes bugs #68992 (Stacking exceptions thrown by JsonSerializable) and #70275 (On recursion error, json_encode can eat up all system memory).</li>
    <li>Implemented FR <a href="http://bugs.php.net/46600">#46600</a> ("_empty_" key in objects).</li>
    <li>Exported JSON parser API including json_parser_method that can be used for implementing custom logic when parsing JSON.</li>
    <li>Escaped U+2028 and U+2029 when JSON_UNESCAPED_UNICODE is supplied as json_encode options and added JSON_UNESCAPED_LINE_TERMINATORS to restore the previous behaviour.</li>
  </ul></li>
<li>LDAP:
  <ul>
    <li>Providing an unknown modification type to ldap_batch_modify() will now throw an instance of Error instead of resulting in a fatal error.</li>
  </ul></li>
<li>Mbstring:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/73532">#73532</a> (Null pointer dereference in mb_eregi).</li>
    <li>Fixed bug <a href="http://bugs.php.net/66964">#66964</a> (mb_convert_variables() cannot detect recursion).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72992">#72992</a> (mbstring.internal_encoding doesn't inherit default_charset).</li>
    <li>Fixed bug <a href="http://bugs.php.net/66797">#66797</a> (mb_substr only takes 32-bit signed integer).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72711">#72711</a> (`mb_ereg` does not clear the `$regs` parameter on failure).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72691">#72691</a> (mb_ereg_search raises a warning if a match zero-width).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72693">#72693</a> (mb_ereg_search increments search position when a match zero-width).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72694">#72694</a> (mb_ereg_search_setpos does not accept a string's last position).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72710">#72710</a> (`mb_ereg` causes buffer overflow on regexp compile error).</li>
    <li>Deprecated mb_ereg_replace() eval option.</li>
    <li>Fixed bug <a href="http://bugs.php.net/69151">#69151</a> (mb_ereg should reject ill-formed byte sequence).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72405">#72405</a> (mb_ereg_replace - mbc_to_code (oniguruma) - oob read access).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72399">#72399</a> (Use-After-Free in MBString (search_re)).</li>
    <li>mb_ereg() and mb_eregi() will now throw an instance of ParseError if an invalid PHP expression is provided and the 'e' option is used.</li>
  </ul></li>
<li>Mcrypt:
  <ul>
    <li>Deprecated ext/mcrypt.</li>
    <li>Fixed bug <a href="http://bugs.php.net/72782">#72782</a> (Heap Overflow due to integer overflows).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72551">#72551</a>, bug #72552 (In correct casting from size_t to int lead to heap overflow in mdecrypt_generic).</li>
    <li>mcrypt_encrypt() and mcrypt_decrypt() will throw an instance of Error instead of resulting in a fatal error if mcrypt cannot be initialized.</li>
  </ul></li>
<li>Mysqli:
  <ul>
    <li>Attempting to read an invalid or write to a readonly property will throw an instance of Error instead of resulting in a fatal error.</li>
  </ul></li>
<li>Mysqlnd:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/64526">#64526</a> (Add missing mysqlnd.* parameters to php.ini-*).</li>
    <li>Fixed bug <a href="http://bugs.php.net/71863">#71863</a> (Segfault when EXPLAIN with "Unknown column" error when using MariaDB).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72701">#72701</a> (mysqli_get_host_info() wrong output).</li>
  </ul></li>
<li>OCI8:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/71148">#71148</a> (Bind reference overwritten on PHP 7).</li>
    <li>Fixed invalid handle error with Implicit Result Sets.</li>
    <li>Fixed bug <a href="http://bugs.php.net/72524">#72524</a> (Binding null values triggers ORA-24816 error).</li>
  </ul></li>
<li>ODBC:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/73448">#73448</a> (odbc_errormsg returns trash, always 513 bytes).</li>
  </ul></li>
<li>Opcache:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/73583">#73583</a> (Segfaults when conditionally declared class and function have the same name).</li>
    <li>Fixed bug <a href="http://bugs.php.net/69090">#69090</a> (check cached files permissions)</li>
    <li>Fixed bug <a href="http://bugs.php.net/72982">#72982</a> (Memory leak in zend_accel_blacklist_update_regexp() function).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72949">#72949</a> (Typo in opcache error message).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72762">#72762</a> (Infinite loop while parsing a file with opcache enabled).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72590">#72590</a> (Opcache restart with kill_all_lockers does not work).</li>
  </ul></li>
<li>OpenSSL:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/73478">#73478</a> (openssl_pkey_new() generates wrong pub/priv keys with Diffie Hellman).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73276">#73276</a> (crash in openssl_random_pseudo_bytes function).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73072">#73072</a> (Invalid path SNI_server_certs causes segfault).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72360">#72360</a> (ext/openssl build failure with OpenSSL 1.1.0).</li>
    <li>Bumped a minimal version to 1.0.1.</li>
    <li>Dropped support for SSL2.</li>
    <li>Implemented FR <a href="http://bugs.php.net/61204">#61204</a> (Add elliptic curve support for OpenSSL).</li>
    <li>Implemented FR <a href="http://bugs.php.net/67304">#67304</a> (Added AEAD support [CCM and GCM modes] to openssl_encrypt and openssl_decrypt).</li>
    <li>Implemented error storing to the global queue and cleaning up the OpenSSL error queue (resolves bugs #68276 and #69882).</li>
  </ul></li>
<li>Pcntl:
  <ul>
    <li>Implemented asynchronous signal handling without TICKS.</li>
    <li>Added pcntl_signal_get_handler() that returns the current signal handler for a particular signal. Addresses FR <a href="http://bugs.php.net/72409">#72409</a>.</li>
    <li>Add signinfo to pcntl_signal() handler args (Bishop Bettini, David Walker)</li>
  </ul></li>
<li>PCRE:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/73483">#73483</a> (Segmentation fault on pcre_replace_callback).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73612">#73612</a> (preg_*() may leak memory).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73392">#73392</a> (A use-after-free in zend allocator management).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73121">#73121</a> (Bundled PCRE doesn't compile because JIT isn't supported on s390).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72688">#72688</a> (preg_match missing group names in matches).</li>
    <li>Downgraded to PCRE 8.38.</li>
    <li>Fixed bug <a href="http://bugs.php.net/72476">#72476</a> (Memleak in jit_stack).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72463">#72463</a> (mail fails with invalid argument).</li>
    <li>Upgraded to PCRE 8.39.</li>
  </ul></li>
<li>PDO:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/72788">#72788</a> (Invalid memory access when using persistent PDO connection).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72791">#72791</a> (Memory leak in PDO persistent connection handling).</li>
    <li>Fixed bug <a href="http://bugs.php.net/60665">#60665</a> (call to empty() on NULL result using PDO::FETCH_LAZY returns false).</li>
  </ul></li>
<li>PDO_DBlib:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/72414">#72414</a> (Never quote values as raw binary data).</li>
    <li>Allow \PDO::setAttribute() to set query timeouts.</li>
    <li>Handle SQLDECIMAL/SQLNUMERIC types, which are used by later TDS versions.</li>
    <li>Add common PDO test suite.</li>
    <li>Free error and message strings when cleaning up PDO instances.</li>
    <li>Fixed bug <a href="http://bugs.php.net/67130">#67130</a> (\PDOStatement::nextRowset() should succeed when all rows in current rowset haven't been fetched).</li>
    <li>Ignore potentially misleading dberr values.</li>
    <li>Implemented stringify 'uniqueidentifier' fields.</li>
  </ul></li>
<li>PDO_Firebird:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/73087">#73087</a>, <a href="http://bugs.php.net/61183">#61183</a>, <a href="http://bugs.php.net/71494">#71494</a> (Memory corruption in bindParam).</li>
    <li>Fixed bug <a href="http://bugs.php.net/60052">#60052</a> (Integer returned as a 64bit integer on X86_64).</li>
  </ul></li>
<li>PDO_pgsql:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/70313">#70313</a> (PDO statement fails to throw exception).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72570">#72570</a> (Segmentation fault when binding parameters on a query without placeholders).</li>
    <li>Implemented FR <a href="http://bugs.php.net/72633">#72633</a> (Postgres PDO lastInsertId() should work without specifying a sequence).</li>
  </ul></li>
<li>Phar:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/72928">#72928</a> (Out of bound when verify signature of zip phar in phar_parse_zipfile).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73035">#73035</a> (Out of bound when verify signature of tar phar in phar_parse_tarfile).</li>
  </ul></li>
<li>phpdbg:
  <ul>
    <li>Added generator command for inspection of currently alive generators.</li>
  </ul></li>
<li>Postgres:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/73498">#73498</a> (Incorrect SQL generated for pg_copy_to()).</li>
    <li>Implemented FR <a href="http://bugs.php.net/31021">#31021</a> (pg_last_notice() is needed to get all notice messages).</li>
    <li>Implemented FR <a href="http://bugs.php.net/48532">#48532</a> (Allow pg_fetch_all() to index numerically).</li>
  </ul></li>
<li>Readline:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/72538">#72538</a> (readline_redisplay crashes php).</li>
  </ul></li>
<li>Reflection:
  <ul>
    <li>Undo backwards compatiblity break in ReflectionType-&gt;__toString() and deprecate via documentation instead.</li>
    <li>Reverted prepending \ for class names.</li>
    <li>Implemented request #38992 (invoke() and invokeArgs() static method calls should match). (cmb).</li>
    <li>Add ReflectionNamedType::getName(). This method should be used instead of ReflectionType::__toString()</li>
    <li>Prepend \ for class names and ? for nullable types returned from ReflectionType::__toString().</li>
    <li>Fixed bug <a href="http://bugs.php.net/72661">#72661</a> (ReflectionType::__toString crashes with iterable).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72222">#72222</a> (ReflectionClass::export doesn't handle array constants).</li>
    <li>Failure to retrieve a reflection object or retrieve an object property will now throw an instance of Error instead of resulting in a fatal error.</li>
    <li>Fix #72209 (ReflectionProperty::getValue() doesn't fail if object doesn't match type).</li>
  </ul></li>
<li>Session:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/73273">#73273</a> (session_unset() empties values from all variables in which is $_session stored).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73100">#73100</a> (session_destroy null dereference in ps_files_path_create).</li>
    <li>Fixed bug <a href="http://bugs.php.net/68015">#68015</a> (Session does not report invalid uid for files save handler).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72940">#72940</a> (SID always return "name=ID", even if session cookie exist).</li>
    <li>Implemented session_gc() (Yasuo) https://wiki.php.net/rfc/session-create-id</li>
    <li>Implemented session_create_id() (Yasuo) https://wiki.php.net/rfc/session-gc</li>
    <li>Implemented RFC: Session ID without hashing. (Yasuo) https://wiki.php.net/rfc/session-id-without-hashing</li>
    <li>Fixed bug <a href="http://bugs.php.net/72531">#72531</a> (ps_files_cleanup_dir Buffer overflow).</li>
    <li>Custom session handlers that do not return strings for session IDs will now throw an instance of Error instead of resulting in a fatal error when a function is called that must generate a session ID.</li>
    <li>An invalid setting for session.hash_function will throw an instance of Error instead of resulting in a fatal error when a session ID is created.</li>
    <li>Fixed bug <a href="http://bugs.php.net/72562">#72562</a> (Use After Free in unserialize() with Unexpected Session Deserialization).</li>
    <li>Improved fix for bug #68063 (Empty session IDs do still start sessions).</li>
    <li>Fixed bug <a href="http://bugs.php.net/71038">#71038</a> (session_start() returns TRUE on failure). Session save handlers must return 'string' always for successful read. i.e. Non-existing session read must return empty string. PHP 7.0 is made not to tolerate buggy return value.</li>
    <li>Fixed bug <a href="http://bugs.php.net/71394">#71394</a> (session_regenerate_id() must close opened session on errors).</li>
  </ul></li>
<li>SimpleXML:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/73293">#73293</a> (NULL pointer dereference in SimpleXMLElement::asXML()).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72971">#72971</a> (SimpleXML isset/unset do not respect namespace).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72957">#72957</a> (Null coalescing operator doesn't behave as expected with SimpleXMLElement).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72588">#72588</a> (Using global var doesn't work while accessing SimpleXML element).</li>
    <li>Creating an unnamed or duplicate attribute will throw an instance of Error instead of resulting in a fatal error.</li>
  </ul></li>
<li>SNMP:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/72708">#72708</a> (php_snmp_parse_oid integer overflow in memory allocation).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72479">#72479</a> (Use After Free Vulnerability in SNMP with GC and unserialize()).</li>
  </ul></li>
<li>Soap:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/73538">#73538</a> (SoapClient::__setSoapHeaders doesn't overwrite SOAP headers).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73452">#73452</a> (Segfault (Regression for #69152)).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73037">#73037</a> (SoapServer reports Bad Request when gzipped).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73237">#73237</a> (Nested object in "any" element overwrites other fields).</li>
    <li>Fixed bug <a href="http://bugs.php.net/69137">#69137</a> (Peer verification fails when using a proxy with SoapClient).</li>
    <li>Fixed bug <a href="http://bugs.php.net/71711">#71711</a> (Soap Server Member variables reference bug).</li>
    <li>Fixed bug <a href="http://bugs.php.net/71996">#71996</a> (Using references in arrays doesn't work like expected).</li>
  </ul></li>
<li>SPL:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/73423">#73423</a> (Reproducible crash with GDB backtrace).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72888">#72888</a> (Segfault on clone on splFileObject).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73029">#73029</a> (Missing type check when unserializing SplArray).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72646">#72646</a> (SplFileObject::getCsvControl does not return the escape character).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72684">#72684</a> (AppendIterator segfault with closed generator).</li>
    <li>Attempting to clone an SplDirectory object will throw an instance of Error instead of resulting in a fatal error.</li>
    <li>Calling ArrayIterator::append() when iterating over an object will throw an instance of Error instead of resulting in a fatal error.</li>
    <li>Fixed bug <a href="http://bugs.php.net/55701">#55701</a> (GlobIterator throws LogicException).</li>
  </ul></li>
<li>SQLite3:
  <ul>
    <li>Update to SQLite 3.15.1.</li>
    <li>Fixed bug <a href="http://bugs.php.net/73530">#73530</a> (Unsetting result set may reset other result set).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73333">#73333</a> (2147483647 is fetched as string).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72668">#72668</a> (Spurious warning when exception is thrown in user defined function).</li>
    <li>Implemented FR <a href="http://bugs.php.net/72653">#72653</a> (SQLite should allow opening with empty filename).</li>
    <li>Fixed bug <a href="http://bugs.php.net/70628">#70628</a> (Clearing bindings on an SQLite3 statement doesn't work).</li>
    <li>Implemented FR <a href="http://bugs.php.net/71159">#71159</a> (Upgraded bundled SQLite lib to 3.9.2).</li>
  </ul></li>
<li>Standard:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/73297">#73297</a> (HTTP stream wrapper should ignore HTTP 100 Continue).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73303">#73303</a> (Scope not inherited by eval in assert()).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73192">#73192</a> (parse_url return wrong hostname).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73203">#73203</a> (passing additional_parameters causes mail to fail).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73203">#73203</a> (passing additional_parameters causes mail to fail).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72920">#72920</a> (Accessing a private constant using constant() creates an exception AND warning).</li>
    <li>Fixed bug <a href="http://bugs.php.net/65550">#65550</a> (get_browser() incorrectly parses entries with "+" sign).</li>
    <li>Fixed bug <a href="http://bugs.php.net/71882">#71882</a> (Negative ftruncate() on php://memory exhausts memory).</li>
    <li>Fixed bug <a href="http://bugs.php.net/55451">#55451</a> (substr_compare NULL length interpreted as 0).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72278">#72278</a> (getimagesize returning FALSE on valid jpg).</li>
    <li>Fixed bug <a href="http://bugs.php.net/61967">#61967</a> (unset array item in array_walk_recursive cause inconsistent array).</li>
    <li>Fixed bug <a href="http://bugs.php.net/62607">#62607</a> (array_walk_recursive move internal pointer).</li>
    <li>Fixed bug <a href="http://bugs.php.net/69068">#69068</a> (Exchanging array during array_walk -&gt; memory errors).</li>
    <li>Fixed bug <a href="http://bugs.php.net/70713">#70713</a> (Use After Free Vulnerability in array_walk()/ array_walk_recursive()).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72622">#72622</a> (array_walk + array_replace_recursive create references from nothing).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72330">#72330</a> (CSV fields incorrectly split if escape char followed by UTF chars).</li>
    <li>Implemented RFC: More precise float values.</li>
    <li>array_multisort now uses zend_sort instead zend_qsort.</li>
    <li>Fixed bug <a href="http://bugs.php.net/72505">#72505</a> (readfile() mangles files larger than 2G).</li>
    <li>assert() will throw a ParseError when evaluating a string given as the first argument if the PHP code is invalid instead of resulting in a catchable fatal error.</li>
    <li>Calling forward_static_call() outside of a class scope will now throw an instance of Error instead of resulting in a fatal error.</li>
    <li>Added is_iterable() function.</li>
    <li>Fixed bug <a href="http://bugs.php.net/72306">#72306</a> (Heap overflow through proc_open and $env parameter).</li>
    <li>Fixed bug <a href="http://bugs.php.net/71100">#71100</a> (long2ip() doesn't accept integers in strict mode).</li>
    <li>Implemented FR <a href="http://bugs.php.net/55716">#55716</a> (Add an option to pass a custom stream context to get_headers()).</li>
    <li>Additional validation for parse_url() for login/pass components).</li>
    <li>Implemented FR <a href="http://bugs.php.net/69359">#69359</a> (Provide a way to fetch the current environment variables).</li>
    <li>unpack() function accepts an additional optional argument $offset.</li>
    <li>Implemented #51879 stream context socket option tcp_nodelay (Joe)</li>
  </ul></li>
<li>Streams:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/73586">#73586</a> (php_user_filter::$stream is not set to the stream the filter is working on).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72853">#72853</a> (stream_set_blocking doesn't work).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72743">#72743</a> (Out-of-bound read in php_stream_filter_create).</li>
    <li>Implemented FR <a href="http://bugs.php.net/27814">#27814</a> (Multiple small packets send for HTTP request).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72764">#72764</a> (ftps:// opendir wrapper data channel encryption fails with IIS FTP 7.5, 8.5).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72810">#72810</a> (Missing SKIP_ONLINE_TESTS checks).</li>
    <li>Fixed bug <a href="http://bugs.php.net/41021">#41021</a> (Problems with the ftps wrapper).</li>
    <li>Fixed bug <a href="http://bugs.php.net/54431">#54431</a> (opendir() does not work with ftps:// wrapper).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72667">#72667</a> (opendir() with ftp:// attempts to open data stream for non-existent directories).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72771">#72771</a> (ftps:// wrapper is vulnerable to protocol downgrade attack).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72534">#72534</a> (stream_socket_get_name crashes).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72439">#72439</a> (Stream socket with remote address leads to a segmentation fault).</li>
  </ul></li>
<li>sysvshm:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/72858">#72858</a> (shm_attach null dereference).</li>
  </ul></li>
<li>Tidy:
  <ul>
    <li>Implemented support for libtidy 5.0.0 and above.</li>
    <li>Creating a tidyNode manually will now throw an instance of Error instead of resulting in a fatal error.</li>
  </ul></li>
<li>Wddx:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/73331">#73331</a> (NULL Pointer Dereference in WDDX Packet Deserialization with PDORow). (CVE-2016-9934)</li>
    <li>Fixed bug <a href="http://bugs.php.net/72142">#72142</a> (WDDX Packet Injection Vulnerability in wddx_serialize_value()).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72749">#72749</a> (wddx_deserialize allows illegal memory access).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72750">#72750</a> (wddx_deserialize null dereference).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72790">#72790</a> (wddx_deserialize null dereference with invalid xml).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72799">#72799</a> (wddx_deserialize null dereference in php_wddx_pop_element).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72860">#72860</a> (wddx_deserialize use-after-free).</li>
    <li>Fixed bug <a href="http://bugs.php.net/73065">#73065</a> (Out-Of-Bounds Read in php_wddx_push_element).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72564">#72564</a> (boolean always deserialized as "true").</li>
    <li>A circular reference when serializing will now throw an instance of Error instead of resulting in a fatal error.</li>
  </ul></li>
<li>XML:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/72135">#72135</a> (malformed XML causes fault).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72714">#72714</a> (_xml_startElementHandler() segmentation fault).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72085">#72085</a> (SEGV on unknown address zif_xml_parse).</li>
  </ul></li>
<li>XMLRPC:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/72647">#72647</a> (xmlrpc_encode() unexpected output after referencing array elements).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72606">#72606</a> (heap-buffer-overflow (write) simplestring_addn simplestring.c).</li>
    <li>A circular reference when serializing will now throw an instance of Error instead of resulting in a fatal error.</li>
  </ul></li>
<li>Zip:
  <ul>
    <li>Fixed bug <a href="http://bugs.php.net/68302">#68302</a> (impossible to compile php with zip support).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72660">#72660</a> (NULL Pointer dereference in zend_virtual_cwd).</li>
    <li>Fixed bug <a href="http://bugs.php.net/72520">#72520</a> (Stack-based buffer overflow vulnerability in php_stream_zip_opener).</li>
    <li>ZipArchive::addGlob() will throw an instance of Error instead of resulting in a fatal error if glob support is not available.</li>
  </ul></li>
</ul>
<!-- }}} --></section>

<a name="PHP_7_0"></a>
<section class="version" id="7.0.33"><!-- {{{ 7.0.33 -->
<h3>Version 7.0.33</h3>
<b><time class='releasedate' datetime='2018-12-06'>06 Dec 2018</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77231">#77231</a> (Segfault when using convert.quoted-printable-encode filter).</li>
</ul></li>
<li>IMAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77020">#77020</a> (null pointer dereference in imap_mail).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77153">#77153</a> (imap_open allows to run arbitrary shell commands via mailbox parameter). (CVE-2018-19518)</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/77022">#77022</a> (PharData always creates new files with mode 0666).</li>
  <li>Fixed bug <a href="http://bugs.php.net/77143">#77143</a> (Heap Buffer Overflow (READ: 4) in phar_parse_pharfile). (CVE-2018-20783)</li>
</ul></li>
</ul>
<!-- }}} --></section>


<section class="version" id="7.0.32"><!-- {{{ 7.0.32 -->
<h3>Version 7.0.32</h3>
<b><time class='releasedate' datetime='2018-09-13'>13 Sep 2018</time></b>
<ul><li>Apache2:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76582">#76582</a> (XSS due to the header Transfer-Encoding: chunked). (CVE-2018-17082)</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.31"><!-- {{{ 7.0.31 -->
<h3>Version 7.0.31</h3>
<b><time class='releasedate' datetime='2018-07-19'>19 Jul 2018</time></b>
<ul><li>Exif:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76423">#76423</a> (Int Overflow lead to Heap OverFlow in exif_thumbnail_extract of exif.c). (CVE-2018-14883)</li>
  <li>Fixed bug <a href="http://bugs.php.net/76557">#76557</a> (heap-buffer-overflow (READ of size 48) while reading exif data). (CVE-2018-14851)</li>
</ul></li>
<li>Win32:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76459">#76459</a> (windows linkinfo lacks openbasedir check). (CVE-2018-15132)</li>
</ul></li>
</ul>
<!-- }}} --></section>


<section class="version" id="7.0.30"><!-- {{{ 7.0.30 -->
<h3>Version 7.0.30</h3>
<b><time class='releasedate' datetime='2018-04-26'>26 Apr 2018</time></b>
<ul><li>Exif:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76130">#76130</a> (Heap Buffer Overflow (READ: 1786) in exif_iif_add_value). (CVE-2018-10549)</li>
</ul></li>
<li>iconv:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76249">#76249</a> (stream filter convert.iconv leads to infinite loop on invalid sequence). (CVE-2018-10546)</li>
</ul></li>
<li>LDAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76248">#76248</a> (Malicious LDAP-Server Response causes Crash). (CVE-2018-10548)</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/76129">#76129</a> (fix for CVE-2018-5712 may not be complete). (CVE-2018-10547)</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.29"><!-- {{{ 7.0.29 -->
<h3>Version 7.0.29</h3>
<b><time class='releasedate' datetime='2018-03-29'>29 Mar 2018</time></b>
<ul><li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75605">#75605</a> (Dumpable FPM child processes allow bypassing opcache access controls). (CVE-2018-10545)</li>
</ul></li>
</ul>
<!-- }}} --></section>


<section class="version" id="7.0.28"><!-- {{{ 7.0.28 -->
<h3>Version 7.0.28</h3>
<b><time class='releasedate' datetime='2018-03-01'>01 Mar 2018</time></b>
<ul><li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75981">#75981</a> (stack-buffer-overflow while parsing HTTP response). (CVE-2018-7584)</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.27"><!-- {{{ 7.0.27 -->
<h3>Version 7.0.27</h3>
<b><time class='releasedate' datetime='2018-01-04'>04 Jan 2018</time></b>
<ul><li>CLI Server:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/60471">#60471</a> (Random "Invalid request (unexpected EOF)" using a router script).</li>
</ul></li>
<li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75384">#75384</a> (PHP seems incompatible with OneDrive files on demand).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75573">#75573</a> (Segmentation fault in 7.1.12 and 7.0.26).</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/64938">#64938</a> (libxml_disable_entity_loader setting is shared between requests).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75571">#75571</a> (Potential infinite loop in gdImageCreateFromGifCtx). (CVE-2018-5711)</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75579">#75579</a> (Interned strings buffer overflow may cause crash).</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74183">#74183</a> (preg_last_error not returning error code after error).</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74782">#74782</a> (Reflected XSS in .phar 404 page). (CVE-2018-5712)</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75535">#75535</a> (Inappropriately parsing HTTP response leads to PHP segment fault). (CVE-2018-14884)</li>
  <li>Fixed bug <a href="http://bugs.php.net/75409">#75409</a> (accept EFAULT in addition to ENOSYS as indicator that getrandom() is missing).</li>
</ul></li>
<li>Zip:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75540">#75540</a> (Segfault with libzip 1.3.1).</li>
</ul></li>
</ul>
<!-- }}} --></section>


<section class="version" id="7.0.26"><!-- {{{ 7.0.26 -->
<h3>Version 7.0.26</h3>
<b><time class='releasedate' datetime='2017-11-23'>23 Nov 2017</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75420">#75420</a> (Crash when modifing property name in __isset for BP_VAR_IS).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75368">#75368</a> (mmap/munmap trashing on unlucky allocations).</li>
</ul></li>
<li>CLI:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75287">#75287</a> (Builtin webserver crash after chdir in a shutdown function).</li>
</ul></li>
<li>Enchant:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/53070">#53070</a> (enchant_broker_get_path crashes if no path is set).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75365">#75365</a> (Enchant still reports version 1.1.0).</li>
</ul></li>
<li>Exif:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75301">#75301</a> (Exif extension has built in revision version).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/65148">#65148</a> (imagerotate may alter image dimensions).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75437">#75437</a> (Wrong reflection on imagewebp).</li>
</ul></li>
<li>intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75317">#75317</a> (UConverter::setDestinationEncoding changes source instead of destination).</li>
</ul></li>
<li>interbase:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75453">#75453</a> (Incorrect reflection for ibase_[p]connect).</li>
</ul></li>
<li>Mysqli:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75434">#75434</a> (Wrong reflection for mysqli_fetch_all function).</li>
</ul></li>
<li>OCI8:
<ul>
  <li>Fixed valgrind issue.</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75373">#75373</a> (Warning Internal error: wrong size calculation).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75363">#75363</a> (openssl_x509_parse leaks memory).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75307">#75307</a> (Wrong reflection for openssl_open function).</li>
</ul></li>
<li>PGSQL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75419">#75419</a> (Default link incorrectly cleared/linked by pg_close()).</li>
</ul></li>
<li>SOAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75464">#75464</a> (Wrong reflection on SoapClient::__setSoapHeaders).</li>
</ul></li>
<li>Zlib:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75299">#75299</a> (Wrong reflection on inflate_init and inflate_add).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.25"><!-- {{{ 7.0.25 -->
<h3>Version 7.0.25</h3>
<b><time class='releasedate' datetime='2017-10-26'>26 Oct 2017</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75241">#75241</a> (Null pointer dereference in zend_mm_alloc_small()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75236">#75236</a> (infinite loop when printing an error-message).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75252">#75252</a> (Incorrect token formatting on two parse errors in one request).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75220">#75220</a> (Segfault when calling is_callable on parent).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75290">#75290</a> (debug info of Closures of internal functions contain garbage argument names).</li>
</ul></li>
<li>Apache2Handler:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75311">#75311</a> (error: 'zend_hash_key' has no member named 'arKey' in apache2handler).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75055">#75055</a> (Out-Of-Bounds Read in timelib_meridian()). (CVE-2017-16642)</li>
</ul></li>
<li>Intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75318">#75318</a> (The parameter of UConverter::getAliases() is not optional).</li>
</ul></li>
<li>mcrypt:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72535">#72535</a> (arcfour encryption stream filter crashes php).</li>
</ul></li>
<li>OCI8:
<ul>
  <li>Fixed incorrect reference counting.</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75207">#75207</a> (applied upstream patch for CVE-2016-1283).</li>
</ul></li>
<li>litespeed:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75248">#75248</a> (Binary directory doesn't get created when building only litespeed SAPI).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75251">#75251</a> (Missing program prefix and suffix).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73629">#73629</a> (SplDoublyLinkedList::setIteratorMode masks intern flags).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.24"><!-- {{{ 7.0.24 -->
<h3>Version 7.0.24</h3>
<b><time class='releasedate' datetime='2017-09-28'>28 Sep 2017</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75042">#75042</a> (run-tests.php issues with EXTENSION block).</li>
</ul></li>
<li>BCMath:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/44995">#44995</a> (bcpowmod() fails if scale != 0).</li>
  <li>Fixed bug <a href="http://bugs.php.net/46781">#46781</a> (BC math handles minus zero incorrectly).</li>
  <li>Fixed bug <a href="http://bugs.php.net/54598">#54598</a> (bcpowmod() may return 1 if modulus is 1).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75178">#75178</a> (bcpowmod() misbehaves for non-integer base or modulus).</li>
</ul></li>
<li>CLI server:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70470">#70470</a> (Built-in server truncates headers spanning over TCP packets).</li>
</ul></li>
<li>CURL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75093">#75093</a> (OpenSSL support not detected).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75124">#75124</a> (gdImageGrayScale() may produce colors).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75139">#75139</a> (libgd/gd_interpolation.c:1786: suspicious if ?).</li>
</ul></li>
<li>Gettext:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73730">#73730</a> (textdomain(null) throws in strict mode).</li>
</ul></li>
<li>Intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75090">#75090</a> (IntlGregorianCalendar doesn't have constants from parent class).</li>
</ul></li>
<li>PDO_OCI:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74631">#74631</a> (PDO_PCO with PHP-FPM: OCI environment initialized before PHP-FPM sets it up).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75173">#75173</a> (incorrect behavior of AppendIterator::append in foreach loop).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75097">#75097</a> (gethostname fails if your host name is 64 chars long).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.23"><!-- {{{ 7.0.23 -->
<h3>Version 7.0.23</h3>
<b><time class='releasedate' datetime='2017-08-31'>31 Aug 2017</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74947">#74947</a> (Segfault in scanner on INF number).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74954">#74954</a> (null deref and segfault in zend_generator_resume()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74725">#74725</a> (html_errors=1 breaks unhandled exceptions).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75349">#75349</a> (NAN comparison).</li>
</ul></li>
<li>cURL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74125">#74125</a> (Fixed finding CURL on systems with multiarch support).</li>
</ul></li>
<li>Date:
<ul>
<li>Fixed bug <a href="http://bugs.php.net/75002">#75002</a> (Null Pointer Dereference in timelib_time_clone).</li>
</ul></li>
<li>Intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74993">#74993</a> (Wrong reflection on some locale_* functions).</li>
</ul></li>
<li>Mbstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71606">#71606</a> (Segmentation fault mb_strcut with HTML-ENTITIES encoding).</li>
  <li>Fixed bug <a href="http://bugs.php.net/62934">#62934</a> (mb_convert_kana() does not convert iteration marks).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75001">#75001</a> (Wrong reflection on mb_eregi_replace).</li>
</ul></li>
<li>MySQLi:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74968">#74968</a> (PHP crashes when calling mysqli_result::fetch_object with an abstract class).</li>
</ul></li>
<li>OCI8:
<ul>
  <li>Expose oci_unregister_taf_callback() (Tianfang Yang)</li>
</ul></li>
<li>phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74991">#74991</a> (include_path has a 4096 char limit in some cases).</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74949">#74949</a> (null pointer dereference in _function_string).</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74833">#74833</a> (SID constant created with wrong module number).</li>
</ul></li>
<li>SimpleXML:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74950">#74950</a> (nullpointer deref in simplexml_element_getDocNamespaces).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75049">#75049</a> (spl_autoload_unregister can't handle spl_autoload_functions results).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74669">#74669</a> (Unserialize ArrayIterator broken).</li>
  <li>Fixed bug <a href="http://bugs.php.net/75015">#75015</a> (Crash in recursive iterator destructors).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/75075">#75075</a> (unpack with X* causes infinity loop).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74103">#74103</a> (heap-use-after-free when unserializing invalid array size). (CVE-2017-12932)</li>
  <li>Fixed bug <a href="http://bugs.php.net/75054">#75054</a> (A Denial of Service Vulnerability was found when performing deserialization).</li>
</ul></li>
<li>WDDX:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73793">#73793</a> (WDDX uses wrong decimal seperator).</li>
</ul></li>
<li>XMLRPC:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74975">#74975</a> (Incorrect xmlrpc serialization for classes with declared properties).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.22"><!-- {{{ 7.0.22 -->
<h3>Version 7.0.22</h3>
<b><time class='releasedate' datetime='2017-08-03'>03 Aug 2017</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74832">#74832</a> (Loading PHP extension with already registered function name leads to a crash).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74780">#74780</a> (parse_url() borken when query string contains colon).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74761">#74761</a> (Unary operator expected error on some systems).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73900">#73900</a> (Use After Free in unserialize() SplFixedArray).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74913">#74913</a> (fixed incorrect poll.h include).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74906">#74906</a> (fixed incorrect errno.h include).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74852">#74852</a> (property_exists returns true on unknown DateInterval property).</li>
</ul></li>
<li>OCI8:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74625">#74625</a> (Integer overflow in oci_bind_array_by_name).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74840">#74840</a> (Opcache overwrites argument of GENERATOR_RETURN within finally).</li>
</ul></li>
<li>PDO:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69356">#69356</a> (PDOStatement::debugDumpParams() truncates query).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73471">#73471</a> (PHP freezes with AppendIterator).</li>
</ul></li>
<li>SQLite3:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74883">#74883</a> (SQLite3::__construct() produces "out of memory" exception with invalid flags).</li>
</ul></li>
<li>Wddx:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73173">#73173</a> (huge memleak when wddx_unserialize).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74145">#74145</a> (wddx parsing empty boolean tag leads to SIGSEGV). (CVE-2017-11143)</li>
</ul></li>
<li>zlib:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73944">#73944</a> (dictionary option of inflate_init() does not work).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.21"><!-- {{{ 7.0.21 -->
<h3>Version 7.0.21</h3>
<b><time class='releasedate' datetime='2017-07-06'>06 Jul 2017</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74738">#74738</a> (Multiple [PATH=] and [HOST=] sections not properly parsed).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74658">#74658</a> (Undefined constants in array properties result in broken properties).</li>
  <li>Fixed misparsing of abstract unix domain socket names.</li>
  <li>Fixed bug <a href="http://bugs.php.net/74101">#74101</a> (Unserialize Heap Use-After-Free (READ: 1) in zval_get_type). (CVE-2017-12934)</li>
  <li>Fixed bug <a href="http://bugs.php.net/74111">#74111</a> (Heap buffer overread (READ: 1) finish_nested_data from unserialize). (CVE-2017-12933)</li>
  <li>Fixed bug <a href="http://bugs.php.net/74603">#74603</a> (PHP INI Parsing Stack Buffer Overflow Vulnerability). (CVE-2017-11628)</li>
  <li>Fixed bug <a href="http://bugs.php.net/74819">#74819</a> (wddx_deserialize() heap out-of-bound read via php_parse_date()). (CVE-2017-11145)</li>
</ul></li>
<li>DOM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69373">#69373</a> (References to deleted XPath query results).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74435">#74435</a> (Buffer over-read into uninitialized memory). (CVE-2017-7890)</li>
</ul></li>
<li>Intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73473">#73473</a> (Stack Buffer Overflow in msgfmt_parse_message). (CVE-2017-11362)</li>
  <li>Fixed bug <a href="http://bugs.php.net/74705">#74705</a> (Wrong reflection on Collator::getSortKey and collator_get_sort_key).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73634">#73634</a> (grapheme_strpos illegal memory access).</li>
</ul></li>
<li>Mbstring:
<ul>
  <li>Add oniguruma upstream fix (CVE-2017-9224, CVE-2017-9226, CVE-2017-9227, CVE-2017-9228, CVE-2017-9229)</li>
</ul></li>
<li>OCI8:
<ul>
  <li>Add TAF callback (PR #2459).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74663">#74663</a> (Segfault with opcache.memory_protect and validate_timestamp).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74651">#74651</a> (negative-size-param (-1) in memcpy in zif_openssl_seal()). (CVE-2017-11144)</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74087">#74087</a> (Segmentation fault in PHP7.1.1(compiled using the bundled PCRE library)).</li>
</ul></li>
<li>PDO_OCI:
<ul>
  <li>Support Instant Client 12.2 in --with-pdo-oci configure option.</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74673">#74673</a> (Segfault when cast Reflection object to string with undefined constant).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74478">#74478</a> (null coalescing operator failing with SplFixedArray).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74708">#74708</a> (Invalid Reflection signatures for random_bytes and random_int).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73648">#73648</a> (Heap buffer overflow in substr).</li>
</ul></li>
<li>FTP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74598">#74598</a> (ftp:// wrapper ignores context arg).</li>
</ul></li>
<li>PHAR:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74386">#74386</a> (Phar::__construct reflection incorrect).</li>
</ul></li>
<li>SOAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74679">#74679</a> (Incorrect conversion array with WSDL_CACHE_MEMORY).</li>
</ul></li>
<li>Streams:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74556">#74556</a> (stream_socket_get_name() returns '\0').</li>
</ul></li>
</ul>
<!-- }}} --></section>


<section class="version" id="7.0.20"><!-- {{{ 7.0.20 -->
<h3>Version 7.0.20</h3>
<b><time class='releasedate' datetime='2017-06-08'>08 Jun 2017</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74600">#74600</a> (crash (SIGSEGV) in _zend_hash_add_or_update_i).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74546">#74546</a> (SIGILL in ZEND_FETCH_CLASS_CONSTANT_SPEC_CONST_CONST).</li>
</ul></li>
<li>intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74468">#74468</a> (wrong reflection on Collator::sortWithSortKeys).</li>
</ul></li>
<li>MySQLi:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74547">#74547</a> (mysqli::change_user() doesn't accept null as $database argument w/strict_types).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74596">#74596</a> (SIGSEGV with opcache.revalidate_path enabled).</li>
</ul></li>
<li>phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/51918">#51918</a> (Phar::webPhar() does not handle requests sent through PUT and DELETE method).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74510">#74510</a> (win32/sendmail.c anchors CC header but not BCC).</li>
</ul></li>
<li>xmlreader:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74457">#74457</a> (Wrong reflection on XMLReader::expand).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.19"><!-- {{{ 7.0.19 -->
<h3>Version 7.0.19</h3>
<b><time class='releasedate' datetime='2017-05-11'>11 May 2017</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74188">#74188</a> (Null coalescing operator fails for undeclared static class properties).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74408">#74408</a> (Endless loop bypassing execution time limit).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74410">#74410</a> (stream_select() is broken on Windows Nanoserver).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74337">#74337</a> (php-cgi.exe crash on facebook callback).</li>
  <li>Patch for bug <a href="http://bugs.php.net/74216">#74216</a> was reverted.</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74404">#74404</a> (Wrong reflection on DateTimeZone::getTransitions).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74080">#74080</a> (add constant for RFC7231 format datetime).</li>
</ul></li>
<li>DOM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74416">#74416</a> (Wrong reflection on DOMNode::cloneNode).</li>
</ul></li>
<li>Fileinfo:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74379">#74379</a> (syntax error compile error in libmagic/apprentice.c).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74343">#74343</a> (compile fails on solaris 11 with system gd2 library).</li>
</ul></li>
<li>intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74433">#74433</a> (wrong reflection for Normalizer methods).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74439">#74439</a> (wrong reflection for Locale methods).</li>
</ul></li>
<li>MySQLi:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74432">#74432</a> (mysqli_connect adding ":3306" to $host if $port parameter not given).</li>
</ul></li>
<li>MySQLnd:
<ul>
  <li>Added support for MySQL 8.0 types.</li>
  <li>Fixed bug <a href="http://bugs.php.net/74376">#74376</a> (Invalid free of persistent results on error/connection loss).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73833">#73833</a> (null character not allowed in openssl_pkey_get_private).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73711">#73711</a> (Segfault in openssl_pkey_new when generating DSA or DH key).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74341">#74341</a> (openssl_x509_parse fails to parse ASN.1 UTCTime without seconds).</li>
  <li>Added OpenSSL 1.1.0 support.</li>
</ul></li>
<li>phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74383">#74383</a> (phar method parameters reflection correction).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74409">#74409</a> (Reflection information for ini_get_all() is incomplete).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72071">#72071</a> (setcookie allows max-age to be negative).</li>
</ul></li>
<li>Streams:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74429">#74429</a> (Remote socket URI with unique persistence identifier broken).</li>
</ul></li>
<li>SQLite3:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74413">#74413</a> (incorrect reflection for SQLite3::enableExceptions).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.18"><!-- {{{ 7.0.18 -->
<h3>Version 7.0.18</h3>
<b><time class='releasedate' datetime='2017-04-13'>13 Apr 2017</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73370">#73370</a> (falsely exits with "Out of Memory" when using USE_ZEND_ALLOC=0).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73960">#73960</a> (Leak with instance method calling static method with referenced return).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74265">#74265</a> (Build problems after 7.0.17 release: undefined reference to `isfinite').</li>
  <li>Fixed bug <a href="http://bugs.php.net/74302">#74302</a> (yield fromLABEL is over-greedy).</li>
</ul></li>
<li>Apache:
<ul>
  <li>Reverted patch for bug #61471, fixes bug #74318.</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72096">#72096</a> (Swatch time value incorrect for dates before 1970).</li>
</ul></li>
<li>DOM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74004">#74004</a> (LIBXML_NOWARNING flag ingnored on loadHTML*).</li>
</ul></li>
<li>iconv:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74230">#74230</a> (iconv fails to fail on surrogates).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72333">#72333</a> (fwrite() on non-blocking SSL sockets doesn't work).</li>
</ul></li>
<li>PDO MySQL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71003">#71003</a> (Expose MYSQLI_CLIENT_SSL_DONT_VERIFY_SERVER_CERT to PDO interface).</li>
</ul></li>
<li>Streams:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74216">#74216</a> (Correctly fail on invalid IP address ports).</li>
</ul></li>
<li>Zlib:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74240">#74240</a> (deflate_add can allocate too much memory).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.17"><!-- {{{ 7.0.17 -->
<h3>Version 7.0.17</h3>
<b><time class='releasedate' datetime='2017-03-16'>16 Mar 2017</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73989">#73989</a> (PHP 7.1 Segfaults within Symfony test suite).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74084">#74084</a> (Out of bound read - zend_mm_alloc_small).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73807">#73807</a> (Performance problem with processing large post request). (CVE-2017-11142)</li>
  <li>Fixed bug <a href="http://bugs.php.net/73998">#73998</a> (array_key_exists fails on arrays created by get_object_vars).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73954">#73954</a> (NAN check fails on Alpine Linux with musl).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74039">#74039</a> (is_infinite(-INF) returns false).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73677">#73677</a> (Generating phar.phar core dump with gcc ASAN enabled build).</li>
</ul></li>
<li>Apache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/61471">#61471</a> (Incomplete POST does not timeout but is passed to PHP).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72719">#72719</a> (Relative datetime format ignores weekday on sundays only).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73294">#73294</a> (DateTime wrong when date string is negative).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73489">#73489</a> (wrong timestamp when call setTimeZone multi times with UTC offset).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73858">#73858</a> (first/last day of' flag is not being reset).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73942">#73942</a> ($date-&gt;modify('Friday this week') doesn't return a Friday if $date is a Sunday).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74057">#74057</a> (wrong day when using "this week" in strtotime).</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69860">#69860</a> (php-fpm process accounting is broken with keepalive).</li>
</ul></li>
<li>Hash:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73127">#73127</a> (gost-crypto hash incorrect if input data contains long 0xFF sequence).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74031">#74031</a> (ReflectionFunction for imagepng is missing last two parameters).</li>
</ul></li>
<li>Mysqlnd:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74021">#74021</a> (fetch_array broken data. Data more then MEDIUMBLOB).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74152">#74152</a> (if statement says true to a null variable).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74019">#74019</a> (Segfault with list).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74022">#74022</a> (PHP Fast CGI crashes when reading from a pfx file).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/74148">#74148</a> (ReflectionFunction incorrectly reports the number of arguments).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74005">#74005</a> (mail.add_x_header causes RFC-breaking lone line feed).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73118">#73118</a> (is_callable callable name reports misleading value for anonymous classes).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74105">#74105</a> (PHP on Linux should use /dev/urandom when getrandom is not available).</li>
</ul></li>
<li>Streams:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73496">#73496</a> (Invalid memory access in zend_inline_hash_func).</li>
  <li>Fixed bug <a href="http://bugs.php.net/74090">#74090</a> (stream_get_contents maxlength&gt;-1 returns empty string).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.16"><!-- {{{ 7.0.16 -->
<h3>Version 7.0.16</h3>
<b><time class='releasedate' datetime='2017-02-16'>16 Feb 2017</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73916">#73916</a> (zend_print_flat_zval_r doesn't consider reference).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73876">#73876</a> (Crash when exporting **= in expansion of assign op).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73969">#73969</a> (segfault in debug_print_backtrace).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73973">#73973</a> (assertion error in debug_zval_dump).</li>
</ul></li>
<li>DOM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/54382">#54382</a> (getAttributeNodeNS doesn't get xmlns* attributes).</li>
</ul></li>
<li>DTrace:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73965">#73965</a> (DTrace reported as enabled when disabled).</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/67583">#67583</a> (double fastcgi_end_request on max_children limit).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69865">#69865</a> (php-fpm does not close stderr when using syslog).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73968">#73968</a> (Premature failing of XBM reading).</li>
</ul></li>
<li>GMP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69993">#69993</a> (test for gmp.h needs to test machine includes).</li>
</ul></li>
<li>Intl:
<ul>
<li>Fixed bug <a href="http://bugs.php.net/73956">#73956</a> (Link use CC instead of CXX).</li>
</ul></li>
<li>LDAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73933">#73933</a> (error/segfault with ldap_mod_replace and opcache).</li>
</ul></li>
<li>MySQLi:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73949">#73949</a> (leak in mysqli_fetch_object).</li>
</ul></li>
<li>Mysqlnd:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69899">#69899</a> (segfault on close() after free_result() with mysqlnd).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73983">#73983</a> (crash on finish work with phar in cli + opcache).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71519">#71519</a> (add serial hex to return value array).</li>
</ul></li>
<li>PDO_Firebird:
<ul>
  <li>Implemented FR <a href="http://bugs.php.net/72583">#72583</a> (All data are fetched as strings).</li>
</ul></li>
<li>PDO_PgSQL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73959">#73959</a> (lastInsertId fails to throw an exception for wrong sequence name).</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70417">#70417</a> (PharData::compress() doesn't close temp file).</li>
</ul></li>
<li>posix:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71219">#71219</a> (configure script incorrectly checks for ttyname_r).</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69582">#69582</a> (session not readable by root in CLI).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73896">#73896</a> (spl_autoload() crashes when calls magic _call()).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69442">#69442</a> (closing of fd incorrect when PTS enabled).</li>
  <li>Fixed bug <a href="http://bugs.php.net/47021">#47021</a> (SoapClient stumbles over WSDL delivered with "Transfer-Encoding: chunked").</li>
  <li>Fixed bug <a href="http://bugs.php.net/72974">#72974</a> (imap is undefined service on AIX).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72979">#72979</a> (money_format stores wrong length AIX).</li>
</ul></li>
<li>ZIP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70103">#70103</a> (ZipArchive::addGlob ignores remove_all_path option).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.15"><!-- {{{ 7.0.15 -->
<h3>Version 7.0.15</h3>
<b><time class='releasedate' datetime='2017-01-19'>19 Jan 2017</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73792">#73792</a> (invalid foreach loop hangs script).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73663">#73663</a> ("Invalid opcode 65/16/8" occurs with a variable created with list()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73585">#73585</a> (Logging of "Internal Zend error - Missing class information" missing class name).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73753">#73753</a> (unserialized array pointer not advancing).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73825">#73825</a> (Heap out of bounds read on unserialize in finish_nested_data()). (CVE-2016-10161)</li>
  <li>Fixed bug <a href="http://bugs.php.net/73831">#73831</a> (NULL Pointer Dereference while unserialize php object). (CVE-2016-10162)</li>
  <li>Fixed bug <a href="http://bugs.php.net/73832">#73832</a> (Use of uninitialized memory in unserialize()). (CVE-2017-5340)</li>
  <li>Fixed bug <a href="http://bugs.php.net/73092">#73092</a> (Unserialize use-after-free when resizing object's properties hash table). (CVE-2016-7479)</li>
  <li>Fixed bug <a href="http://bugs.php.net/69425">#69425</a> (Use After Free in unserialize()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72731">#72731</a> (Type Confusion in Object Deserialization).</li>
</ul></li>
<li>COM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73679">#73679</a> (DOTNET read access violation using invalid codepage).</li>
</ul></li>
<li>DOM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/67474">#67474</a> (getElementsByTagNameNS filter on default ns).</li>
</ul></li>
<li>EXIF:
<ul>
<li>Fixed bug <a href="http://bugs.php.net/73737">#73737</a> (FPE when parsing a tag format). (CVE-2016-10158)</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73869">#73869</a> (Signed Integer Overflow gd_io.c). (CVE-2016-10168)</li>
  <li>Fixed bug <a href="http://bugs.php.net/73868">#73868</a> (DOS vulnerability in gdImageCreateFromGd2Ctx()). (CVE-2016-10167)</li>
</ul></li>
<li>GMP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70513">#70513</a> (GMP Deserialization Type Confusion Vulnerability).</li>
</ul></li>
<li>Mysqli:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73462">#73462</a> (Persistent connections don't set $connect_errno).</li>
</ul></li>
<li>Mysqlnd:
<ul>
  <li>Fixed issue with decoding BIT columns when having more than one rows in the result set. 7.0+ problem.</li>
  <li>Fixed bug <a href="http://bugs.php.net/73800">#73800</a> (sporadic segfault with MYSQLI_OPT_INT_AND_FLOAT_NATIVE).</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73612">#73612</a> (preg_*() may leak memory).</li>
</ul></li>
<li>PDO_Firebird:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72931">#72931</a> (PDO_FIREBIRD with Firebird 3.0 not work on returning statement).</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73773">#73773</a> (Seg fault when loading hostile phar). (CVE-2017-11147)</li>
  <li>Fixed bug <a href="http://bugs.php.net/73768">#73768</a> (Memory corruption when loading hostile phar). (CVE-2016-10160)</li>
  <li>Fixed bug <a href="http://bugs.php.net/73764">#73764</a> (Crash while loading hostile phar archive). (CVE-2016-10159)</li>
</ul></li>
<li>Phpdbg:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73615">#73615</a> (phpdbg without option never load .phpdbginit at startup).</li>
  <li>Fixed issue getting executable lines from custom wrappers.</li>
  <li>Fixed bug <a href="http://bugs.php.net/73704">#73704</a> (phpdbg shows the wrong line in files with shebang).</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/46103">#46103</a> (ReflectionObject memory leak).</li>
</ul></li>
<li>Streams:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73586">#73586</a> (php_user_filter::$stream is not set to the stream the filter is working on).</li>
</ul></li>
<li>SQLite3:
<ul>
<li>Reverted fix for <a href="http://bugs.php.net/73530">#73530</a> (Unsetting result set may reset other result set).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73594">#73594</a> (dns_get_record does not populate $additional out parameter).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70213">#70213</a> (Unserialize context shared on double class lookup).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73154">#73154</a> (serialize object with __sleep function crash).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70490">#70490</a> (get_browser function is very slow).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73265">#73265</a> (Loading browscap.ini at startup causes high memory usage).</li>
  <li>Fixed bug <a href="http://bugs.php.net/31875">#31875</a> (get_defined_functions additional param to exclude disabled functions).</li>
</ul></li>
<li>Zlib:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73373">#73373</a> (deflate_add does not verify that output was not truncated).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.14"><!-- {{{ 7.0.14 -->
<h3>Version 7.0.14</h3>
<b><time class='releasedate' datetime='2016-12-08'>08 Dec 2016</time></b>
<ul><li>Core:
<ul>
  <li>Fixed memory leak(null coalescing operator with Spl hash).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72736">#72736</a> (Slow performance when fetching large dataset with mysqli / PDO).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72978">#72978</a> (Use After Free Vulnerability in unserialize()). (CVE-2016-9936)</li>
</ul></li>
<li>Calendar:
<ul>
  <li>(Fix integer overflows).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69587">#69587</a> (DateInterval properties and isset).</li>
</ul></li>
<li>DTrace:
<ul>
  <li>Disabled PHP call tracing by default (it makes significant overhead). This may be enabled again using envirionment variable USE_ZEND_DTRACE=1.</li>
</ul></li>
<li>JSON:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73526">#73526</a> (php_json_encode depth issue).</li>
</ul></li>
<li>Mysqlnd:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/64526">#64526</a> (Add missing mysqlnd.* parameters to php.ini-*).</li>
</ul></li>
<li>ODBC:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73448">#73448</a> (odbc_errormsg returns trash, always 513 bytes).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69090">#69090</a> (check cached files permissions).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73546">#73546</a> (Logging for opcache has an empty file name).</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73483">#73483</a> (Segmentation fault on pcre_replace_callback).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73392">#73392</a> (A use-after-free in zend allocator management).</li>
</ul></li>
<li>PDO_Firebird:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73087">#73087</a>, <a href="http://bugs.php.net/61183">#61183</a>, <a href="http://bugs.php.net/71494">#71494</a> (Memory corruption in bindParam).</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73580">#73580</a> (Phar::isValidPharFilename illegal memory access).</li>
</ul></li>
<li>Postgres:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73498">#73498</a> (Incorrect SQL generated for pg_copy_to()).</li>
</ul></li>
<li>Soap:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73538">#73538</a> (SoapClient::__setSoapHeaders doesn't overwrite SOAP headers).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73452">#73452</a> (Segfault (Regression for <a href="http://bugs.php.net/69152">#69152</a>)).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73423">#73423</a> (Reproducible crash with GDB backtrace).</li>
</ul></li>
<li>SQLite3:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73530">#73530</a> (Unsetting result set may reset other result set).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73297">#73297</a> (HTTP stream wrapper should ignore HTTP 100 Continue).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73645">#73645</a> (version_compare illegal write access).</li>
</ul></li>
<li>Wddx:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73631">#73631</a> (Invalid read when wddx decodes empty boolean element). (CVE-2016-9935)</li>
</ul></li>
<li>XML:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72135">#72135</a> (malformed XML causes fault).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.13"><!-- {{{ 7.0.13 -->
<h3>Version 7.0.13</h3>
<b><time class='releasedate' datetime='2016-11-10'>10 Nov 2016</time></b>
<ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73350">#73350</a> (Exception::__toString() cause circular references).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73181">#73181</a> (parse_str() without a second argument leads to crash).</li>
  <li>Fixed bug <a href="http://bugs.php.net/66773">#66773</a> (Autoload with Opcache allows importing conflicting class name to namespace).</li>
  <li>Fixed bug <a href="http://bugs.php.net/66862">#66862</a> ((Sub-)Namespaces unexpected behaviour).</li>
  <li>Fix pthreads detection when cross-compiling.</li>
  <li>Fixed bug <a href="http://bugs.php.net/73337">#73337</a> (try/catch not working with two exceptions inside a same operation).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73338">#73338</a> (Exception thrown from error handler causes valgrind warnings (and crashes)).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73329">#73329</a> ((Float)"Nano" == NAN).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73213">#73213</a> (Integer overflow in imageline() with antialiasing).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73272">#73272</a> (imagescale() is not affected by, but affects imagesetinterpolation()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73279">#73279</a> (Integer overflow in gdImageScaleBilinearPalette()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73280">#73280</a> (Stack Buffer Overflow in GD dynamicGetbuf).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72482">#72482</a> (Ilegal write/read access caused by gdImageAALine overflow).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72696">#72696</a> (imagefilltoborder stackoverflow on truecolor images). (CVE-2016-9933)</li>
</ul></li>
<li>IMAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73418">#73418</a> (Integer Overflow in "_php_imap_mail" leads to crash).</li>
</ul></li>
<li>OCI8:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71148">#71148</a> (Bind reference overwritten on PHP 7).</li>
</ul></li>
<li>phpdbg:
<ul>
  <li>Properly allow for stdin input from a file.</li>
  <li>Add -s command line option / stdin command for reading script from stdin.</li>
  <li>Ignore non-executable opcodes in line mode of phpdbg_end_oplog().</li>
  <li>Fixed bug <a href="http://bugs.php.net/70776">#70776</a> (Simple SIGINT does not have any effect with -rr).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71234">#71234</a> (INI files are loaded even invoked as -n --version).</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73273">#73273</a> (session_unset() empties values from all variables in which is $_session stored).</li>
</ul></li>
<li>SOAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73037">#73037</a> (SoapServer reports Bad Request when gzipped).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73237">#73237</a> (Nested object in "any" element overwrites other fields).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69137">#69137</a> (Peer verification fails when using a proxy with SoapClient)</li>
</ul></li>
<li>SQLite3:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73333">#73333</a> (2147483647 is fetched as string).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73203">#73203</a> (passing additional_parameters causes mail to fail).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71241">#71241</a> (array_replace_recursive sometimes mutates its parameters).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73192">#73192</a> (parse_url return wrong hostname).</li>
</ul></li>
<li>Wddx:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73331">#73331</a> (NULL Pointer Dereference in WDDX Packet Deserialization with PDORow). (CVE-2016-9934)</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.12"><!-- {{{ 7.0.12 -->
<h3>Version 7.0.12</h3>
<time class='releasedate' datetime='2016-10-13'>13 Oct 2016</time><ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73025">#73025</a> (Heap Buffer Overflow in virtual_popen of zend_virtual_cwd.c).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72703">#72703</a> (Out of bounds global memory read in BF_crypt triggered by password_verify).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73058">#73058</a> (crypt broken when salt is 'too' long).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69579">#69579</a> (Invalid free in extension trait).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73156">#73156</a> (segfault on undefined function).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73163">#73163</a> (PHP hangs if error handler throws while accessing undef const in default value).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73172">#73172</a> (parse error: Invalid numeric literal).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73240">#73240</a> (Write out of bounds at number_format).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73147">#73147</a> (Use After Free in PHP7 unserialize()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73189">#73189</a> (Memcpy negative size parameter php_resolve_path).</li>
</ul></li>
<li>BCmath:
<ul>
<li>Fixed bug <a href="http://bugs.php.net/73190">#73190</a> (memcpy negative parameter _bc_new_num_ex).</li>
</ul></li>
<li>COM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73126">#73126</a> (Cannot pass parameter 1 by reference).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73091">#73091</a> (Unserializing DateInterval object may lead to __toString invocation).</li>
</ul></li>
<li>DOM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73150">#73150</a> (missing NULL check in dom_document_save_html).</li>
</ul></li>
<li>Filter:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72972">#72972</a> (Bad filter for the flags FILTER_FLAG_NO_RES_RANGE and FILTER_FLAG_NO_PRIV_RANGE).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73054">#73054</a> (default option ignored when object passed to int filter).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/67325">#67325</a> (imagetruecolortopalette: white is duplicated in palette).</li>
  <li>Fixed bug <a href="http://bugs.php.net/50194">#50194</a> (imagettftext broken on transparent background w/o alphablending).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73003">#73003</a> (Integer Overflow in gdImageWebpCtx of gd_webp.c).</li>
  <li>Fixed bug <a href="http://bugs.php.net/53504">#53504</a> (imagettfbbox gives incorrect values for bounding box).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73157">#73157</a> (imagegd2() ignores 3rd param if 4 are given).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73155">#73155</a> (imagegd2() writes wrong chunk sizes on boundaries).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73159">#73159</a> (imagegd2(): unrecognized formats may result in corrupted files).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73161">#73161</a> (imagecreatefromgd2() may leak memory).</li>
</ul></li>
<li>Intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73218">#73218</a> (add mitigation for ICU int overflow).</li>
</ul></li>
<li>Mbstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/66797">#66797</a> (mb_substr only takes 32-bit signed integer).</li>
  <li>Fixed bug <a href="http://bugs.php.net/66964">#66964</a> (mb_convert_variables() cannot detect recursion).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72992">#72992</a> (mbstring.internal_encoding doesn't inherit default_charset).</li>
</ul></li>
<li>Mysqlnd:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72489">#72489</a> (PHP Crashes When Modifying Array Containing MySQLi Result Data).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72982">#72982</a> (Memory leak in zend_accel_blacklist_update_regexp() function).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73072">#73072</a> (Invalid path SNI_server_certs causes segfault).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73276">#73276</a> (crash in openssl_random_pseudo_bytes function).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73275">#73275</a> (crash in openssl_encrypt function).</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73121">#73121</a> (Bundled PCRE doesn't compile because JIT isn't supported on s390).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73174">#73174</a> (heap overflow in php_pcre_replace_impl).</li>
</ul></li>
<li>PDO_DBlib:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72414">#72414</a> (Never quote values as raw binary data).</li>
  <li>Allow \PDO::setAttribute() to set query timeouts.</li>
  <li>Handle SQLDECIMAL/SQLNUMERIC types, which are used by later TDS versions.</li>
  <li>Add common PDO test suite.</li>
  <li>Free error and message strings when cleaning up PDO instances.</li>
  <li>Fixed bug <a href="http://bugs.php.net/67130">#67130</a> (\PDOStatement::nextRowset() should succeed when all rows in current rowset haven't been fetched).</li>
  <li>Ignore potentially misleading dberr values.</li>
</ul></li>
<li>phpdbg:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72996">#72996</a> (phpdbg_prompt.c undefined reference to DL_LOAD).</li>
  <li>Fixed next command not stopping when leaving function.</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/68015">#68015</a> (Session does not report invalid uid for files save handler).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73100">#73100</a> (session_destroy null dereference in ps_files_path_create).</li>
</ul></li>
<li>SimpleXML:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73293">#73293</a> (NULL pointer dereference in SimpleXMLElement::asXML()).</li>
</ul></li>
<li>SOAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71711">#71711</a> (Soap Server Member variables reference bug).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71996">#71996</a> (Using references in arrays doesn't work like expected).</li>
</ul></li>
<li>SPL:
<ul>
<li>Fixed bug <a href="http://bugs.php.net/73257">#73257</a>, Fixed bug <a href="http://bugs.php.net/73258">#73258</a> (SplObjectStorage unserialize allows use of non-object as key).</li>
</ul></li>
<li>SQLite3:
<ul>
  <li>Updated bundled SQLite3 to 3.14.2.</li>
</ul></li>
<li>Zip:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70752">#70752</a> (Depacking with wrong password leaves 0 length files).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.11"><!-- {{{ 7.0.11 -->
<h3>Version 7.0.11</h3>
<time class='releasedate' datetime='2016-09-15'>15 Sep 2016</time><ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72944">#72944</a> (Null pointer deref in zval_delref_p).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72943">#72943</a> (assign_dim on string doesn't reset hval).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72911">#72911</a> (Memleak in zend_binary_assign_op_obj_helper).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72813">#72813</a> (Segfault with __get returned by ref).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72767">#72767</a> (PHP Segfaults when trying to expand an infinite operator).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72854">#72854</a> (PHP Crashes on duplicate destructor call).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72857">#72857</a> (stream_socket_recvfrom read access violation).</li>
</ul></li>
<li>COM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72922">#72922</a> (COM called from PHP does not return out parameters).</li>
</ul></li>
<li>Dba:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70825">#70825</a> (Cannot fetch multiple values with group in ini file).</li>
</ul></li>
<li>FTP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70195">#70195</a> (Cannot upload file using ftp_put to FTPES with require_ssl_reuse).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72709">#72709</a> (imagesetstyle() causes OOB read for empty $styles).</li>
  <li>Fixed bug <a href="http://bugs.php.net/66005">#66005</a> (imagecopy does not support 1bit transparency on truecolor images).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72913">#72913</a> (imagecopy() loses single-color transparency on palette images).</li>
  <li>Fixed bug <a href="http://bugs.php.net/68716">#68716</a> (possible resource leaks in _php_image_convert()).</li>
</ul></li>
<li>iconv:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72320">#72320</a> (iconv_substr returns false for empty strings).</li>
</ul></li>
<li>IMAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72852">#72852</a> (imap_mail null dereference).</li>
</ul></li>
<li>Intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/65732">#65732</a> (grapheme_*() is not Unicode compliant on CR LF sequence).</li>
  <li>Fixed bug <a href="http://bugs.php.net/73007">#73007</a> (add locale length check). (CVE-2016-7416)</li>
</ul></li>
<li>Mysqlnd:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72293">#72293</a> (Heap overflow in mysqlnd related to BIT fields). (CVE-2016-7412)</li>
</ul></li>
<li>OCI8:
<ul>
  <li>Fixed invalid handle error with Implicit Result Sets.</li>
  <li>Fixed bug <a href="http://bugs.php.net/72524">#72524</a> (Binding null values triggers ORA-24816 error).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72949">#72949</a> (Typo in opcache error message).</li>
</ul></li>
<li>PDO:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72788">#72788</a> (Invalid memory access when using persistent PDO connection).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72791">#72791</a> (Memory leak in PDO persistent connection handling).</li>
  <li>Fixed bug <a href="http://bugs.php.net/60665">#60665</a> (call to empty() on NULL result using PDO::FETCH_LAZY returns false).</li>
</ul></li>
<li>PDO_DBlib:
<ul>
  <li>Implemented stringify 'uniqueidentifier' fields.</li>
</ul></li>
<li>PDO_pgsql:
<ul>
  <li>Implemented FR <a href="http://bugs.php.net/72633">#72633</a> (Postgres PDO lastInsertId() should work without specifying a sequence).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72759">#72759</a> (Regression in pgo_pgsql).</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72928">#72928</a> (Out of bound when verify signature of zip phar in phar_parse_zipfile). (CVE-2016-7414)</li>
  <li>Fixed bug <a href="http://bugs.php.net/73035">#73035</a> (Out of bound when verify signature of tar phar in phar_parse_tarfile).</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72846">#72846</a> (getConstant for a array constant with constant values returns NULL/NFC/UKNOWN).</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72724">#72724</a> (PHP7: session-uploadprogress kills httpd).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72940">#72940</a> (SID always return "name=ID", even if session cookie exist).</li>
</ul></li>
<li>SimpleXML:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72971">#72971</a> (SimpleXML isset/unset do not respect namespace).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72957">#72957</a> (Null coalescing operator doesn't behave as expected with SimpleXMLElement).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/73029">#73029</a> (Missing type check when unserializing SplArray). (CVE-2016-7417)</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/55451">#55451</a> (substr_compare NULL length interpreted as 0).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72278">#72278</a> (getimagesize returning FALSE on valid jpg).</li>
  <li>Fixed bug <a href="http://bugs.php.net/65550">#65550</a> (get_browser() incorrectly parses entries with "+" sign).</li>
</ul></li>
<li>Streams:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72853">#72853</a> (stream_set_blocking doesn't work).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72764">#72764</a> (ftps:// opendir wrapper data channel encryption fails with IIS FTP 7.5, 8.5).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71882">#71882</a> (Negative ftruncate() on php://memory exhausts memory).</li>
</ul></li>
<li>SQLite3:
<ul>
<li>Downgraded bundled SQLite to 3.8.10.2, see <a href="http://bugs.php.net/73068">#73068</a></li>
</ul></li>
<li>Sysvshm:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72858">#72858</a> (shm_attach null dereference).</li>
</ul></li>
<li>Wddx:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72860">#72860</a> (wddx_deserialize use-after-free). (CVE-2016-7413)</li>
  <li>Fixed bug <a href="http://bugs.php.net/73065">#73065</a> (Out-Of-Bounds Read in php_wddx_push_element). (CVE-2016-7418)</li>
</ul></li>
<li>XML:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72085">#72085</a> (SEGV on unknown address zif_xml_parse).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72714">#72714</a> (_xml_startElementHandler() segmentation fault).</li>
</ul></li>
<li>ZIP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/68302">#68302</a> (impossible to compile php with zip support).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.10"><!-- {{{ 7.0.10 -->
<h3>Version 7.0.10</h3>
<time class='releasedate' datetime='2016-08-18'>18 Aug 2016</time><ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72629">#72629</a> (Caught exception assignment to variables ignores references).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72594">#72594</a> (Calling an earlier instance of an included anonymous class fatals).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72581">#72581</a> (previous property undefined in Exception after deserialization).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72496">#72496</a> (Cannot declare public method with signature incompatible with parent private method).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72024">#72024</a> (microtime() leaks memory).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71911">#71911</a> (Unable to set --enable-debug on building extensions by phpize on Windows).</li>
  <li>Fixed bug causing ClosedGeneratorException being thrown into the calling code instead of the Generator yielding from.</li>
  <li>Implemented FR <a href="http://bugs.php.net/72614">#72614</a> (Support "nmake test" on building extensions by phpize).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72641">#72641</a> (phpize (on Windows) ignores PHP_PREFIX).</li>
  <li>Fixed potential segfault in object storage freeing in shutdown sequence.</li>
  <li>Fixed bug <a href="http://bugs.php.net/72663">#72663</a> (Create an Unexpected Object and Don't Invoke __wakeup() in Deserialization). (CVE-2016-7124)</li>
  <li>Fixed bug <a href="http://bugs.php.net/72681">#72681</a> (PHP Session Data Injection Vulnerability). (CVE-2016-7125)</li>
  <li>Fixed bug <a href="http://bugs.php.net/72683">#72683</a> (getmxrr broken).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72742">#72742</a> (memory allocator fails to realloc small block to large one). (CVE-2016-7133)</li>
</ul></li>
<li>Bz2:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72837">#72837</a> (integer overflow in bzdecompress caused heap corruption).</li>
</ul></li>
<li>Calendar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/67976">#67976</a> (cal_days_month() fails for final month of the French calendar).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71894">#71894</a> (AddressSanitizer: global-buffer-overflow in zif_cal_from_jd).</li>
</ul></li>
<li>COM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72569">#72569</a> (DOTNET/COM array parameters broke in PHP7).</li>
</ul></li>
<li>CURL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71709">#71709</a> (curl_setopt segfault with empty CURLOPT_HTTPHEADER).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71929">#71929</a> (CURLINFO_CERTINFO data parsing error).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72674">#72674</a> (Heap overflow in curl_escape). (CVE-2016-7134)</li>
</ul></li>
<li>DOM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/66502">#66502</a> (DOM document dangling reference).</li>
</ul></li>
<li>EXIF:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72735">#72735</a> (Samsung picture thumb not read (zero size)).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72627">#72627</a> (Memory Leakage In exif_process_IFD_in_TIFF). (CVE-2016-7128)</li>
</ul></li>
<li>Filter:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71745">#71745</a> (FILTER_FLAG_NO_RES_RANGE does not cover whole 127.0.0.0/8 range).</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72575">#72575</a> (using --allow-to-run-as-root should ignore missing user).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72596">#72596</a> (imagetypes function won't advertise WEBP support).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72604">#72604</a> (imagearc() ignores thickness for full arcs).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70315">#70315</a> (500 Server Error but page is fully rendered).</li>
  <li>Fixed bug <a href="http://bugs.php.net/43828">#43828</a> (broken transparency of imagearc for truecolor in blendingmode).</li>
  <li>Fixed bug <a href="http://bugs.php.net/66555">#66555</a> (Always false condition in ext/gd/libgd/gdkanji.c).</li>
  <li>Fixed bug <a href="http://bugs.php.net/68712">#68712</a> (suspicious if-else statements).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72697">#72697</a> (select_colors write out-of-bounds). (CVE-2016-7126)</li>
  <li>Fixed bug <a href="http://bugs.php.net/72730">#72730</a> (imagegammacorrect allows arbitrary write access). (CVE-2016-7127)</li>
  <li>Fixed bug <a href="http://bugs.php.net/72494">#72494</a> (imagecropauto out-of-bounds access)</li>
</ul></li>
<li>Intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72639">#72639</a> (Segfault when instantiating class that extends IntlCalendar and adds a property).</li>
  <li>Partially fixed Fixed bug <a href="http://bugs.php.net/72506">#72506</a> (idn_to_ascii for UTS #46 incorrect for long domain names).</li>
</ul></li>
<li>mbstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72691">#72691</a> (mb_ereg_search raises a warning if a match zero-width).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72693">#72693</a> (mb_ereg_search increments search position when a match zero-width).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72694">#72694</a> (mb_ereg_search_setpos does not accept a string's last position).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72710">#72710</a> (`mb_ereg` causes buffer overflow on regexp compile error).</li>
</ul></li>
<li>Mcrypt:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72782">#72782</a> (Heap Overflow due to integer overflows).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72590">#72590</a> (Opcache restart with kill_all_lockers does not work).</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72688">#72688</a> (preg_match missing group names in matches).</li>
</ul></li>
<li>PDO_pgsql:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70313">#70313</a> (PDO statement fails to throw exception).</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72222">#72222</a> (ReflectionClass::export doesn't handle array constants).</li>
</ul></li>
<li>SimpleXML:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72588">#72588</a> (Using global var doesn't work while accessing SimpleXML element).</li>
</ul></li>
<li>SNMP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72708">#72708</a> (php_snmp_parse_oid integer overflow in memory allocation).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/55701">#55701</a> (GlobIterator throws LogicException).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72646">#72646</a> (SplFileObject::getCsvControl does not return the escape character).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72684">#72684</a> (AppendIterator segfault with closed generator).</li>
</ul></li>
<li>SQLite3:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72668">#72668</a> (Spurious warning when exception is thrown in user defined function).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72571">#72571</a> (SQLite3::bindValue, SQLite3::bindParam crash).</li>
  <li>Implemented FR <a href="http://bugs.php.net/72653">#72653</a> (SQLite should allow opening with empty filename).</li>
  <li>Updated to SQLite3 3.13.0.</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72622">#72622</a> (array_walk + array_replace_recursive create references from nothing).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72152">#72152</a> (base64_decode $strict fails to detect null byte).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72263">#72263</a> (base64_decode skips a character after padding in strict mode).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72264">#72264</a> (base64_decode $strict fails with whitespace between padding).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72330">#72330</a> (CSV fields incorrectly split if escape char followed by UTF chars).</li>
</ul></li>
<li>Streams:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/41021">#41021</a> (Problems with the ftps wrapper).</li>
  <li>Fixed bug <a href="http://bugs.php.net/54431">#54431</a> (opendir() does not work with ftps:// wrapper).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72667">#72667</a> (opendir() with ftp:// attempts to open data stream for non-existent directories).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72771">#72771</a> (ftps:// wrapper is vulnerable to protocol downgrade attack).</li>
</ul></li>
<li>XMLRPC:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72647">#72647</a> (xmlrpc_encode() unexpected output after referencing array elements).</li>
</ul></li>
<li>Wddx:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72564">#72564</a> (boolean always deserialized as "true").</li>
  <li>Fixed bug <a href="http://bugs.php.net/72142">#72142</a> (WDDX Packet Injection Vulnerability in wddx_serialize_value()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72749">#72749</a> (wddx_deserialize allows illegal memory access). (CVE-2016-7129)</li>
  <li>Fixed bug <a href="http://bugs.php.net/72750">#72750</a> (wddx_deserialize null dereference). (CVE-2016-7130)</li>
  <li>Fixed bug <a href="http://bugs.php.net/72790">#72790</a> (wddx_deserialize null dereference with invalid xml). (CVE-2016-7131)</li>
  <li>Fixed bug <a href="http://bugs.php.net/72799">#72799</a> (wddx_deserialize null dereference in php_wddx_pop_element). (CVE-2016-7132)</li>
</ul></li>
<li>Zip:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72660">#72660</a> (NULL Pointer dereference in zend_virtual_cwd).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.9"><!-- {{{ 7.0.9 -->
<h3>Version 7.0.9</h3>
<time class='releasedate' datetime='2016-07-21'>21 Jul 2016</time><ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72508">#72508</a> (strange references after recursive function call and "switch" statement).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72513">#72513</a> (Stack-based buffer overflow vulnerability in virtual_file_ex). (CVE-2016-6289)</li>
  <li>Fixed bug <a href="http://bugs.php.net/72573">#72573</a> (HTTP_PROXY is improperly trusted by some PHP libraries and applications). (CVE-2016-5385)</li>
</ul></li>
<li>bz2:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72613">#72613</a> (Inadequate error handling in bzread()). (CVE-2016-5399)</li>
</ul></li>
<li>CLI:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72484">#72484</a> (SCRIPT_FILENAME shows wrong path if the user specify router.php).</li>
</ul></li>
<li>COM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72498">#72498</a> (variant_date_from_timestamp null dereference).</li>
</ul></li>
<li>Curl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72541">#72541</a> (size_t overflow lead to heap corruption).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/66836">#66836</a> (DateTime::createFromFormat 'U' with pre 1970 dates fails parsing).</li>
</ul></li>
<li>Exif:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72603">#72603</a> (Out of bound read in exif_process_IFD_in_MAKERNOTE). (CVE-2016-6291)</li>
  <li>Fixed bug <a href="http://bugs.php.net/72618">#72618</a> (NULL Pointer Dereference in exif_process_user_comment). (CVE-2016-6292)</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/43475">#43475</a> (Thick styled lines have scrambled patterns).</li>
  <li>Fixed bug <a href="http://bugs.php.net/53640">#53640</a> (XBM images require width to be multiple of 8).</li>
  <li>Fixed bug <a href="http://bugs.php.net/64641">#64641</a> (imagefilledpolygon doesn't draw horizontal line).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72512">#72512</a> (gdImageTrueColorToPaletteBody allows arbitrary write/read access).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72519">#72519</a> (imagegif/output out-of-bounds access).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72558">#72558</a> (Integer overflow error within _gdContributionsAlloc()). (CVE-2016-6207)</li>
  <li>Fixed bug <a href="http://bugs.php.net/72482">#72482</a> (Ilegal write/read access caused by gdImageAALine overflow).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72494">#72494</a> (imagecropauto out-of-bounds access).</li>
</ul></li>
<li>Intl:
<ul>

  <li>Fixed bug <a href="http://bugs.php.net/72533">#72533</a> (locale_accept_from_http out-of-bounds access). (CVE-2016-6294)</li>
</ul></li>
<li>Mbstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72405">#72405</a> (mb_ereg_replace - mbc_to_code (oniguruma) - oob read access).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72399">#72399</a> (Use-After-Free in MBString (search_re)).</li>
</ul></li>
<li>mcrypt:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72551">#72551</a>, bug <a href="http://bugs.php.net/72552">#72552</a> (Incorrect casting from size_t to int lead to heap overflow in mdecrypt_generic).</li>
</ul></li>
<li>PDO_pgsql:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72570">#72570</a> (Segmentation fault when binding parameters on a query without placeholders).</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72476">#72476</a> (Memleak in jit_stack).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72463">#72463</a> (mail fails with invalid argument).</li>
</ul></li>
<li>Readline:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72538">#72538</a> (readline_redisplay crashes php).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72505">#72505</a> (readfile() mangles files larger than 2G).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72306">#72306</a> (Heap overflow through proc_open and $env parameter).</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72531">#72531</a> (ps_files_cleanup_dir Buffer overflow).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72562">#72562</a> (Use After Free in unserialize() with Unexpected Session Deserialization).</li>
</ul></li>
<li>SNMP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72479">#72479</a> (Use After Free Vulnerability in SNMP with GC and unserialize()). (CVE-2016-6295)</li>
</ul></li>
<li>Streams:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72439">#72439</a> (Stream socket with remote address leads to a segmentation fault).</li>
</ul></li>
<li>XMLRPC:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72606">#72606</a> (heap-buffer-overflow (write) simplestring_addn simplestring.c). (CVE-2016-6296)</li>
</ul></li>
<li>Zip:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72520">#72520</a> (Stack-based buffer overflow vulnerability in php_stream_zip_opener). (CVE-2016-6297)</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.8"><!-- {{{ 7.0.8 -->
<h3>Version 7.0.8</h3>
<time class='releasedate' datetime='2016-06-23'>23 Jun 2016</time><ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72218">#72218</a> (If host name cannot be resolved then PHP 7 crashes).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72221">#72221</a> (segfault, past-the-end access).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72268">#72268</a> (Integer Overflow in nl2br()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72275">#72275</a> (Integer Overflow in json_encode()/json_decode()/ json_utf8_to_utf16()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72400">#72400</a> (Integer Overflow in addcslashes/addslashes).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72403">#72403</a> (Integer Overflow in Length of String-typed ZVAL).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/63740">#63740</a> (strtotime seems to use both sunday and monday as start of week).</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72308">#72308</a> (fastcgi_finish_request and logging environment variables).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72298">#72298</a> (pass2_no_dither out-of-bounds access).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72337">#72337</a> (invalid dimensions can lead to crash).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72339">#72339</a> (Integer Overflow in _gd2GetHeader() resulting in heap overflow). (CVE-2016-5766)</li>
  <li>Fixed bug <a href="http://bugs.php.net/72407">#72407</a> (NULL Pointer Dereference at _gdScaleVert).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72446">#72446</a> (Integer Overflow in gdImagePaletteToTrueColor() resulting in heap overflow). (CVE-2016-5767)</li>
</ul></li>
<li>Intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70484">#70484</a> (selectordinal doesn't work with named parameters).</li>
</ul></li>
<li>mbstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72402">#72402</a> (_php_mb_regex_ereg_replace_exec - double free). (CVE-2016-5768)</li>
</ul></li>
<li>mcrypt:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72455">#72455</a> (Heap Overflow due to integer overflows). (CVE-2016-5769)</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72140">#72140</a> (segfault after calling ERR_free_strings()).</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72143">#72143</a> (preg_replace uses int instead of size_t).</li>
</ul></li>
<li>PDO_pgsql:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71573">#71573</a> (Segfault (core dumped) if paramno beyond bound).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72294">#72294</a> (Segmentation fault/invalid pointer in connection with pgsql_stmt_dtor).</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72321">#72321</a> (invalid free in phar_extract_file()). (CVE-2016-4473)</li>
</ul></li>
<li>Phpdbg:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72284">#72284</a> (phpdbg fatal errors with coverage).</li>
</ul></li>
<li>Postgres:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72195">#72195</a> (pg_pconnect/pg_connect cause use-after-free).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72197">#72197</a> (pg_lo_create arbitrary read).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72017">#72017</a> (range() with float step produces unexpected result).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72193">#72193</a> (dns_get_record returns array containing elements of type 'unknown').</li>
  <li>Fixed bug <a href="http://bugs.php.net/72229">#72229</a> (Wrong reference when serialize/unserialize an object).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72300">#72300</a> (ignore_user_abort(false) has no effect).</li>
</ul></li>
<li>WDDX:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72340">#72340</a> (Double Free Courruption in wddx_deserialize). (CVE-2016-5772)</li>
</ul></li>
<li>XML:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72206">#72206</a> (xml_parser_create/xml_parser_free leaks mem).</li>
</ul></li>
<li>XMLRPC:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72155">#72155</a> (use-after-free caused by get_zval_xmlrpc_type).</li>
</ul></li>
<li>Zip:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72258">#72258</a> (ZipArchive converts filenames to unrecoverable form).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72434">#72434</a> (ZipArchive class Use After Free Vulnerability in PHP's GC algorithm and unserialize). (CVE-2016-5773)</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.7"><!-- {{{ 7.0.7 -->
<h3>Version 7.0.7</h3>
<time class='releasedate' datetime='2016-05-26'>26 May 2016</time><ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72162">#72162</a> (use-after-free - error_reporting).</li>
  <li>Add compiler option to disable special case function calls.</li>
  <li>Fixed bug <a href="http://bugs.php.net/72101">#72101</a> (crash on complex code).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72100">#72100</a> (implode() inserts garbage into resulting string when joins very big integer).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72057">#72057</a> (PHP Hangs when using custom error handler and typehint).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72038">#72038</a> (Function calls with values to a by-ref parameter don't always throw a notice).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71737">#71737</a> (Memory leak in closure with parameter named $this).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72059">#72059</a> (?? is not allowed on constant expressions).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72159">#72159</a> (Imported Class Overrides Local Class Name).</li>
</ul></li>
<li>Curl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/68658">#68658</a> (Define CURLE_SSL_CACERT_BADFILE).</li>
</ul></li>
<li>DBA:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72157">#72157</a> (use-after-free caused by dba_open).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72227">#72227</a> (imagescale out-of-bounds read). (CVE-2013-7456)</li>
</ul></li>
<li>Intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/64524">#64524</a> (Add intl.use_exceptions to php.ini-*).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72241">#72241</a> (get_icu_value_internal out-of-bounds read). (CVE-2016-5093)</li>
</ul></li>
<li>JSON:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72069">#72069</a> (Behavior \JsonSerializable different from json_encode).</li>
</ul></li>
<li>Mbstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72164">#72164</a> (Null Pointer Dereference - mb_ereg_replace).</li>
</ul></li>
<li>OCI8:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71600">#71600</a> (oci_fetch_all segfaults when selecting more than eight columns).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72014">#72014</a> (Including a file with anonymous classes multiple times leads to fatal error).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72165">#72165</a> (Null pointer dereference - openssl_csr_new).</li>
</ul></li>
<li>PCNTL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72154">#72154</a> (pcntl_wait/pcntl_waitpid array internal structure overwrite).</li>
</ul></li>
<li>POSIX:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72133">#72133</a> (php_posix_group_to_array crashes if gr_passwd is NULL).</li>
</ul></li>
<li>Postgres:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72028">#72028</a> (pg_query_params(): NULL converts to empty string).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71062">#71062</a> (pg_convert() doesn't accept ISO 8601 for datatype timestamp).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72151">#72151</a> (mysqli_fetch_object changed behaviour). Patch to <a href="http://bugs.php.net/71820">#71820</a> is reverted.</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72174">#72174</a> (ReflectionProperty#getValue() causes __isset call).</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71972">#71972</a> (Cyclic references causing session_start(): Failed to decode session object).</li>
</ul></li>
<li>Sockets:
<ul>
  <li>Added socket_export_stream() function for getting a stream compatible resource from a socket resource.</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72051">#72051</a> (The reference in CallbackFilterIterator doesn't work as expected).</li>
</ul></li>
<li>SQLite3:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/68849">#68849</a> (bindValue is not using the right data type).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72075">#72075</a> (Referencing socket resources breaks stream_select).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72031">#72031</a> (array_column() against an array of objects discards all values matching null).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.6"><!-- {{{ 7.0.6 -->
<h3>Version 7.0.6</h3>
<time class='releasedate' datetime='2016-04-28'>28 Apr 2016</time><ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71930">#71930</a> (_zval_dtor_func: Assertion `(arr)-&gt;gc.refcount &lt;= 1' failed).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71922">#71922</a> (Crash on assert(new class{})).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71914">#71914</a> (Reference is lost in "switch").</li>
  <li>Fixed bug <a href="http://bugs.php.net/71871">#71871</a> (Interfaces allow final and abstract functions).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71859">#71859</a> (zend_objects_store_call_destructors operates on realloced memory, crashing).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71841">#71841</a> (EG(error_zval) is not handled well).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71750">#71750</a> (Multiple Heap Overflows in php_raw_url_encode/ php_url_encode).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71731">#71731</a> (Null coalescing operator and ArrayAccess).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71609">#71609</a> (Segmentation fault on ZTS with gethostbyname).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71414">#71414</a> (Inheritance, traits and interfaces).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71359">#71359</a> (Null coalescing operator and magic).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71334">#71334</a> (Cannot access array keys while uksort()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69659">#69659</a> (ArrayAccess, isset() and the offsetExists method).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69537">#69537</a> (__debugInfo with empty string for key gives error).</li>
  <li>Fixed bug <a href="http://bugs.php.net/62059">#62059</a> (ArrayObject and isset are not friends).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71980">#71980</a> (Decorated/Nested Generator is Uncloseable in Finally).</li>
</ul></li>
<li>BCmath:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72093">#72093</a> (bcpowmod accepts negative scale and corrupts _one_ definition). (CVE-2016-4537, CVE-2016-4538)</li>
</ul></li>
<li>Curl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71831">#71831</a> (CURLOPT_NOPROXY applied as long instead of string).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71889">#71889</a> (DateInterval::format Segmentation fault).</li>
</ul></li>
<li>EXIF:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72094">#72094</a> (Out of bounds heap read access in exif header processing). (CVE-2016-4542, CVE-2016-4543, CVE-2016-4544)</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71912">#71912</a> (libgd: signedness vulnerability). (CVE-2016-3074)</li>
</ul></li>
<li>Intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71516">#71516</a> (IntlDateFormatter looses locale if pattern is set via constructor).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70455">#70455</a> (Missing constant: IntlChar::NO_NUMERIC_VALUE).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70451">#70451</a>, #70452 (Inconsistencies in return values of IntlChar methods).</li>
  <li>Fixed bug <a href="http://bugs.php.net/68893">#68893</a> (Stackoverflow in datefmt_create).</li>
  <li>Fixed bug <a href="http://bugs.php.net/66289">#66289</a> (Locale::lookup incorrectly returns en or en_US if locale is empty).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70484">#70484</a> (selectordinal doesn't work with named parameters).</li>
  <li>Fixed bug <a href="http://bugs.php.net/72061">#72061</a> (Out-of-bounds reads in zif_grapheme_stripos with negative offset). (CVE-2016-4540, CVE-2016-4541)</li>
</ul></li>
<li>ODBC:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/63171">#63171</a> (Script hangs after max_execution_time).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71843">#71843</a> (null ptr deref ZEND_RETURN_SPEC_CONST_HANDLER).</li>
</ul></li>
<li>PDO:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/52098">#52098</a> (Own PDOStatement implementation ignore __call()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71447">#71447</a> (Quotes inside comments not properly handled).</li>
</ul></li>
<li>PDO_DBlib:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71943">#71943</a> (dblib_handle_quoter needs to allocate an extra byte).</li>
  <li>Add DBLIB-specific attributes for controlling timeouts.</li>
</ul></li>
<li>PDO_pgsql:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/62498">#62498</a> (pdo_pgsql inefficient when getColumnMeta() is used).</li>
</ul></li>
<li>Postgres:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71820">#71820</a> (pg_fetch_object binds parameters before call constructor).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71998">#71998</a> (Function pg_insert does not insert when column type = inet).</li>
</ul></li>
<li>SOAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71986">#71986</a> (Nested foreach assign-by-reference creates broken variables).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71838">#71838</a> (Deserializing serialized SPLObjectStorage-Object can't access properties in PHP).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71735">#71735</a> (Double-free in SplDoublyLinkedList::offsetSet).</li>
  <li>Fixed bug <a href="http://bugs.php.net/67582">#67582</a> (Cloned SplObjectStorage with overwritten getHash fails offsetExists()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/52339">#52339</a> (SPL autoloader breaks class_exists()).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72116">#72116</a> (array_fill optimization breaks implementation).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71995">#71995</a> (Returning the same var twice from __sleep() produces broken serialized data).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71940">#71940</a> (Unserialize crushes on restore object reference).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71969">#71969</a> (str_replace returns an incorrect resulting array after a foreach by reference).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71891">#71891</a> (header_register_callback() and register_shutdown_function()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71884">#71884</a> (Null pointer deref (segfault) in stream_context_get_default).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71840">#71840</a> (Unserialize accepts wrongly data).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71837">#71837</a> (Wrong arrays behaviour).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71827">#71827</a> (substr_replace bug, string length).</li>
  <li>Fixed bug <a href="http://bugs.php.net/67512">#67512</a> (php_crypt() crashes if crypt_r() does not exist or _REENTRANT is not defined).</li>
</ul></li>
<li>XML:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/72099">#72099</a> (xml_parse_into_struct segmentation fault). (CVE-2016-4539)</li>
</ul></li>
<li>Zip:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71923">#71923</a> (integer overflow in ZipArchive::getFrom*). (CVE-2016-3078)</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.5"><!-- {{{ 7.0.5 -->
<h3>Version 7.0.5</h3>
<time class='releasedate' datetime='2016-03-31'>31 Mar 2016</time><ul><li>Core:
<ul>
  <li>Huge pages disabled by default.</li>
  <li>Added ability to enable huge pages in Zend Memory Manager through the environment variable USE_ZEND_ALLOC_HUGE_PAGES=1.</li>
  <li>Fixed bug <a href="http://bugs.php.net/71756">#71756</a> (Call-by-reference widens scope to uninvolved functions when used in switch).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71729">#71729</a> (Possible crash in zend_bin_strtod, zend_oct_strtod, zend_hex_strtod).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71695">#71695</a> (Global variables are reserved before execution).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71629">#71629</a> (Out-of-bounds access in php_url_decode in context php_stream_url_wrap_rfc2397).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71622">#71622</a> (Strings used in pass-as-reference cannot be used to invoke C::$callable()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71596">#71596</a> (Segmentation fault on ZTS with date function (setlocale)).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71535">#71535</a> (Integer overflow in zend_mm_alloc_heap()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71470">#71470</a> (Leaked 1 hashtable iterators).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71575">#71575</a> (ISO C does not allow extra &lsquo;;&rsquo; outside of a function).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71724">#71724</a> (yield from does not count EOLs).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71767">#71767</a> (ReflectionMethod::getDocComment returns the wrong comment).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71806">#71806</a> (php_strip_whitespace() fails on some numerical values).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71624">#71624</a> (`php -R` (PHP_MODE_PROCESS_STDIN) is broken).</li>
</ul></li>
<li>CLI Server:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69953">#69953</a> (Support MKCALENDAR request method).</li>
</ul></li>
<li>Curl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71694">#71694</a> (Support constant CURLM_ADDED_ALREADY).</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71635">#71635</a> (DatePeriod::getEndDate segfault).</li>
</ul></li>
<li>Fileinfo:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71527">#71527</a> (Buffer over-write in finfo_open with malformed magic file). (CVE-2015-8865)</li>
</ul></li>
<li>libxml:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71536">#71536</a> (Access Violation crashes php-cgi.exe).</li>
</ul></li>
<li>mbstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71906">#71906</a> (AddressSanitizer: negative-size-param (-1) in mbfl_strcut). (CVE-2016-4073)</li>
</ul></li>
<li>ODBC:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/47803">#47803</a>, #69526 (Executing prepared statements is succesfull only for the first two statements).</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71659">#71659</a> (segmentation fault in pcre running twig tests).</li>
</ul></li>
<li>PDO_DBlib:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/54648">#54648</a> (PDO::MSSQL forces format of datetime fields).</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71625">#71625</a> (Crash in php7.dll with bad phar filename).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71317">#71317</a> (PharData fails to open specific file).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71860">#71860</a> (Invalid memory write in phar on filename with \0 in name). (CVE-2016-4072)</li>
</ul></li>
<li>phpdbg:
<ul>
  <li>Fixed crash when advancing (except step) inside an internal function.</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71683">#71683</a> (Null pointer dereference in zend_hash_str_find_bucket).</li>
</ul></li>
<li>SNMP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71704">#71704</a> (php_snmp_error() Format String Vulnerability). (CVE-2016-4071)</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71617">#71617</a> (private properties lost when unserializing ArrayObject).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71660">#71660</a> (array_column behaves incorrectly after foreach by reference).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71798">#71798</a> (Integer Overflow in php_raw_url_encode). (CVE-2016-4070)</li>
</ul></li>
<li>Zip:
<ul>
  <li>Update bundled libzip to 1.1.2.</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.4"><!-- {{{ 7.0.4 -->
<h3>Version 7.0.4</h3>
<time class='releasedate' datetime='2016-03-03'>03 Mar 2016</time><ul><li>Core:
<ul>
  <li>Fixed bug (Low probability segfault in zend_arena).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71441">#71441</a> (Typehinted Generator with return in try/finally crashes).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71442">#71442</a> (forward_static_call crash).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71443">#71443</a> (Segfault using built-in webserver with intl using symfony).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71449">#71449</a> (An integer overflow bug in php_implode()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71450">#71450</a> (An integer overflow bug in php_str_to_str_ex()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71474">#71474</a> (Crash because of VM stack corruption on Magento2).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71485">#71485</a> (Return typehint on internal func causes Fatal error when it throws exception).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71529">#71529</a> (Variable references on array elements don't work when using count).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71601">#71601</a> (finally block not executed after yield from).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71637">#71637</a> (Multiple Heap Overflow due to integer overflows in xml/filter_url/addcslashes). (CVE-2016-4344, CVE-2016-4345, CVE-2016-4346)</li>
</ul></li>
<li>CLI server:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71559">#71559</a> (Built-in HTTP server, we can download file in web by bug).</li>
</ul></li>
<li>CURL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71523">#71523</a> (Copied handle with new option CURLOPT_HTTPHEADER crashes while curl_multi_exec).</li>
  <li>Fixed memory leak in curl_getinfo().</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71525">#71525</a> (Calls to date_modify will mutate timelib_rel_time, causing date_date_set issues).</li>
</ul></li>
<li>Fileinfo:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71434">#71434</a> (finfo throws notice for specific python file).</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/62172">#62172</a> (FPM not working with Apache httpd 2.4 balancer/fcgi setup).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71269">#71269</a> (php-fpm dumped core).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71584">#71584</a> (Possible use-after-free of ZCG(cwd) in Zend Opcache).</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71537">#71537</a> (PCRE segfault from Opcache).</li>
</ul></li>
<li>phpdbg:
<ul>
  <li>Fixed inherited functions from unspecified files being included in phpdbg_get_executable().</li>
</ul></li>
<li>SOAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71610">#71610</a> (Type Confusion Vulnerability - SOAP / make_http_soap_request()). (CVE-2016-3185)</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71603">#71603</a> (compact() maintains references in php7).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70720">#70720</a> (strip_tags improper php code parsing).</li>
</ul></li>
<li>XMLRPC:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71501">#71501</a> (xmlrpc_encode_request ignores encoding option).</li>
</ul></li>
<li>Zip:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71561">#71561</a> (NULL pointer dereference in Zip::ExtractTo).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.3"><!-- {{{ 7.0.3 -->
<h3>Version 7.0.3</h3>
<time class='releasedate' datetime='2016-02-04'>04 Feb 2016</time><ul><li>Core:
<ul>
  <li>Added support for new HTTP 451 code.</li>
  <li>Fixed bug <a href="http://bugs.php.net/71039">#71039</a> (exec functions ignore length but look for NULL termination).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71089">#71089</a> (No check to duplicate zend_extension).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71201">#71201</a> (round() segfault on 64-bit builds).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71221">#71221</a> (Null pointer deref (segfault) in get_defined_vars via ob_start).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71248">#71248</a> (Wrong interface is enforced).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71273">#71273</a> (A wrong ext directory setup in php.ini leads to crash).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71275">#71275</a> (Bad method called on cloning an object having a trait).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71297">#71297</a> (Memory leak with consecutive yield from).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71300">#71300</a> (Segfault in zend_fetch_string_offset).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71314">#71314</a> (var_export(INF) prints INF.0).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71323">#71323</a> (Output of stream_get_meta_data can be falsified by its input).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71336">#71336</a> (Wrong is_ref on properties as exposed via get_object_vars()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71459">#71459</a> (Integer overflow in iptcembed()).</li>
</ul></li>
<li>Apache2handler:
<ul>
  <li>Fix &gt;2G Content-Length headers in apache2handler.</li>
</ul></li>
<li>CURL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71227">#71227</a> (Can't compile php_curl statically).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71225">#71225</a> (curl_setopt() fails to set CURLOPT_POSTFIELDS with reference to CURLFile).</li>
</ul></li>
<li>GD:
<ul>
  <li>Improved fix for bug <a href="http://bugs.php.net/70976">#70976</a>.</li>
</ul></li>
<li>Interbase:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71305">#71305</a> (Crash when optional resource is omitted).</li>
</ul></li>
<li>LDAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71249">#71249</a> (ldap_mod_replace/ldap_mod_add store value as string "Array").</li>
</ul></li>
<li>mbstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71397">#71397</a> (mb_send_mail segmentation fault).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71475">#71475</a> (openssl_seal() uninitialized memory usage).</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Upgraded bundled PCRE library to 8.38. (CVE-2015-8383, CVE-2015-8386, CVE-2015-8387, CVE-2015-8389, CVE-2015-8390, CVE-2015-8391, CVE-2015-8393, CVE-2015-8394)</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71354">#71354</a> (Heap corruption in tar/zip/phar parser). (CVE-2016-4342)</li>
  <li>Fixed bug <a href="http://bugs.php.net/71331">#71331</a> (Uninitialized pointer in phar_make_dirstream()). (CVE-2016-4343)</li>
  <li>Fixed bug <a href="http://bugs.php.net/71391">#71391</a> (NULL Pointer Dereference in phar_tar_setupmetadata()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71488">#71488</a> (Stack overflow when decompressing tar archives). (CVE-2016-2554)</li>
</ul></li>
<li>SOAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70979">#70979</a> (crash with bad soap request).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71204">#71204</a> (segfault if clean spl_autoload_funcs while autoloading).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71202">#71202</a> (Autoload function registered by another not activated immediately).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71311">#71311</a> (Use-after-free vulnerability in SPL(ArrayObject, unserialize)).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71313">#71313</a> (Use-after-free vulnerability in SPL(SplObjectStorage, unserialize)).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71287">#71287</a> (Error message contains hexadecimal instead of decimal number).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71264">#71264</a> (file_put_contents() returns unexpected value when filesystem runs full).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71245">#71245</a> (file_get_contents() ignores "header" context option if it's a reference).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71220">#71220</a> (Null pointer deref (segfault) in compact via ob_start).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71190">#71190</a> (substr_replace converts integers in original $search array to strings).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71188">#71188</a> (str_replace converts integers in original $search array to strings).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71132">#71132</a>, <a href="http://bugs.php.net/71197">#71197</a> (range() segfaults).</li>
</ul></li>
<li>WDDX:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71335">#71335</a> (Type Confusion in WDDX Packet Deserialization).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.2"><!-- {{{ 7.0.2 -->
<h3>Version 7.0.2</h3>
<time class='releasedate' datetime='2016-01-07'>07 Jan 2016</time><ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71165">#71165</a> (-DGC_BENCH=1 doesn't work on PHP7).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71163">#71163</a> (Segmentation Fault: cleanup_unfinished_calls).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71109">#71109</a> (ZEND_MOD_CONFLICTS("xdebug") doesn't work).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71092">#71092</a> (Segmentation fault with return type hinting).</li>
  <li>Fixed bug memleak in header_register_callback.</li>
  <li>Fixed bug <a href="http://bugs.php.net/71067">#71067</a> (Local object in class method stays in memory for each call).</li>
  <li>Fixed bug <a href="http://bugs.php.net/66909">#66909</a> (configure fails utf8_to_mutf7 test).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70781">#70781</a> (Extension tests fail on dynamic ext dependency).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71089">#71089</a> (No check to duplicate zend_extension).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71086">#71086</a> (Invalid numeric literal parse error within highlight_string() function).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71154">#71154</a> (Incorrect HT iterator invalidation causes iterator reuse).</li>
  <li>Fixed bug <a href="http://bugs.php.net/52355">#52355</a> (Negating zero does not produce negative zero).</li>
  <li>Fixed bug <a href="http://bugs.php.net/66179">#66179</a> (var_export() exports float as integer).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70804">#70804</a> (Unary add on negative zero produces positive zero).</li>
</ul></li>
<li>CURL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71144">#71144</a> (Sementation fault when using cURL with ZTS).</li>
</ul></li>
<li>DBA:
<ul>
  <li>Fixed key leak with invalid resource.</li>
</ul></li>
<li>Filter:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71063">#71063</a> (filter_input(INPUT_ENV, ..) does not work).</li>
</ul></li>
<li>FTP:
<ul>
  <li>Implemented FR <a href="http://bugs.php.net/55651">#55651</a> (Option to ignore the returned FTP PASV address).</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70755">#70755</a> (fpm_log.c memory leak and buffer overflow). (CVE-2016-5114)</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70976">#70976</a> (Memory Read via gdImageRotateInterpolated Array Index Out of Bounds). (CVE-2016-1903)</li>
</ul></li>
<li>Mbstring:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71066">#71066</a> (mb_send_mail: Program terminated with signal SIGSEGV, Segmentation fault).</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71127">#71127</a> (Define in auto_prepend_file is overwrite).</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71178">#71178</a> (preg_replace with arrays creates [0] in replace array if not already set).</li>
</ul></li>
<li>Readline:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71094">#71094</a> (readline_completion_function corrupts static array on second TAB).</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71122">#71122</a> (Session GC may not remove obsolete session data).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71077">#71077</a> (ReflectionMethod for ArrayObject constructor returns wrong number of parameters).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71153">#71153</a> (Performance Degradation in ArrayIterator with large arrays).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71270">#71270</a> (Heap BufferOver Flow in escapeshell functions). (CVE-2016-1904)</li>
</ul></li>
<li>WDDX:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70661">#70661</a> (Use After Free Vulnerability in WDDX Packet Deserialization).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70741">#70741</a> (Session WDDX Packet Deserialization Type Confusion Vulnerability).</li>
</ul></li>
<li>XMLRPC:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70728">#70728</a> (Type Confusion Vulnerability in PHP_to_XMLRPC_worker).</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.1"><!-- {{{ 7.0.1 -->
<h3>Version 7.0.1</h3>
<time class='releasedate' datetime='2015-12-17'>17 Dec 2015</time><ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71105">#71105</a> (Format String Vulnerability in Class Name Error Message). (CVE-2015-8617)</li>
  <li>Fixed bug <a href="http://bugs.php.net/70831">#70831</a> (Compile fails on system with 160 CPUs).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71006">#71006</a> (symbol referencing errors on Sparc/Solaris).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70997">#70997</a> (When using parentClass:: instead of parent::, static context changed).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70970">#70970</a> (Segfault when combining error handler with output buffering).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70967">#70967</a> (Weird error handling for __toString when Error is thrown).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70958">#70958</a> (Invalid opcode while using ::class as trait method paramater default value).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70944">#70944</a> (try{ } finally{} can create infinite chains of exceptions).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70931">#70931</a> (Two errors messages are in conflict).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70904">#70904</a> (yield from incorrectly marks valid generator as finished).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70899">#70899</a> (buildconf failure in extensions).</li>
  <li>Fixed bug <a href="http://bugs.php.net/61751">#61751</a> (SAPI build problem on AIX: Undefined symbol: php_register_internal_extensions).</li>
  <li>Fixed \int (or generally every scalar type name with leading backslash) to not be accepted as type name.</li>
  <li>Fixed exception not being thrown immediately into a generator yielding from an array.</li>
  <li>Fixed bug <a href="http://bugs.php.net/70987">#70987</a> (static::class within Closure::call() causes segfault).</li>
  <li>Fixed bug <a href="http://bugs.php.net/71013">#71013</a> (Incorrect exception handler with yield from).</li>
  <li>Fixed double free in error condition of format printer.</li>
</ul></li>
<li>CLI server:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71005">#71005</a> (Segfault in php_cli_server_dispatch_router()).</li>
</ul></li>
<li>Intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71020">#71020</a> (Use after free in Collator::sortWithSortKeys). (CVE-2015-8616)</li>
</ul></li>
<li>Mysqlnd:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/68077">#68077</a> (LOAD DATA LOCAL INFILE / open_basedir restriction).</li>
  <li>Fixed bug <a href="http://bugs.php.net/68344">#68344</a> (MySQLi does not provide way to disable peer certificate validation) by introducing MYSQLI_CLIENT_SSL_DONT_VERIFY_SERVER_CERT connection flag.</li>
</ul></li>
<li>OCI8:
<ul>
  <li>Fixed LOB implementation size_t/zend_long mismatch reported by gcov.</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71024">#71024</a> (Unable to use PHP 7.0 x64 side-by-side with PHP 5.6 x32 on the same server).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70991">#70991</a> (zend_file_cache.c:710: error: array type has incomplete element type).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70977">#70977</a> (Segmentation fault with opcache.huge_code_pages=1).</li>
</ul></li>
<li>PDO_Firebird:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/60052">#60052</a> (Integer returned as a 64bit integer on X64_86).</li>
</ul></li>
<li>Phpdbg:
<ul>
  <li>Fixed stderr being written to stdout.</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71018">#71018</a> (ReflectionProperty::setValue() behavior changed).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70982">#70982</a> (setStaticPropertyValue behaviors inconsistently with 5.6).</li>
</ul></li>
<li>Soap:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70993">#70993</a> (Array key references break argument processing).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71028">#71028</a> (Undefined index with ArrayIterator).</li>
</ul></li>
<li>SQLite3:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/71049">#71049</a> (SQLite3Stmt::execute() releases bound parameter instead of internal buffer).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70999">#70999</a> (php_random_bytes: called object is not a function).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70960">#70960</a> (ReflectionFunction for array_unique returns wrong number of parameters).</li>
</ul></li>
<li>Streams/Socket:
<ul>
  <li>Add IPV6_V6ONLY constant / make it usable in stream contexts.</li>
</ul></li>
</ul>
<!-- }}} --></section>

<section class="version" id="7.0.0"><!-- {{{ 7.0.0 -->
<h3>Version 7.0.0</h3>
<time class='releasedate' datetime='2015-12-03'>03 Dec 2015</time><ul><li>Core:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70947">#70947</a> (INI parser segfault with INI_SCANNER_TYPED).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70914">#70914</a> (zend_throw_or_error() format string vulnerability).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70912">#70912</a> (Null ptr dereference instantiating class with invalid array property).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70895">#70895</a>, <a href="http://bugs.php.net/70898">#70898</a> (null ptr deref and segfault with crafted calable).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70249">#70249</a> (Segmentation fault while running PHPUnit tests on phpBB 3.2-dev).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70805">#70805</a> (Segmentation faults whilst running Drupal 8 test suite).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70842">#70842</a> (Persistent Stream Segmentation Fault).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70862">#70862</a> (Several functions do not check return code of php_stream_copy_to_mem()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70863">#70863</a> (Incorect logic to increment_function for proxy objects).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70323">#70323</a> (Regression in zend_fetch_debug_backtrace() can cause segfaults).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70873">#70873</a> (Regression on private static properties access).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70748">#70748</a> (Segfault in ini_lex () at Zend/zend_ini_scanner.l).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70689">#70689</a> (Exception handler does not work as expected).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70430">#70430</a> (Stack buffer overflow in zend_language_parser()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70782">#70782</a> (null ptr deref and segfault (zend_get_class_fetch_type)).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70785">#70785</a> (Infinite loop due to exception during identical comparison).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70630">#70630</a> (Closure::call/bind() crash with ReflectionFunction-&gt; getClosure()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70662">#70662</a> (Duplicate array key via undefined index error handler).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70681">#70681</a> (Segfault when binding $this of internal instance method to null).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70685">#70685</a> (Segfault for getClosure() internal method rebind with invalid $this).</li>
  <li>Added zend_internal_function.reserved[] fields.</li>
  <li>Fixed bug <a href="http://bugs.php.net/70557">#70557</a> (Memleak on return type verifying failed).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70555">#70555</a> (fun_get_arg() on unsetted vars return UNKNOW).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70548">#70548</a> (Redundant information printed in case of uncaught engine exception).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70547">#70547</a> (unsetting function variables corrupts backtrace).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70528">#70528</a> (assert() with instanceof adds apostrophes around class name).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70481">#70481</a> (Memory leak in auto_global_copy_ctor() in ZTS build).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70431">#70431</a> (Memory leak in php_ini.c).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70478">#70478</a> (**= does no longer work).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70398">#70398</a> (SIGSEGV, Segmentation fault zend_ast_destroy_ex).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70332">#70332</a> (Wrong behavior while returning reference on object).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70300">#70300</a> (Syntactical inconsistency with new group use syntax).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70321">#70321</a> (Magic getter breaks reference to array property).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70187">#70187</a> (Notice: unserialize(): Unexpected end of serialized data).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70145">#70145</a> (From field incorrectly parsed from headers).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70370">#70370</a> (Bundled libtool.m4 doesn't handle FreeBSD 10 when building extensions).</li>
  <li>Fixed bug causing exception traces with anon classes to be truncated.</li>
  <li>Fixed bug <a href="http://bugs.php.net/70397">#70397</a> (Segmentation fault when using Closure::call and yield).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70299">#70299</a> (Memleak while assigning object offsetGet result).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70288">#70288</a> (Apache crash related to ZEND_SEND_REF).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70262">#70262</a> (Accessing array crashes PHP 7.0beta3).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70258">#70258</a> (Segfault if do_resize fails to allocated memory).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70253">#70253</a> (segfault at _efree () in zend_alloc.c:1389).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70240">#70240</a> (Segfault when doing unset($var());).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70223">#70223</a> (Incrementing value returned by magic getter).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70215">#70215</a> (Segfault when __invoke is static).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70207">#70207</a> (Finally is broken with opcache).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70173">#70173</a> (ZVAL_COPY_VALUE_EX broken for 32bit Solaris Sparc).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69487">#69487</a> (SAPI may truncate POST data).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70198">#70198</a> (Checking liveness does not work as expected).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70241">#70241</a>, <a href="http://bugs.php.net/70293">#70293</a> (Skipped assertions affect Generator returns).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70239">#70239</a> (Creating a huge array doesn't result in exhausted, but segfault).</li>
  <li>Fixed "finally" issues.</li>
  <li>Fixed bug <a href="http://bugs.php.net/70098">#70098</a> (Real memory usage doesn't decrease).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70159">#70159</a> (__CLASS__ is lost in closures).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70156">#70156</a> (Segfault in zend_find_alias_name).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70124">#70124</a> (null ptr deref / seg fault in ZEND_HANDLE_EXCEPTION).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70117">#70117</a> (Unexpected return type error).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70106">#70106</a> (Inheritance by anonymous class).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69674">#69674</a> (SIGSEGV array.c:953).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70164">#70164</a> (__COMPILER_HALT_OFFSET__ under namespace is not defined).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70108">#70108</a> (sometimes empty $_SERVER['QUERY_STRING']).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70179">#70179</a> ($this refcount issue).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69896">#69896</a> ('asm' operand has impossible constraints).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70183">#70183</a> (null pointer deref (segfault) in zend_eval_const_expr).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70182">#70182</a> (Segfault in ZEND_ASSIGN_DIV_SPEC_CV_UNUSED_HANDLER).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69793">#69793</a> (Remotely triggerable stack exhaustion via recursive method calls).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69892">#69892</a> (Different arrays compare indentical due to integer key truncation).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70121">#70121</a> (unserialize() could lead to unexpected methods execution / NULL pointer deref).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70089">#70089</a> (segfault at ZEND_FETCH_DIM_W_SPEC_VAR_CONST_HANDLER ()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70057">#70057</a> (Build failure on 32-bit Mac OS X 10.6.8: recursive inlining).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70012">#70012</a> (Exception lost with nested finally block).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69996">#69996</a> (Changing the property of a cloned object affects the original).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70083">#70083</a> (Use after free with assign by ref to overloaded objects).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70006">#70006</a> (cli - function with default arg = STDOUT crash output).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69521">#69521</a> (Segfault in gc_collect_cycles()).</li>
  <li>Improved zend_string API.</li>
  <li>Fixed bug <a href="http://bugs.php.net/69955">#69955</a> (Segfault when trying to combine [] and assign-op on ArrayAccess object).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69957">#69957</a> (Different ways of handling div/mod/intdiv).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69900">#69900</a> (Too long timeout on pipes).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69872">#69872</a> (uninitialised value in strtr with array).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69868">#69868</a> (Invalid read of size 1 in zend_compile_short_circuiting).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69849">#69849</a> (Broken output of apache_request_headers).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69840">#69840</a> (iconv_substr() doesn't work with UTF-16BE).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69823">#69823</a> (PHP 7.0.0alpha1 segmentation fault when exactly 33 extensions are loaded).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69805">#69805</a> (null ptr deref and seg fault in zend_resolve_class_name).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69802">#69802</a> (Reflection on Closure::__invoke borks type hint class name).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69761">#69761</a> (Serialization of anonymous classes should be prevented).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69551">#69551</a> (parse_ini_file() and parse_ini_string() segmentation fault).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69781">#69781</a> (phpinfo() reports Professional Editions of Windows 7/8/8.1/10 as "Business").</li>
  <li>Fixed bug <a href="http://bugs.php.net/69835">#69835</a> (phpinfo() does not report many Windows SKUs).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69889">#69889</a> (Null coalesce operator doesn't work for string offsets).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69891">#69891</a> (Unexpected array comparison result).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69892">#69892</a> (Different arrays compare indentical due to integer key truncation).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69893">#69893</a> (Strict comparison between integer and empty string keys crashes).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69767">#69767</a> (Default parameter value with wrong type segfaults).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69756">#69756</a> (Fatal error: Nesting level too deep - recursive dependency ? with ===).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69758">#69758</a> (Item added to array not being removed by array_pop/shift ).</li>
  <li>Fixed bug <a href="http://bugs.php.net/68475">#68475</a> (Add support for $callable() sytnax with 'Class::method').</li>
  <li>Fixed bug <a href="http://bugs.php.net/69485">#69485</a> (Double free on zend_list_dtor).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69427">#69427</a> (Segfault on magic method __call of private method in superclass).</li>
  <li>Improved __call() and __callStatic() magic method handling. Now they are called in a stackless way using ZEND_CALL_TRAMPOLINE opcode, without additional stack frame.</li>
  <li>Optimized strings concatenation.</li>
  <li>Fixed weird operators behavior. Division by zero now emits warning and returns +/-INF, modulo by zero and intdid() throws an exception, shifts by negative offset throw exceptions. Compile-time evaluation of division by zero is disabled.</li>
  <li>Fixed bug <a href="http://bugs.php.net/69371">#69371</a> (Hash table collision leads to inaccessible array keys).</li>
  <li>Fixed bug <a href="http://bugs.php.net/68933">#68933</a> (Invalid read of size 8 in zend_std_read_property).</li>
  <li>Fixed bug <a href="http://bugs.php.net/68252">#68252</a> (segfault in Zend/zend_hash.c in function _zend_hash_del_el).</li>
  <li>Fixed bug <a href="http://bugs.php.net/65598">#65598</a> (Closure executed via static autoload incorrectly marked as static).</li>
  <li>Fixed bug <a href="http://bugs.php.net/66811">#66811</a> (Cannot access static::class in lambda, writen outside of a class).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69568">#69568</a> (call a private function in closure failed).</li>
  <li>Added PHP_INT_MIN constant.</li>
  <li>Added Closure::call() method.</li>
  <li>Fixed bug <a href="http://bugs.php.net/67959">#67959</a> (Segfault when calling phpversion('spl')).</li>
  <li>Implemented the RFC `Catchable "Call to a member function bar() on a non-object"`.</li>
  <li>Added options parameter for unserialize allowing to specify acceptable classes (https://wiki.php.net/rfc/secure_unserialize).</li>
  <li>Fixed bug <a href="http://bugs.php.net/63734">#63734</a> (Garbage collector can free zvals that are still referenced).</li>
  <li>Removed ZEND_ACC_FINAL_CLASS, promoting ZEND_ACC_FINAL as final class modifier.</li>
  <li>is_long() &amp; is_integer() is now an alias of is_int().</li>
  <li>Implemented FR <a href="http://bugs.php.net/55467">#55467</a> (phpinfo: PHP Variables with $ and single quotes).</li>
  <li>Added ?? operator.</li>
  <li>Added &lt;=&gt; operator.</li>
  <li>Added \u{xxxxx} Unicode Codepoint Escape Syntax.</li>
  <li>Fixed oversight where define() did not support arrays yet const syntax did.</li>
  <li>Use "integer" and "float" instead of "long" and "double" in ZPP, type hint and conversion error messages.</li>
  <li>Implemented FR <a href="http://bugs.php.net/55428">#55428</a> (E_RECOVERABLE_ERROR when output buffering in output buffering handler).</li>
  <li>Removed scoped calls of non-static methods from an incompatible $this context.</li>
  <li>Removed support for #-style comments in ini files.</li>
  <li>Removed support for assigning the result of new by reference.</li>
  <li>Invalid octal literals in source code now produce compile errors, fixes PHPSadness #31.</li>
  <li>Removed dl() function on fpm-fcgi.</li>
  <li>Removed support for hexadecimal numeric strings.</li>
  <li>Removed obsolete extensions and SAPIs. See the full list in UPGRADING.</li>
  <li>Added NULL byte protection to exec, system and passthru.</li>
  <li>Added error_clear_last() function.</li>
  <li>Fixed bug <a href="http://bugs.php.net/68797">#68797</a> (Number 2.2250738585072012e-308 converted incorrectly).</li>
  <li>Improved zend_qsort(using hybrid sorting algo) for better performance, and also renamed zend_qsort to zend_sort.</li>
  <li>Added stable sorting algo zend_insert_sort.</li>
  <li>Improved zend_memnchr(using sunday algo) for better performance.</li>
  <li>Implemented the RFC `Scalar Type Decalarations v0.5`.</li>
  <li>Implemented the RFC `Group Use Declarations`.</li>
  <li>Implemented the RFC `Continue Output Buffering`.</li>
  <li>Implemented the RFC `Constructor behaviour of internal classes`.</li>
  <li>Implemented the RFC `Fix "foreach" behavior`.</li>
  <li>Implemented the RFC `Generator Delegation`.</li>
  <li>Implemented the RFC `Anonymous Class Support`.</li>
  <li>Implemented the RFC `Context Sensitive Lexer`.</li>
  <li>Fixed bug <a href="http://bugs.php.net/69511">#69511</a> (Off-by-one buffer overflow in php_sys_readlink).</li>
</ul></li>
<li>CLI server:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/68291">#68291</a> (404 on urls with '+').</li>
  <li>Fixed bug <a href="http://bugs.php.net/66606">#66606</a> (Sets HTTP_CONTENT_TYPE but not CONTENT_TYPE).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70264">#70264</a> (CLI server directory traversal).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69655">#69655</a> (php -S changes MKCALENDAR request method to MKCOL).</li>
  <li>Fixed bug <a href="http://bugs.php.net/64878">#64878</a> (304 responses return Content-Type header).</li>
  <li>Refactor MIME type handling to use a hash table instead of linear search.</li>
  <li>Update the MIME type list from the one shipped by Apache HTTPD.</li>
  <li>Added support for SEARCH WebDav method.</li>
</ul></li>
<li>COM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69939">#69939</a> (Casting object to bool returns false).</li>
</ul></li>
<li>Curl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70330">#70330</a> (Segmentation Fault with multiple "curl_copy_handle").</li>
  <li>Fixed bug <a href="http://bugs.php.net/70163">#70163</a> (curl_setopt_array() type confusion).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70065">#70065</a> (curl_getinfo() returns corrupted values).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69831">#69831</a> (Segmentation fault in curl_getinfo).</li>
  <li>Fixed bug <a href="http://bugs.php.net/68937">#68937</a> (Segfault in curl_multi_exec).</li>
  <li>Removed support for unsafe file uploads.</li>
</ul></li>
<li>Date:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70245">#70245</a> (strtotime does not emit warning when 2nd parameter is object or string).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70266">#70266</a> (DateInterval::__construct.interval_spec is not supposed to be optional).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70277">#70277</a> (new DateTimeZone($foo) is ignoring text after null byte).</li>
  <li>Fixed day_of_week function as it could sometimes return negative values internally.</li>
  <li>Removed $is_dst parameter from mktime() and gmmktime().</li>
  <li>Removed date.timezone warning (https://wiki.php.net/rfc/date.timezone_warning_removal).</li>
  <li>Added "v" DateTime format modifier to get the 3-digit version of fraction of seconds.</li>
  <li>Implemented FR <a href="http://bugs.php.net/69089">#69089</a> (Added DateTime::RFC3339_EXTENDED to output in RFC3339 Extended format which includes fraction of seconds).</li>
</ul></li>
<li>DBA:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/62490">#62490</a> (dba_delete returns true on missing item (inifile)).</li>
  <li>Fixed bug <a href="http://bugs.php.net/68711">#68711</a> (useless comparisons).</li>
</ul></li>
<li>DOM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70558">#70558</a> ("Couldn't fetch" error in DOMDocument::registerNodeClass()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70001">#70001</a> (Assigning to DOMNode::textContent does additional entity encoding).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69846">#69846</a> (Segmenation fault (access violation) when iterating over DOMNodeList).</li>
  <li>Made DOMNode::textContent writeable.</li>
</ul></li>
<li>EXIF:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70385">#70385</a> (Buffer over-read in exif_read_data with TIFF IFD tag byte value of 32 bytes).</li>
</ul></li>
<li>Fileinfo:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/66242">#66242</a> (libmagic: don't assume char is signed).</li>
</ul></li>
<li>Filter:
<ul>
  <li>New FILTER_VALIDATE_DOMAIN and better RFC conformance for FILTER_VALIDATE_URL.</li>
  <li>Fixed bug <a href="http://bugs.php.net/67167">#67167</a> 	(Wrong return value from FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE).</li>
</ul></li>
<li>FPM:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70538">#70538</a> ("php-fpm -i" crashes).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70279">#70279</a> (HTTP Authorization Header is sometimes passed to newer reqeusts).</li>
  <li>Fixed bug <a href="http://bugs.php.net/68945">#68945</a> (Unknown admin values segfault pools).</li>
  <li>Fixed bug <a href="http://bugs.php.net/65933">#65933</a> (Cannot specify config lines longer than 1024 bytes).</li>
  <li>Implemented FR <a href="http://bugs.php.net/67106">#67106</a> (Split main fpm config).</li>
</ul></li>
<li>FTP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69082">#69082</a> (FTPS support on Windows).</li>
</ul></li>
<li>GD:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/53156">#53156</a> (imagerectangle problem with point ordering).</li>
  <li>Fixed bug <a href="http://bugs.php.net/66387">#66387</a> (Stack overflow with imagefilltoborder). (CVE-2015-8874)</li>
  <li>Fixed bug <a href="http://bugs.php.net/70102">#70102</a> (imagecreatefromwebm() shifts colors).</li>
  <li>Fixed bug <a href="http://bugs.php.net/66590">#66590</a> (imagewebp() doesn't pad to even length).</li>
  <li>Fixed bug <a href="http://bugs.php.net/66882">#66882</a> (imagerotate by -90 degrees truncates image by 1px).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70064">#70064</a> (imagescale(..., IMG_BICUBIC) leaks memory).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69024">#69024</a> (imagescale segfault with palette based image).</li>
  <li>Fixed bug <a href="http://bugs.php.net/53154">#53154</a> (Zero-height rectangle has whiskers).</li>
  <li>Fixed bug <a href="http://bugs.php.net/67447">#67447</a> (imagecrop() add a black line when cropping).</li>
  <li>Fixed bug <a href="http://bugs.php.net/68714">#68714</a> (copy 'n paste error).</li>
  <li>Fixed bug <a href="http://bugs.php.net/66339">#66339</a> (PHP segfaults in imagexbm).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70047">#70047</a> (gd_info() doesn't report WebP support).</li>
  <li>Replace libvpx with libwebp for bundled libgd.</li>
  <li>Fixed bug <a href="http://bugs.php.net/61221">#61221</a> (imagegammacorrect function loses alpha channel).</li>
  <li>Made fontFetch's path parser thread-safe.</li>
  <li>Removed T1Lib support.</li>
</ul></li>
<li>GMP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70284">#70284</a> (Use after free vulnerability in unserialize() with GMP).</li>
</ul></li>
<li>hash:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70312">#70312</a> (HAVAL gives wrong hashes in specific cases).</li>
</ul></li>
<li>IMAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70158">#70158</a> (Building with static imap fails).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69998">#69998</a> (curl multi leaking memory).</li>
</ul></li>
<li>Intl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70453">#70453</a> (IntlChar::foldCase() incorrect arguments and missing constants).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70454">#70454</a> (IntlChar::forDigit second parameter should be optional).</li>
  <li>Removed deprecated aliases datefmt_set_timezone_id() and IntlDateFormatter::setTimeZoneID().</li>
</ul></li>
<li>JSON:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/62010">#62010</a> (json_decode produces invalid byte-sequences).</li>
  <li>Fixed bug <a href="http://bugs.php.net/68546">#68546</a> (json_decode() Fatal error: Cannot access property started with '\0').</li>
  <li>Replace non-free JSON parser with a parser from Jsond extension, fixes <a href="http://bugs.php.net/63520">#63520</a> (JSON extension includes a problematic license statement).</li>
  <li>Fixed bug <a href="http://bugs.php.net/68938">#68938</a> (json_decode() decodes empty string without error).</li>
</ul></li>
<li>LDAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/47222">#47222</a> (Implement LDAP_OPT_DIAGNOSTIC_MESSAGE).</li>
</ul></li>
<li>LiteSpeed:
<ul>
  <li>Updated LiteSpeed SAPI code from V5.5 to V6.6.</li>
</ul></li>
<li>libxml:
<ul>
  <li>Fixed handling of big lines in error messages with libxml &gt;= 2.9.0.</li>
</ul></li>
<li>Mcrypt:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70625">#70625</a> (mcrypt_encrypt() won't return data when no IV was specified under RC4).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69833">#69833</a> (mcrypt fd caching not working).</li>
  <li>Fixed possible read after end of buffer and use after free.</li>
  <li>Removed mcrypt_generic_end() alias.</li>
  <li>Removed mcrypt_ecb(), mcrypt_cbc(), mcrypt_cfb(), mcrypt_ofb().</li>
</ul></li>
<li>Mysqli:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/32490">#32490</a> (constructor of mysqli has wrong name).</li>
</ul></li>
<li>Mysqlnd:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70949">#70949</a> (SQL Result Sets With NULL Can Cause Fatal Memory Errors).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70384">#70384</a> (mysqli_real_query():Unknown type 245 sent by the server).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70456">#70456</a> (mysqlnd doesn't activate TCP keep-alive when connecting to a server).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70572">#70572</a> segfault in mysqlnd_connect.</li>
  <li>Fixed bug <a href="http://bugs.php.net/69796">#69796</a> (mysqli_stmt::fetch doesn't assign null values to bound variables).</li>
</ul></li>
<li>OCI8:
<ul>
  <li>Fixed memory leak with LOBs.</li>
  <li>Fixed bug <a href="http://bugs.php.net/68298">#68298</a> (OCI int overflow).</li>
  <li>Corrected oci8 hash destructors to prevent segfaults, and a few other fixes.</li>
</ul></li>
<li>ODBC:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69975">#69975</a> (PHP segfaults when accessing nvarchar(max) defined columns. (CVE-2015-8879)</li>
</ul></li>
<li>Opcache:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70656">#70656</a> (require() statement broken after opcache_reset() or a few hours of use).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70843">#70843</a> (Segmentation fault on MacOSX with opcache.file_cache_only=1).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70724">#70724</a> (Undefined Symbols from opcache.so on Mac OS X 10.10).</li>
  <li>Fixed compatibility with Windows 10 (see also bug <a href="http://bugs.php.net/70652">#70652</a>).</li>
  <li>Attmpt to fix "Unable to reattach to base address" problem.</li>
  <li>Fixed bug <a href="http://bugs.php.net/70423">#70423</a> (Warning Internal error: wrong size calculation).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70237">#70237</a> (Empty while and do-while segmentation fault with opcode on CLI enabled).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70111">#70111</a> (Segfault when a function uses both an explicit return type and an explicit cast).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70058">#70058</a> (Build fails when building for i386).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70022">#70022</a> (Crash with opcache using opcache.file_cache_only=1).</li>
  <li>Removed opcache.load_comments configuration directive. Now doc comments loading costs nothing and always enabled.</li>
  <li>Fixed bug <a href="http://bugs.php.net/69838">#69838</a> (Wrong size calculation for function table).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69688">#69688</a> (segfault with eval and opcache fast shutdown).</li>
  <li>Added experimental (disabled by default) file based opcode cache.</li>
  <li>Fixed bug with try blocks being removed when extended_info opcode generation is turned on.</li>
  <li>Fixed bug <a href="http://bugs.php.net/68644">#68644</a> (strlen incorrect : mbstring + func_overload=2 +UTF-8 + Opcache).</li>
</ul></li>
<li>OpenSSL:
<ul>
  <li>Require at least OpenSSL version 0.9.8.</li>
  <li>Fixed bug <a href="http://bugs.php.net/68312">#68312</a> (Lookup for openssl.cnf causes a message box).</li>
  <li>Fixed bug <a href="http://bugs.php.net/55259">#55259</a> (openssl extension does not get the DH parameters from DH key resource).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70395">#70395</a> (Missing ARG_INFO for openssl_seal()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/60632">#60632</a> (openssl_seal fails with AES).</li>
  <li>Implemented FR <a href="http://bugs.php.net/70438">#70438</a> (Add IV parameter for openssl_seal and openssl_open).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70014">#70014</a> (openssl_random_pseudo_bytes() is not cryptographically secure). (CVE-2015-8867)</li>
  <li>Fixed bug <a href="http://bugs.php.net/69882">#69882</a> (OpenSSL error "key values mismatch" after openssl_pkcs12_read with extra cert).</li>
  <li>Added "alpn_protocols" SSL context option allowing encrypted client/server streams to negotiate alternative protocols using the ALPN TLS extension when built against OpenSSL 1.0.2 or newer. Negotiated protocol information is accessible through stream_get_meta_data() output.</li>
  <li>Removed "CN_match" and "SNI_server_name" SSL context options. Use automatic detection or the "peer_name" option instead.</li>
</ul></li>
<li>Pcntl:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70386">#70386</a> (Can't compile on NetBSD because of missing WCONTINUED and WIFCONTINUED).</li>
  <li>Fixed bug <a href="http://bugs.php.net/60509">#60509</a> (pcntl_signal doesn't decrease ref-count of old handler when setting SIG_DFL).</li>
  <li>Implemented FR <a href="http://bugs.php.net/68505">#68505</a> (Added wifcontinued and wcontinued).</li>
  <li>Added rusage support to pcntl_wait() and pcntl_waitpid().</li>
</ul></li>
<li>PCRE:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70232">#70232</a> (Incorrect bump-along behavior with \K and empty string match).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70345">#70345</a> (Multiple vulnerabilities related to PCRE functions).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70232">#70232</a> (Incorrect bump-along behavior with \K and empty string match).</li>
  <li>Fixed bug <a href="http://bugs.php.net/53823">#53823</a> (preg_replace: * qualifier on unicode replace garbles the string).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69864">#69864</a> (Segfault in preg_replace_callback).</li>
  <li>Removed support for the /e (PREG_REPLACE_EVAL) modifier.</li>
</ul></li>
<li>PDO:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70861">#70861</a> (Segmentation fault in pdo_parse_params() during Drupal 8 test suite).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70389">#70389</a> (PDO constructor changes unrelated variables).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70272">#70272</a> (Segfault in pdo_mysql).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70221">#70221</a> (persistent sqlite connection + custom function segfaults).</li>
  <li>Fixed bug <a href="http://bugs.php.net/59450">#59450</a> (./configure fails with "Cannot find php_pdo_driver.h").</li>
</ul></li>
<li>PDO_DBlib:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69757">#69757</a> (Segmentation fault on nextRowset).</li>
</ul></li>
<li>PDO_mysql:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/68424">#68424</a> (Add new PDO mysql connection attr to control multi statements option).</li>
</ul></li>
<li>PDO_OCI:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70308">#70308</a> (PDO::ATTR_PREFETCH is ignored).</li>
</ul></li>
<li>PDO_pgsql:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69752">#69752</a> (PDOStatement::execute() leaks memory with DML Statements when closeCuror() is u).</li>
  <li>Removed PGSQL_ATTR_DISABLE_NATIVE_PREPARED_STATEMENT attribute in favor of ATTR_EMULATE_PREPARES).</li>
</ul></li>
<li>Phar:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69720">#69720</a> (Null pointer dereference in phar_get_fp_offset()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70433">#70433</a> (Uninitialized pointer in phar_make_dirstream when zip entry filename is "/").</li>
  <li>Improved fix for bug <a href="http://bugs.php.net/69441">#69441</a>.</li>
  <li>Fixed bug <a href="http://bugs.php.net/70019">#70019</a> (Files extracted from archive may be placed outside of destination directory).</li>
</ul></li>
<li>Phpdbg:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70614">#70614</a> (incorrect exit code in -rr mode with Exceptions).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70532">#70532</a> (phpdbg must respect set_exception_handler).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70531">#70531</a> (Run and quit mode (-qrr) should not fallback to interactive mode).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70533">#70533</a> (Help overview (-h) does not rpint anything under Windows).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70449">#70449</a> (PHP won't compile on 10.4 and 10.5 because of missing constants).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70214">#70214</a> (FASYNC not defined, needs sys/file.h include).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70138">#70138</a> (Segfault when displaying memory leaks).</li>
</ul></li>
<li>Reflection:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70650">#70650</a> (Wrong docblock assignment).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70674">#70674</a> (ReflectionFunction::getClosure() leaks memory when used for internal functions).</li>
  <li>Fixed bug causing bogus traces for ReflectionGenerator::getTrace().</li>
  <li>Fixed inheritance chain of Reflector interface.</li>
  <li>Added ReflectionGenerator class.</li>
  <li>Added reflection support for return types and type declarations.</li>
</ul></li>
<li>Session:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70876">#70876</a> (Segmentation fault when regenerating session id with strict mode).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70529">#70529</a> (Session read causes "String is not zero-terminated" error).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70013">#70013</a> (Reference to $_SESSION is lost after a call to session_regenerate_id()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69952">#69952</a> (Data integrity issues accessing superglobals by reference).</li>
  <li>Fixed bug <a href="http://bugs.php.net/67694">#67694</a> (Regression in session_regenerate_id()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/68941">#68941</a> (mod_files.sh is a bash-script).</li>
</ul></li>
<li>SOAP:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70940">#70940</a> (Segfault in soap / type_to_string).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70900">#70900</a> (SoapClient systematic out of memory error).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70875">#70875</a> (Segmentation fault if wsdl has no targetNamespace attribute).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70715">#70715</a> (Segmentation fault inside soap client).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70709">#70709</a> (SOAP Client generates Segfault).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70388">#70388</a> (SOAP serialize_function_call() type confusion / RCE).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70081">#70081</a> (SoapClient info leak / null pointer dereference via multiple type confusions).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70079">#70079</a> (Segmentation fault after more than 100 SoapClient calls).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70032">#70032</a> (make_http_soap_request calls zend_hash_get_current_key_ex(,,,NULL).</li>
  <li>Fixed bug <a href="http://bugs.php.net/68361">#68361</a> (Segmentation fault on SoapClient::__getTypes).</li>
</ul></li>
<li>SPL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70959">#70959</a> (ArrayObject unserialize does not restore protected fields).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70853">#70853</a> (SplFixedArray throws exception when using ref variable as index).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70868">#70868</a> (PCRE JIT and pattern reuse segfault).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70730">#70730</a> (Incorrect ArrayObject serialization if unset is called in serialize()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70573">#70573</a> (Cloning SplPriorityQueue leads to memory leaks).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70303">#70303</a> (Incorrect constructor reflection for ArrayObject).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70068">#70068</a> (Dangling pointer in the unserialization of ArrayObject items).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70166">#70166</a> (Use After Free Vulnerability in unserialize() with SPLArrayObject).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70168">#70168</a> (Use After Free Vulnerability in unserialize() with SplObjectStorage).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70169">#70169</a> (Use After Free Vulnerability in unserialize() with SplDoublyLinkedList).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70053">#70053</a> (MutlitpleIterator array-keys incompatible change in PHP 7).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69970">#69970</a> (Use-after-free vulnerability in spl_recursive_it_move_forward_ex()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69845">#69845</a> (ArrayObject with ARRAY_AS_PROPS broken).</li>
  <li>Changed ArrayIterator implementation using zend_hash_iterator_... API. Allowed modification of iterated ArrayObject using the same behavior as proposed in `Fix "foreach" behavior`. Removed "Array was modified outside object and internal position is no longer valid" hack.</li>
  <li>Implemented FR <a href="http://bugs.php.net/67886">#67886</a> (SplPriorityQueue/SplHeap doesn't expose extractFlags nor curruption state).</li>
  <li>Fixed bug <a href="http://bugs.php.net/66405">#66405</a> (RecursiveDirectoryIterator::CURRENT_AS_PATHNAME breaks the RecursiveIterator).</li>
</ul></li>
<li>SQLite3:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70571">#70571</a> (Memory leak in sqlite3_do_callback).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69972">#69972</a> (Use-after-free vulnerability in sqlite3SafetyCheckSickOrOk()).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69897">#69897</a> (segfault when manually constructing SQLite3Result).</li>
  <li>Fixed bug <a href="http://bugs.php.net/68260">#68260</a> (SQLite3Result::fetchArray declares wrong required_num_args).</li>
</ul></li>
<li>Standard:
<ul>
  <li>Fixed count on symbol tables.</li>
  <li>Fixed bug <a href="http://bugs.php.net/70963">#70963</a> (Unserialize shows UNKNOWN in result).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70910">#70910</a> (extract() breaks variable references).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70808">#70808</a> (array_merge_recursive corrupts memory of unset items).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70667">#70667</a> (strtr() causes invalid writes and a crashes).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70668">#70668</a> (array_keys() doesn't respect references when $strict is true).</li>
  <li>Implemented the RFC `Random Functions Throwing Exceptions in PHP 7`.</li>
  <li>Fixed bug <a href="http://bugs.php.net/70487">#70487</a> (pack('x') produces an error).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70342">#70342</a> (changing configuration with ignore_user_abort(true) isn't working).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70295">#70295</a> (Segmentation fault with setrawcookie).</li>
  <li>Fixed bug <a href="http://bugs.php.net/67131">#67131</a> (setcookie() conditional for empty values not met).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70365">#70365</a> (Use-after-free vulnerability in unserialize() with SplObjectStorage).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70366">#70366</a> (Use-after-free vulnerability in unserialize() with SplDoublyLinkedList).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70250">#70250</a> (extract() turns array elements to references).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70211">#70211</a> (php 7 ZEND_HASH_IF_FULL_DO_RESIZE use after free).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70208">#70208</a> (Assert breaking access on objects).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70140">#70140</a> (str_ireplace/php_string_tolower - Arbitrary Code Execution).</li>
  <li>Implemented FR <a href="http://bugs.php.net/70112">#70112</a> (Allow "dirname" to go up various times).</li>
  <li>Fixed bug <a href="http://bugs.php.net/36365">#36365</a> (scandir duplicates file name at every 65535th file).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70096">#70096</a> (Repeated iptcembed() adds superfluous FF bytes).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70018">#70018</a> (exec does not strip all whitespace).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69983">#69983</a> (get_browser fails with user agent of null).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69976">#69976</a> (Unable to parse "all" urls with colon char).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69768">#69768</a> (escapeshell*() doesn't cater to !).</li>
  <li>Fixed bug <a href="http://bugs.php.net/62922">#62922</a> (Truncating entire string should result in string).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69723">#69723</a> (Passing parameters by reference and array_column).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69523">#69523</a> (Cookie name cannot be empty).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69325">#69325</a> (php_copy_file_ex does not pass the argument).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69299">#69299</a> (Regression in array_filter's $flag argument in PHP 7).</li>
  <li>Removed call_user_method() and call_user_method_array() functions.</li>
  <li>Fixed user session handlers (See rfc:session.user.return-value).</li>
  <li>Added intdiv() function.</li>
  <li>Improved precision of log() function for base 2 and 10.</li>
  <li>Remove string category support in setlocale().</li>
  <li>Remove set_magic_quotes_runtime() and its alias magic_quotes_runtime().</li>
  <li>Fixed bug <a href="http://bugs.php.net/65272">#65272</a> (flock() out parameter not set correctly in windows).</li>
  <li>Added preg_replace_callback_array function.</li>
  <li>Deprecated salt option to password_hash.</li>
  <li>Fixed bug <a href="http://bugs.php.net/69686">#69686</a> (password_verify reports back error on PHP7 will null string).</li>
  <li>Added Windows support for getrusage().</li>
  <li>Removed hardcoded limit on number of pipes in proc_open().</li>
</ul></li>
<li>Streams:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70361">#70361</a> (HTTP stream wrapper doesn't close keep-alive connections).</li>
  <li>Fixed bug <a href="http://bugs.php.net/68532">#68532</a> (convert.base64-encode omits padding bytes).</li>
  <li>Removed set_socket_blocking() in favor of its alias stream_set_blocking().</li>
</ul></li>
<li>Tokenizer:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/69430">#69430</a> (token_get_all has new irrecoverable errors).</li>
</ul></li>
<li>XMLReader:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70309">#70309</a> (XmlReader read generates extra output).</li>
</ul></li>
<li>XMLRPC:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70526">#70526</a> (xmlrpc_set_type returns false on success).</li>
</ul></li>
<li>XSL:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70678">#70678</a> (PHP7 returns true when false is expected).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70535">#70535</a> (XSLT: free(): invalid pointer).</li>
  <li>Fixed bug <a href="http://bugs.php.net/69782">#69782</a> (NULL pointer dereference).</li>
  <li>Fixed bug <a href="http://bugs.php.net/64776">#64776</a> (The XSLT extension is not thread safe).</li>
  <li>Removed xsl.security_prefs ini option.</li>
</ul></li>
<li>Zlib:
<ul>
  <li>Added deflate_init(), deflate_add(), inflate_init(), inflate_add() functions allowing incremental/streaming compression/decompression.</li>
</ul></li>
<li>Zip:
<ul>
  <li>Fixed bug <a href="http://bugs.php.net/70322">#70322</a> (ZipArchive::close() doesn't indicate errors).</li>
  <li>Fixed bug <a href="http://bugs.php.net/70350">#70350</a> (ZipArchive::extractTo allows for directory traversal when creating directories). (CVE-2014-9767)</li>
  <li>Added ZipArchive::setCompressionName and ZipArchive::setCompressionIndex methods.</li>
  <li>Update bundled libzip to 1.0.1.</li>
  <li>Fixed bug <a href="http://bugs.php.net/67161">#67161</a> (ZipArchive::getStream() returns NULL for certain file).</li>
</ul></li>
</ul>
<!-- }}} --></section>

    </section><!-- layout-content -->
    

  </div><!-- layout -->

  <footer>
    <div class="container footer-content">
      <div class="row-fluid">
      <ul class="footmenu">
        <li><a href="/copyright.php">Copyright &copy; 2001-2020 The PHP Group</a></li>
        <li><a href="/my.php">My PHP.net</a></li>
        <li><a href="/contact.php">Contact</a></li>
        <li><a href="/sites.php">Other PHP.net sites</a></li>
        <li><a href="/privacy.php">Privacy policy</a></li>
      </ul>
      </div>
    </div>
  </footer>

    
 <!-- External and third party libraries. -->
 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="/cached.php?t=1421837618&amp;f=/js/ext/modernizr.js"></script>
<script src="/cached.php?t=1421837618&amp;f=/js/ext/hogan-2.0.0.min.js"></script>
<script src="/cached.php?t=1421837618&amp;f=/js/ext/typeahead.min.js"></script>
<script src="/cached.php?t=1421837618&amp;f=/js/ext/mousetrap.min.js"></script>
<script src="/cached.php?t=1421837618&amp;f=/js/search.js"></script>
<script src="/cached.php?t=1539765004&amp;f=/js/common.js"></script>

<a id="toTop" href="javascript:;"><span id="toTopHover"></span><img width="40" height="40" alt="To Top" src="/images/to-top@2x.png"></a>

</body>
</html>
